﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Services;
using LinqDB.ConnectDB;
using LinqDB.TABLE;
using System.Data.SqlClient;

/// <summary>
/// Summary description for MobileWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class MobileWebService : System.Web.Services.WebService
{

    public MobileWebService()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //[WebMethod]
    //public string HelloWorld()
    //{
    //    return "Hello World";
    //}

    [WebMethod]
    public ExecuteDataInfo TestFunction() {
        ////#########Test Insert Data #################
        //TransactionDB trans = new TransactionDB();
        //MsIndicatorLinqDB lnq = new MsIndicatorLinqDB();
        //lnq.INDICATOR_CODE = "001";
        //lnq.INDICATOR_NAME = "ด้านเศรษฐกิจ";
        //lnq.ACTIVE_STATUS = 'Y';

        //ExecuteDataInfo ret = lnq.InsertData("TestFunction", trans.Trans);
        //if (ret.IsSuccess == false)
        //{
        //    trans.RollbackTransaction();
        //    string _err = ret.ErrorMessage;
        //}
        //else {
        //    trans.CommitTransaction();

        //    long _id = lnq.ID;



        //}
        //lnq = null;
        ////#########################################


        ////######### Test Select Data #################
        ////####### Test 1 GetDataList
        //TransactionDB trans = new TransactionDB();
        //MsIndicatorLinqDB lnq = new MsIndicatorLinqDB();
        //SqlParameter[] p = new SqlParameter[1];
        //p[0] = SqlDB.SetBigInt("@_ID", 2);

        //DataTable dt = lnq.GetDataList("id=@_ID", "id", trans.Trans, p);
        //dt.TableName = "TestFunction";
        //if (dt.Rows.Count > 0)
        //{
        //    trans.CommitTransaction();
        //}
        //else {
        //    trans.RollbackTransaction();
        //    string _err = lnq.ErrorMessage;
        //}
        //lnq = null;

        ////####### Test 2 GetListBySql
        //TransactionDB trans = new TransactionDB();
        //MsIndicatorLinqDB lnq = new MsIndicatorLinqDB();
        //SqlParameter[] p = new SqlParameter[1];
        //p[0] = SqlDB.SetBigInt("@_ID", 3);

        //string sql = "select id, indicator_code, indicator_name from ms_indicator where id=@_ID";

        //DataTable dt = lnq.GetListBySql(sql, trans.Trans, p);
        //dt.TableName = "TestFunction";
        //if (dt.Rows.Count > 0)
        //{
        //    trans.CommitTransaction();
        //}
        //else {
        //    trans.RollbackTransaction();
        //    string _err = lnq.ErrorMessage;
        //}
        //lnq = null;
        ////#########################################


        ////######### Test Update Data #################
        //TransactionDB trans = new TransactionDB();
        //MsIndicatorLinqDB lnq = new MsIndicatorLinqDB();
        //lnq.GetDataByPK(2, trans.Trans);
        //lnq.INDICATOR_CODE = "0001";
        //lnq.INDICATOR_NAME = "ด้านเศรษฐกิจ0000";
        //lnq.ACTIVE_STATUS = 'N';

        //ExecuteDataInfo ret;
        //if (lnq.ID > 0)
        //{
        //    ret=lnq.UpdateData("TestFunction", trans.Trans);


        //}
        //else {
        //    ret=lnq.InsertData("TestFunction", trans.Trans);
        //    long _id = lnq.ID;

        //}


        //if (ret.IsSuccess == false)
        //{
        //    trans.RollbackTransaction();
        //    string _err = ret.ErrorMessage;
        //}
        //else {
        //    trans.CommitTransaction();
        //}
        //lnq = null;
        ////#########################################


        ////######### Test Delete Data #################
        //TransactionDB trans = new TransactionDB();
        //MsIndicatorLinqDB lnq = new MsIndicatorLinqDB();
        //ExecuteDataInfo ret = lnq.DeleteByPK(3, trans.Trans);
        //if (ret.IsSuccess == true)
        //{
        //    trans.CommitTransaction();
        //}
        //else
        //{
        //    trans.RollbackTransaction();
        //    string _err = ret.ErrorMessage;
        //}
        //lnq = null;
        ////############################################

        return new ExecuteDataInfo();
    }
}
