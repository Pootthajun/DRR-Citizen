﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_stakeholders2.aspx.cs" Inherits="DRR_Citizen.frm_stakeholders2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
<div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
          <div class="col-md-12">  
  <div class="box box-solid box-primary">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลผู้มีส่วนได้ส่วนเสีย (CPM_UT0502)</b></h4></div>
             <div class="box-primary">
                 <b class="text-info">ข้อมูลส่วนตัว </b>
                   
                          <div class="box box-solid box-primary">
                           
                       <table class="table">
    	                       
                    <tr>
                        <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-5 control-label">เลขที่บัตรประชาชน :</label>
                         <div class="col-sm-7">
                         <asp:TextBox ID="TextBox14" runat="server" class="form-control" placeholder="3-1304-00102-91-2" ReadOnly ="true"></asp:TextBox>
                          </div>
                        </div><!-- /.form-group --> </td>
 
                        
                       <td class="actions" style=" text-align :left ;"><div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">คำนำหน้า: </label>
                       <div class="col-sm-8">
                       <asp:TextBox ID="TextBox1" runat="server" class="form-control" placeholder="นาย" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td> 
    		          </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-5 control-label ">ชื่อ :</label>
                     <div class="col-sm-7">
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control" placeholder="ธีรเจต" ReadOnly ="true"></asp:TextBox></div>
                     </div><!-- /.form-group --></td>
  
                    <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                    <div class="col-sm-8">
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control" placeholder="ภู่ระหงษ์" ReadOnly ="true"></asp:TextBox></div>
                  </div><!-- /.form-group --> </td>
  
                  </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-5 control-label">บ้านเลขที่ :</label>
                         <div class="col-sm-7">
                         <asp:TextBox ID="TextBox4" runat="server" class="form-control" placeholder="122" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">หมู่ที่ :</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox5" runat="server" class="form-control" placeholder="3" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-5 control-label ">ตำบล :</label>
                        <div class="col-sm-7">
                        <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="บึงชำอ้อ" ReadOnly ="true"></asp:TextBox></div>
                        </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">จังหวัด :</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox7" runat="server" class="form-control" placeholder="ปทุมธานี" ReadOnly ="true"></asp:TextBox>
                         </div>
                       </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-5 control-label">อาชีพ :</label>
                        <div class="col-sm-7">
                        <asp:TextBox ID="TextBox8" runat="server" class="form-control" placeholder="เกษตรกร" ReadOnly ="true"></asp:TextBox>
                    </div>
                  </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">การศึกษา :</label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="TextBox9" runat="server" class="form-control" placeholder="มัธยมศึกษา" ReadOnly ="true"></asp:TextBox>
                            </div>
                          </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                   
                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label"> เบอร์โทร :</label>
                      <div class="col-sm-7">
                          <asp:TextBox ID="TextBox11" runat="server" class="form-control" placeholder="0-2905-9522" ReadOnly ="true"></asp:TextBox>
                    </div>
                    </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :center ;"></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"></td>
                     <td class="actions" style=" text-align :left ;"></td> </tr>
                  </table>
                   
                        </div><!-- /.box-body -->
                    
               
                 
                 <b class="text-info">ประวัติการเข้าร่วมกิจกรรม</b><br />
                  <div class="box box-solid box-primary">
                   <div class="row">
                         <form class="form-horizontal">
                    <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label text-right">โครงการ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ค้นหาโครงการ</option>
                      <option>ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)</option>
                      <option>ถนนสายแยก ทช.สต.5041 - บ.ศาลเจ้า</option>
                      <option>ถนนสายแยก ทล.4051 - บ.หาดทรายยาว</option>
                      <option>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</option>
                      <option>อื่น</option>
                    </select></div>
                    </div>
                
                  </div><!-- /.box-body -->
                  
                </form><br />

                </div>
                    <div class="box-body no-padding">
                      <table class="table table-striped table-bordered">
                      <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ลำดับ</th>
                      <th class="actions" style=" text-align :center ;">การเข้าร่วมกิจกรรม</th>
                      <th class="actions" style=" text-align :center ;">ประเภทกิจกรรม</th>
                      <th class="actions" style=" text-align :center ;">วันที่-เวลา </th>
                      <th class="actions" style=" text-align :center ;">ชื่อกิจกรรม</th>
                      <th class="aactions" style=" text-align :center ;">ชื่อโครงการ</th>
                      <th class="actions" style=" text-align :center ;">ชื่อสายทาง</th>
                      <th class="actions" style=" text-align :center ;">ผลความคิดเห็น</th>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;">1</td>
                      <td class="actions" style=" text-align :center ;"><small>วางแผน</small></td>
                      <td class="actions" style=" text-align :center ;"><small>จัดประชุม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>10/01/2557 13.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมวางแผนโครงการ</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>
                     
                     <tr>
                      <td class="actions" style=" text-align :center ;">2</td>
                      <td class="actions" style=" text-align :center ;"><small>เสนอแผนงปม.ให้กรมพิจารณา</small></td>
                      <td class="actions" style=" text-align :center ;"><small>จัดประชุม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>12/07/2557 08.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมจัดเสนอแผนโครงการ</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทช.สต.5041 - บ.ศาลเจ้า</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">ลบ</span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;">3</td>
                      <td class="actions" style=" text-align :center ;"><small>เสนอแผนงปม.ให้กรมพิจารณา</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมระดมสมอง</small></td>
                      <td class="actions" style=" text-align :center ;"><small>12/08/2557 09.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมโครงการเพื่อแก้ปัญหา</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.4051 - บ.หาดทรายยาว</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;">4</td>
                      <td class="actions" style=" text-align :center ;"><small>จัดจ้างก่อสร้าง</small></td>
                      <td class="actions" style=" text-align :center ;"><small>จัดประชุม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>22/05/2556 10.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมวางแผน</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;">5</td>
                      <td class="actions" style=" text-align :center ;"><small>การก่อสร้าง</small></td>
                      <td class="actions" style=" text-align :center ;"><small>แบบสอบถาม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>2/09/2556 11.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 2 งานดิน</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>
                  </table>
                 </div>
                    </div>
              </div>
                </div>
                
                       
                <div class="modal-footer">
                <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
               <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
              </div><br />
                 
               </div>
            </div><!-- /.box-body -->
        </div>
             
    </section>
   </div>

</asp:Content>
