﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce7.aspx.cs" Inherits="DRR_Citizen.frm_techniqce7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
<div class="content-wrapper">
       
       <section class="content" runat ="server">
         
           <div class="row">
             <div class="col-md-12">
                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลโทรทัศน์ (CPM_UT0416)</b></h4></div>
             <div class="box-body">
                   <div class="box-body no-padding"style="overflow-x:auto;">
                    <table class="table table-bordered">
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                     </tr>
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>หัวข้อข่าว :</b></td>
                    <td width="80%"><div class="col-sm-12">
                          <div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                          </div></div></td>
                    </tr> 
                     <tr>
    		          <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		          <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
    		         </tr>

                    
                    
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการ :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
                     </tr>

                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>เวลา :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox7" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                     </div></div></div> </td>
                     </tr>

                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>สื่อที่ใช้เผยแพร่ :</b></td>
                     <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox8" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>

                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผู้แถลงข่าว :</b></td>
                     <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>

                  
                    <tr>
    		        <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
                      <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                      <textarea class="form-control" rows="7" placeholder="" disabled></textarea>
                     </div></div></td></tr>

                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
                     <td width="70%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
                     <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                      <!-----------------------------------------แนบเอกสาร------------------------------------>               
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>แนบเอกสาร :</b>
                         <br /> <br />
                           <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td> 
                       
                      <td>
                          <div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                      <div class="timeline-body"><br />
                               
                    </div></div> </td> 
                    </tr>


                  </table>
                </div>
                </div><!-- /.box-body -->
        
                
                    <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   </div>

              </div>

               </div>    
            </div>
          
        </section><!-- /.content -->

    </div>
      
      
</asp:Content>
