﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_stakeholders1.aspx.cs" Inherits="DRR_Citizen.frm_stakeholders1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper">
      
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
          <div class="col-md-12">  
               <div class="box box-solid box-info">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลรายชื่อผู้มีส่วนได้ส่วนเสีย (CPM_UT0501)</b></h4></div>
             <div class="box-body">
                 <div class="row">
   
              
                     <div class="col-md-6">
                       <form class="form-horizontal">
                         <div class="box-body">

                           <div class="form-group">
                                <label for="inputEmail3" class="col-sm-5 control-label text-right">ปีงบประมาณ :</label>
                                <div class="col-sm-7">
                                    <select class="form-control select2" style="width: 100%;">
                                    <option>2559 (2213 โครงการ)</option>
                                    <option>2558 (4122 โครงการ)</option>
                                    <option selected="selected">2557 (1659 โครงการ)</option>
                                    <option>2556 (11 โครงการ)</option>
                                    <option>2555 (1 โครงการ)</option>
                                <option>ดูทั้งหมด</option>
                            </select></div>
                          </div><br />
                  
                     <div class="form-group"><br />
                 <label for="inputEmail3" class="col-sm-5 control-label text-right">สังกัดหน่วย :</label>
                   <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">สำนักงานทางหลวงชนบทที่ 1 (ปทุมธานี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 2 (สระบุรี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 3 (ชลบุรี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 4 (เพชรบุรี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 5 (นครราชสีมา)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 6 (ขอนแก่น)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 7 (อุบลราชธานี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 8 (นครสวรรค์)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 9 (อุตรดิตถ์)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 10 (เชียงใหม่)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 11 (สุราษฏร์ธานี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 12 (สงขลา)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 13 (ฉะเชิงเทรา)</option>
                      <option>ประเภท อื่นๆ</option>
                    </select></div><br />
                  </div><!-- /.form-group -->

                     <div class="form-group"><br />

                    <label for="inputEmail3" class="col-sm-5 control-label text-right">เทคนิคการมีส่วนร่วม :</label>
                        <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">แผ่นป้ายประชาสัมพันธ์</option>
                      <option> Website</option>
                      <option> E-mail</option>
                      <option>แถลงข่าว</option>
                      <option>วิทยุ</option>
                      <option>หนังสือพิมพ์</option>
                      <option>โทรทัศน์</option>
                      <option>จดหมายข่าว</option>
                      <option>การจัดประชุม</option>
                    </select></div>
                  </div><!-- /.form-group -->  <br />
                     <div class="form-group"> <br />
                  
                    <label for="inputEmail3" class="col-sm-5 control-label text-right">ความเห็น :</label>
                        <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ทั้งหมด</option>
                      <option>บวก</option>
                      <option>ลบ</option>
                    </select></div>
                  </div><!-- /.form-group -->
                  </div><!-- /.box-body --> 
                  
                </form>
                </div>

                     <div class="col-md-6">
                     
                   <form class="form-horizontal">
                  <div class="box-body">
                  

                     <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label">กระบวนงาน :</label>
                        <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">วางแผน</option>
                      <option>สำรวจออกแบบ</option>
                      <option>เสนอแผนงปม.ให้กรมพิจารณา</option>
                      <option>เวนคืนอสังหาริมทรัพย์</option>
                      <option>จัดจ้างก่อสร้าง</option>
                      <option>การก่อสร้าง</option>
                      <option>คืนค้ำประกันสัญญา</option>
                    </select></div>
                  </div><!-- /.form-group -->
                 
                       <div class="form-group">
                   <label for="inputEmail3" class="col-sm-5 control-label">หน่วยดำเนินงาน :</label>
                        <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">แขวงทางหลวงชนบทนนทบุรี</option>
                      <option>แขวงทางหลวงชนบทปทุมธานี</option>
                      <option>แขวงทางหลวงชนบทพระนครศรีอยุธยา  </option>
                      <option>แขวงทางหลวงชนบทสมุทรปราการ</option>
                      <option>แขวงทางหลวงชนบทอ่างทอง</option>
                      <option>แขวงทางหลวงชนบทชัยนาท</option>
                      <option>แขวงทางหลวงชนบทลพบุรี</option>
                      <option>แขวงทางหลวงชนบทสระบุรี</option>
                      <option>แขวงทางหลวงชนบทสิงห์บุรี</option>
                      <option>แขวงทางหลวงชนบทจันทบุรี</option>
                      <option>แขวงทางหลวงชนบทชลบุรี</option>
                      <option>แขวงทางหลวงชนบทตราด</option>
                      <option>แขวงทางหลวงชนบทระยอง</option>
                      <option>ประเภท อื่นๆ</option>
                    </select></div>
                  </div><!-- /.form-group --> 

                       <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label ">ประเภทผู้มีส่วนได้ส่วนเสีย :</label>
                        <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ทั้งหมด</option>
                      <option>Key</option>
                      <option>Primary</option>
                      <option>Seconary</option>
                    </select></div>
                  </div><!-- /.form-group --> 

                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label"><small>ค้นหา :</small></label>
                        <div class="col-sm-7">
                    <div class="input-group" style="width: 100%;">
                      <input type="text" name="table_search" class="form-control input-sm " placeholder="ค้นหาตามชื่อ-นามสกุล,รหัสสายทาง,ชื่อสายทาง......">
                     
                    </div>
                  </div></div><!-- /.form-group -->
                  </div><!-- /.box-body -->
                   
                </form>
                
                </div>

                </div>
               </div>
            <div class="row no-print">
               <button class="btn bg-yellow pull-right" style="margin-right: 30px;"><b><i class="fa fa-search"></i></b>  ค้นหา </button></div><br /><br />
         <!-----------------Table----------------------->
      
                <div class="box-body no-padding">
                     <table class="table table-bordered table-striped table-hover">
                       <tr class="bg-blue-gradient">
                      <TH COLSPAN="4" class="actions" style=" text-align :center ;">ข้อมูล</th>
                      <TH COLSPAN="8" class="actions" style=" text-align :center ;">การมีส่วนร่วม</th> 
                     <TH COLSPAN="2" class="actions " style=" text-align :center ;">ดำเนินการ<small></small></th>
                      </tr>
                      <tr class="bg-blue-gradient">
                      <th class="actions " style=" text-align :center ;"><small>ลำดับ</small></th>
                      <th class="actions " style=" text-align :center ;"><small>ชื่อ</small></th>
                      <th class="actions " style=" text-align :center ;"><small>นามสกุล</small></th>
                      <th class="actions " style=" text-align :center ;"><small>จังหวัด</small></th>
                      <th class="actions " style=" text-align :center ;"><small>วางแผน</small></th>
                      <th class="actions " style=" text-align :center ;"><small>สำรวจออกแบบ</small></th>
                      <th class="actions " style=" text-align :center ;"><small> เสนอแผนงปม.ให้กรมพิจารณา</small></th>
                      <th class="actions " style=" text-align :center ;"><small>เวนคืนอสังหาริมทรัพย์</small></th>
                      <th class="actions " style=" text-align :center ;"><small>จัดจ้างก่อสร้าง</small></th>
                      <th class="actions " style=" text-align :center ;"><small>การก่อสร้าง</small></th>
                      <th class="actions " style=" text-align :center ;"><small>คืนค้ำประกันสัญญา</small></th>
                      <th class="actions " style=" text-align :center ;"><small>รวม</small></th>
                      <th class="actions " style=" text-align :center ;"><small>แก้ไข</small></th>
                      <th class="actions " style=" text-align :center ;"><small>ลบ</small></th>  
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>ธีรเจต</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>ภู่ระหงษ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>ขอนแก่น</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>เจนจิรา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ทองมี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นครราชสีมา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>8</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>3</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>พัฒนา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>สาไร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ชลบุรี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>6</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>เอกบัณฑิต</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>หวังธรรมมมั่ง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ชลบุรี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>3</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>7</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>5</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>อรพรรณ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>รุ่งเรือง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นครราชสีมา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>3</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>8</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>6</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>กนกพร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>จั่นเรไร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นครราชสีมา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>5</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>จักรพงษ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>จำเนียรทรัพย์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ระยอง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>7</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>
                        <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>8</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ธีรเจต</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ภู่ระหงษ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ระยอง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>9</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>9</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>เจนจิรา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ทองมี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ชลบุรี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>7</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ฐาปกรณ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ถาดสูงเนิน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>พังงา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>3</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>0</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>9</small></a> </td>
                      <td><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                       <td><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                  </table>
                </div><!-- /.box-body -->
               
                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนหลัง</a></li>
                  </ul>
                </div>
             
           
            </div><!-- /.box-body -->
        
              </div>

       
           </div>
        
     </div>
    </section>
   </div>

</asp:Content>
