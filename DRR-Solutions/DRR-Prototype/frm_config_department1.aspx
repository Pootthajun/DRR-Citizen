﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_department1.aspx.cs" Inherits="DRR_Citizen.frm_config_department1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">

                 <div class="box box-solid box-primary">
      
                      <div class="box-body">
        <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลหน่วยงาน (CPM_UT0201)</b></h4></div>
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">สังกัดหน่วย</th>
                      <th class="actions" style=" text-align :center ;">หน่วยดำเนินงาน</th>
                      <th class="actions" style=" text-align :center ;">ดู</th>
                     </tr>
                    <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 1 (ปทุมธานี)</td> 
                      <td>แขวงทางหลวงชนบทนนทบุรี<br />
                          แขวงทางหลวงชนบทปทุมธานี <br />
                          แขวงทางหลวงชนบทพระนครศรีอยุธยา<br />
                          แขวงทางหลวงชนบทสมุทรปราการ<br />
                          แขวงทางหลวงชนบทอ่างทอง
                      </td> 
                      <th class="actions" style=" text-align :center ;"><a href="frm_config_department2.aspx"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></a></th>
                  </tr>
                    <tr>
                      <td> สำนักงานทางหลวงชนบทที่ 2 (สระบุรี)</td> 
                      <td> แขวงทางหลวงชนบทชัยนาท<br />
                           แขวงทางหลวงชนบทลพบุรี<br />
                           แขวงทางหลวงชนบทสระบุรี <br />
                          แขวงทางหลวงชนบทสิงห์บุรี<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                   
                    <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 3 (ชลบุรี)</td> 
                      <td>แขวงทางหลวงชนบทจันทบุรี<br />
                          แขวงทางหลวงชนบทชลบุรี<br />
                          แขวงทางหลวงชนบทตราด<br />
                          แขวงทางหลวงชนบทระยอง<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th> </tr>
                   <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 4 (เพชรบุรี)</td> 
                      <td>แขวงทางหลวงชนบทประจวบคีรีขันธ์<br />
                          แขวงทางหลวงชนบทเพชรบุรี <br />
                          แขวงทางหลวงชนบทราชบุรี<br />
                          แขวงทางหลวงชนบทสมุทรสงคราม<br />
                          แขวงทางหลวงชนบทสมุทรสาคร<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                   <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 5 (นครราชสีมา)</td> 
                      <td>แขวงทางหลวงชนบทชัยภูมิ<br />
                          แขวงทางหลวงชนบทนครราชสีมา<br />
                          แขวงทางหลวงชนบทบุรีรัมย์<br />
                          แขวงทางหลวงชนบทสุรินทร์<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                   <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 6 (ขอนแก่น)</td> 
                      <td>แขวงทางหลวงชนบทขอนแก่น<br />
                          แขวงทางหลวงชนบทมหาสารคาม<br />
                          แขวงทางหลวงชนบทร้อยเอ็ด<br />
                          แขวงทางหลวงชนบทเลย<br />
                      </td> 
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 7 (อุบลราชธานี)</td> 
                      <td>แขวงทางหลวงชนบทอำนาจเจริญ<br />
                          แขวงทางหลวงชนบทยโสธร<br />
                          แขวงทางหลวงชนบทศรีสะเกษ<br />
                          แขวงทางหลวงชนบทอุบลราชธานี<br />
                      </td> 
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 8 (นครสวรรค์)</td> 
                      <td>แขวงทางหลวงชนบทกำแพงเพชร<br />
                          แขวงทางหลวงชนบทตาก<br />
                          แขวงทางหลวงชนบทนครสวรรค์<br />
                          แขวงทางหลวงชนบทพิจิตร<br />
                          แขวงทางหลวงชนบทอุทัยธานี<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th> </tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 9 (อุตรดิตถ์)</td> 
                      <td>แขวงทางหลวงชนบทพิษณุโลก<br />
                          แขวงทางหลวงชนบทเพชรบูรณ์<br />
                          แขวงทางหลวงชนบทสุโขทัย<br />
                          แขวงทางหลวงชนบทดอุตรดิตถ์<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 10 (เชียงใหม่)</td> 
                      <td>แขวงทางหลวงชนบทเชียงใหม่<br />
                          แขวงทางหลวงชนบทแพร่<br />
                          แขวงทางหลวงชนบทแม่ฮ่องสอน <br />
                          แขวงทางหลวงชนบทลำปาง<br />
                          แขวงทางหลวชนบทลำพูน<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 11 (สุราษฏร์ธานี)</td> 
                      <td>แขวงทางหลวงชนบทชุมพร<br />
                          แขวงทางหลวงชนบทนครศรีธรรมราช<br />
                          แขวงทางหลวงชนบทระนอง<br />
                          แขวงทางหลวงชนบทสุราษฏร์ธานี<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 12 (สงขลา)</td> 
                      <td>แขวงทางหลวชนบทนราธิวาส<br />
                          แขวงทางหลวงชนบทปัตตานี<br />
                          แขวงทางหลวงชนบทหวัดยะลา<br />
                          แขวงทางหลวงชนบทสงขลา<br />
                          แขวงทางหลวงชนบทสตูล<br />
                      </td> 
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th> </tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 13 (ฉะเชิงเทรา)</td> 
                      <td>แขวงทางหลวงชนบทฉะเชิงเทรา<br />
                          แขวงทางหลวงชนบทนครนายก<br />
                          แขวงทางหลวงชนบทปราจีนบุรี<br />
                          แขวงทางหลวงชนบทสระแก้ว<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 14 (กระบี่)</td> 
                      <td>แขวงทางหลวงชนบทกระบี่<br />
                          แขวงทางหลวงชนบทหวัดตรัง<br />
                          แขวงทางหลวงชนบทพังงา<br />
                          แขวงทางหลวงชนบทพัทลุง<br />
                          แขวงทางหลวงชนบทดภูเก็ต<br />
                      </td> 
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                      <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 15 (อุดรธานี)</td> 
                      <td>แขวงทางหลวงชนบทบึงกาฬ<br />
                          แขวงทางหลวงชนบทหนองคาย<br />
                          แขวงทางหลวงชนบทหนองบัวลำภู<br />
                          แขวงทางหลวงชนบทอุดรธานี<br />
                      </td> 
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th> </tr>
                       <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 16 (กาฬสินธุ์)</td> 
                      <td>แขวงทางหลวงชนบทกาฬสินธุ์<br />
                          แขวงทางหลวงชนบทนครพนม<br />
                          แขวงทางหลวงชนบทมุกดาหาร<br />
                          แขวงทางหลวงชนบทสกลนคร<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                       <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 17 (เชียงราย)</td> 
                      <td>แขวงทางหลวงชนบทเชียงราย<br />
                          แขวงทางหลวงชนบทน่าน<br />
                          แขวงทางหลวงชนบทพะเยา<br />
                      </td> 
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th></tr>
                       <tr>
                      <td>สำนักงานทางหลวงชนบทที่ 18 (สุพรรณบุรี)</td> 
                      <td>แขวงทางหลวงชนบทกาญจนบุรี<br />
                          แขวงทางหลวงชนบทนครปฐม<br />
                          แขวงทางหลวงชนบทสุพรรณบุรี<br />
                      </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-search" data-widget="add" data-toggle="tooltip" title="ค้นหา"></i></span></th> </tr>
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
        
              </div>
           
            

          </div></div>
        </section><!-- /.content -->
      </div>
</asp:Content>
