﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_process2.aspx.cs" Inherits="DRR_Citizen.frm_config_process2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
             <div class="box box-solid box-primary">
                    <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลกระบวนงาน (CPM_UT0204)</b></h4></div>
 
                    <form class="form-horizontal">
                        <div class="col-md-6">

                    
                        <div class="box-body">
                
                  <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right"><small>รหัสกระบวนงาน :</small></label>
                      <div class="col-sm-8">
                       <asp:TextBox ID="input1" runat="server" class="form-control text-left" placeholder="01 " ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                  
                      </div></div>

                        <div class="col-md-6">

                    
                        <div class="box-body">
                
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right"><small>ชื่อกระบวนงาน :</small></label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="วางแผน" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                        
                      </div></div>
                    </form>
                       </div><br />
               
                     <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  </div><!-- /.box -->
             
            </div><!-- /.box-body -->
            </div>
              
        </section><!-- /.content -->
      </div>
</asp:Content>
