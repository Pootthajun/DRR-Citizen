﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_t.aspx.cs" Inherits="DRR_Citizen.frm_question_t" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>


<div class="content-wrapper">
       

<!-- Main content ----------------------------------------------------------->
        <section class="content">


         <div class="row">
          <div class="col-md-12">
            
                <div class="box box-solid box-primary">
     
            <div class="box-body">
             <div class="box-header with-border">
             <h4 class="pull-left"><b>ข้อมูลประเภทแผ่นพับ (CPM_UT0603)</b></h4></div>

             <div class="box-body">
                 <div class="row">
                         <form class="form-horizontal">
                                        <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">ค้นหาแผ่นพับ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ก่อสร้างทาง</option>
                      <option>ก่อสร้างสะพาน</option>
                      <option>บำรุงรักษา</option>
                      <option>อำนวยควมปลอดภัย</option>
                      <option>ซ่อมบำรุง</option>
                      <option>อื่น</option>
                    </select></div>
                    </div>
                
                  
                  </div><!-- /.box-body -->
                  
                </form>

                </div><div class="box-body">
               <br /><div class="row">
              <div class="col-md-1"></div>

                   <div class="col-md-10">

                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-condensed">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                     </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t1.aspx">ท 1 </a></td>
                      <td><a href="frm_question_t1.aspx">ประชนมีส่วนร่วม</a></td>  
                    </tr>
                 
                       <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t2.aspx">ท 2 </a></td>
                      <td><a href="frm_question_t2.aspx">งานดิน</a></td>  
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t31.aspx">ท 3.1 </a></td>
                      <td><a href="frm_question_t31.aspx">งานท่อกลม กสล.</a></td> 
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t32.aspx">ท 3.2 </a></td>
                      <td><a href="frm_question_t32.aspx">งานท่อลอดเหลี่ยม</a></td> 
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t4.aspx">ท 4</a></td>
                      <td><a href="frm_question_t4.aspx">งานลูกรัง</a></td>  
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t51.aspx">ท 5.1 </a></td>
                      <td><a href="frm_question_t51.aspx">งานหินคลุก</a></td> 
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t52.aspx">ท 5.2 </a></td>
                      <td><a href="frm_question_t52.aspx">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</a></td> 
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t53.aspx">ท 5.3 </a></td>
                      <td><a href="frm_question_t53.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_t61.aspx">ท 6.1 </a></td>
                      <td><a href="frm_question_t61.aspx">งานผิวทางแอสฬลต์คอนกรีต</a></td> 
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t62.aspx">ท 6.2</a></td>
                      <td><a href="frm_question_t62.aspx">งานผิวทางคอนกรีต</a></td> 
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t63.aspx">ท 6.3 </a></td>
                      <td><a href="frm_question_t63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t7.aspx">ท 7</a></td>
                      <td><a href="frm_question_t7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td>  
                    </tr>
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->

              <div class="col-md-1"></div>

            </div><!-- /.box-body --><br />
          
        </div> </div>
                  </div></div>      
         
            
            </div>
        </div>

    </section>
   </div>
</asp:Content>
