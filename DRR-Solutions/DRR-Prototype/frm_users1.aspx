﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_users1.aspx.cs" Inherits="DRR_Citizen.frm_users1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
<div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
          <div class="col-md-12">  

             <div class="box box-solid box-primary">
<!-----------------------------------------------------ส่วนค้นหา---------------------------------------------------->   
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลรายชื่อ อสทช. (CPM_UT0503)</b></h4></div>
             <div class="box-body">
                 <div class="row">
   
                     <div class="col-md-6">
                        <form class="form-horizontal">
                        <div class="box-body">
                   <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label text-right"><small>สังกัดหน่วย :</small></label>
                        <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</option>
                      <option> สำกงานทางหลวงชนบทที่ 2</option>
                      <option> สำกงานทางหลวงชนบทที่ 3</option>
                      <option> สำกงานทางหลวงชนบทที่ 4</option>
                      <option> สำกงานทางหลวงชนบทที่ 5</option>
                      <option> สำกงานทางหลวงชนบทที่ 6</option>
                      <option> สำกงานทางหลวงชนบทที่ 7</option>
                      <option> สำกงานทางหลวงชนบทที่ 8</option>
                      <option> สำกงานทางหลวงชนบทที่ 9</option>
                      <option> สำกงานทางหลวงชนบทที่ 10</option>
                      <option> สำกงานทางหลวงชนบทที่ 11</option>
                      <option> สำกงานทางหลวงชนบทที่ 12</option>
                      <option> สำกงานทางหลวงชนบทที่ 13</option>
                      <option> สำกงานทางหลวงชนบทที่ 14</option>
                      <option> สำกงานทางหลวงชนบทที่ 15</option>
                      <option> สำกงานทางหลวงชนบทที่ 16</option>
                    </select></div><br />
                  </div><!-- /.form-group -->
                    
                            <div class="form-group"><br />
                    <label for="inputEmail3" class="col-sm-3 control-label text-right"><small>สถานะ :</small></label>
                        <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ทั้งหมด</option>
                      <option>Active</option>
                      <option>Inactive</option>
                    </select></div><br />
                  </div><!-- /.form-group -->

                        </div><!-- /.box-body -->
                     </form>
                  </div>

                     <div class="col-md-6">
                     
                   <form class="form-horizontal">
                  <div class="box-body">
                  
                     <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label"><small>หน่วยดำเนินงาน :</small></label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">แขวงทางหลวงชนบทปทุมธานี</option>
                      <option>แขวงทางหลวงชนบทนนทบุรี</option>
                      <option>แขวงทางหลวงชนบทปทุมธานี</option>
                      <option>แขวงทางหลวงชนบทพระนครศรีอยุธยา  </option>
                      <option>แขวงทางหลวงชนบทสมุทรปราการ</option>
                      <option>แขวงทางหลวงชนบทอ่างทอง</option>
                      <option>แขวงทางหลวงชนบทชัยนาท</option>
                      <option>แขวงทางหลวงชนบทลพบุรี</option>
                      <option>แขวงทางหลวงชนบทสระบุรี</option>
                      <option>แขวงทางหลวงชนบทสิงห์บุรี</option>
                      <option>แขวงทางหลวงชนบทจันทบุรี</option>
                      <option>แขวงทางหลวงชนบทชลบุรี</option>
                      <option>แขวงทางหลวงชนบทตราด</option>
                      <option>แขวงทางหลวงชนบทระยอง</option>
                      <option>ประเภท อื่นๆ</option>
                    </select></div>
                  </div><!-- /.form-group -->
                      
                      <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label"><small>ค้นหา :</small></label>
                        <div class="col-sm-8">
                    <div class="input-group" style="width: 100%;">
                      <input type="text" name="table_search" class="form-control input-sm " placeholder="ค้นหาตามชื่อ-นามสกุล,เบอร์โทร,วันที่เข้าเป็นสมาชิก">
                     
                    </div>
                  </div></div><!-- /.form-group -->

                  </div><!-- /.box-body -->
                   
                  </form>
                </div>
            
               </div>

            <div class="row no-print">
               <a href="New_project.aspx" class="btn bg-aqua pull-right" style="margin-right: 30px;"><i class="fa fa-plus"> เพิ่ม</i></a>
               <button class="btn bg-yellow pull-right" style="margin-right: 20px;"><span><b><i class="fa fa-search"></i></b>  ค้นหา </span> </button></div><br /><br />
    
  <!-----------------------------------------------------ส่วนตาราง---------------------------------------------------->
              <div class="box-body no-padding">
                   <table class="table table-bordered tab-content table-hover">
                   <tr class="bg-blue-gradient">
                    </tr>
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;"><small>ลำดับ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ชื่อ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>นามสกุล</small></th>
                      <th class="actions" style=" text-align :center ;"><small>เบอร์โทร</small></th>
                      <th class="actions" style=" text-align :center ;"><small>สังกัดหน่วย</small></th>
                      <th class="actions" style=" text-align :center ;"><small>หน่วยดำเนินงาน</small></th> 
                      <th class="actions" style=" text-align :center ;"><small>วันเข้าเป็นสมาชิก</small></th>
                      <th class="actions" style=" text-align :center ;"><small>สถานะ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ระยะเวลาการเป็นสมาชิก</small></th>
                      <th class="actions" style=" text-align :center ;"><small>แก้ไข</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ลบ</small></th>
                      
                    </tr>
                
                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>1 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>เอกชัย </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ลิ้มเจริญอนันต์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>0-2905-9522</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>15/01/2555</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4 ปี 4 เดือน</small></a> </td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                    
                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>2 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-1712-3195</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>  
                      <td class="actions" style=" text-align :center ;"><a href="#"><small> ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4/06/2553</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2 ปี 5 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                      
                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>3 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สมยศ  </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แสงมณี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-1712-3195</small></a></td>
                      <td><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>  
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>20/03/2552</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2 ปี 5 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                    
                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>4 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>วีระ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ชื่นทรัพย์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-7915-4843</small></a></td>
                      <td><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>  
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>31/12/2550</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4 ปี 5 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>5 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-1712-3195</small></a></td> 
                      <td><a href="#"><small> สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td> 
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>14/04/2551</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>5 ปี 5 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>6 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-1712-3195</small></a></td>
                      <td><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>  
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>23/01/2554</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small> 8 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>7 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-1712-3195</small></a></td>
                     <td><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>  
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>15/01/2554</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>3 ปี 1 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                    
                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>8 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>วีระ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ชื่นทรัพย์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-7915-4843</small></a></td>
                      <td><a href="#"><small>สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td>  
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>31/01/2552</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>6 ปี 11 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>9 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>08-1712-3195</small></a></td> 
                      <td><a href="#"><small> สำกงานทางหลวงชนบทที่ 1 ปทุมธานี</small></a> </td> 
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปทุมธานี</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>14/02/2553</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4 ปี 8 เดือน</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
             <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
                </div>
            </div><!-- /.box-body -->
        
              </div>
  
           </div>
        
     </div>
    </section>
   </div>



</asp:Content>
