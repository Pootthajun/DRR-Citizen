﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_b.aspx.cs" Inherits="DRR_Citizen.frm_question_b" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>


<div class="content-wrapper">
      
<!-- Main content ----------------------------------------------------------->
        <section class="content">


         <div class="row">
          <div class="col-md-12">
            
                <div class="box box-solid box-info ">
                    
                      <div class="box-body">
             <div class="box-body">
                 <div class="row">
                         <form class="form-horizontal">
                                        <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">ค้นหาแผ่นพับ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option ">ประเภท ท</option>
                      <option selected="selected">ประเภท บ</option>
                      <option>ประเภท ส</option>
                      <option>ประเภท ป</option>
                      <option>ประเภท ซ</option>
                      <option>อื่น</option>
                    </select></div>
                    </div>
                
                  
                  </div><!-- /.box-body -->
                  
                </form>

                </div></div>
                  </div></div>      
              <div class="box box-solid box-info ">  
         <div class="box-body">
               <br /><div class="row">
              <div class="col-md-1"></div>

                   <div class="col-md-10">

                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b1.aspx">บ 1 </a></td>
                      <td><a href="frm_techniqce_9b1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b21.aspx">บ 2.1 </a></td>
                      <td><a href="frm_techniqce_9b21.aspx">งานท่อกลม กสล.</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                     <td><a href="frm_techniqce_9t1.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b22.aspx">บ 2.2 </a></td>
                      <td><a href="frm_techniqce_9b22.aspx">งานท่อลอดเหลี่ยม</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b3.aspx">บ 3 </a></td>
                      <td><a href="frm_techniqce_9b3.aspx">งานดิน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b4.aspx">บ 4</a></td>
                      <td><a href="frm_techniqce_9b4.aspx">งานลูกรัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p51.aspx">บ 5.1</a></td>
                      <td><a href="frm_techniqce_9p51.aspx">งานหินคลุก</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p52.aspx">บ 5.2 </a></td>
                      <td><a href="frm_techniqce_9p52.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t53.aspx">ท 5.3 </a></td>
                      <td><a href="frm_techniqce_9t53.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b53.aspx">บ 5.3 </a></td>
                      <td><a href="frm_techniqce_9b53.aspx">งานซ่อมสร้างผิวทางแอสฟัลต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b61.aspx">บ 6.1 </a></td>
                      <td><a href="frm_techniqce_9b61.aspx">งานไพร์มโคท</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b62.aspx">บ 6.2</a></td>
                      <td><a href="frm_techniqce_9b62.aspx">งานแทคโคท</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b63.aspx">บ 6.3 </a></td>
                      <td><a href="frm_techniqce_9b63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b64.aspx">บ 6.4</a></td>
                      <td><a href="frm_techniqce_9b64.aspx">งานผิวทางแอสฟัสต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b65.aspx">บ 6.5 </a></td>
                      <td><a href="frm_techniqce_9b65.aspx">งานผิวทางคอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b66.aspx">บ 6.6</a></td>
                      <td><a href="frm_techniqce_9b66.aspx">งานเสริมผิวทางลาดยางแอสฟัสต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b7.aspx">บ 7 </a></td>
                      <td><a href="frm_techniqce_9b7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b81.aspx">บ 8.1</a></td>
                      <td><a href="frm_techniqce_9b81.aspx"> งานสำรวจสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b82.aspx">บ 8.2 </a></td>
                      <td><a href="frm_techniqce_9b82.aspx">งานฐานรากสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b83.aspx">บ 8.3</a></td>
                      <td><a href="frm_techniqce_9b83.aspx">งานตอม่อสะพาน</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b84.aspx">บ 8.4 </a></td>
                      <td><a href="frm_techniqce_9b84.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                       <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b85.aspx">บ 8.5</a></td>
                      <td><a href="frm_techniqce_9b85.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p16.aspx">บ 8.6</a></td>
                      <td><a href="frm_techniqce_9b86.aspx">งานทางเท้าและราวสะพาน</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b87.aspx">บ 8.7</a></td>
                      <td><a href="frm_techniqce_9b87.aspx">งานพื้นคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p16.aspx">บ 8.8</a></td>
                      <td><a href="frm_techniqce_9b88.aspx">งานส่วนประกอบอื่นๆ ของสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->

              <div class="col-md-1"></div>

            </div><!-- /.box-body --><br />
          
        </div></div> 
            
            </div>
        </div>

    </section>
   </div>

   </div>

</asp:Content>

