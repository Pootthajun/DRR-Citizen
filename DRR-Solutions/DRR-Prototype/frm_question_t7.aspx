﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_t7.aspx.cs" Inherits="DRR_Citizen.frm_question_t7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

     <script src="../../plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="box-header">
                
                  <h4>เทคนิคการมีส่วนร่วม</h4>
                </div>
           <ol class="breadcrumb">
            <li><a href="Dashboard.aspx">หน้าหลัก</a></li>
            <li><a href="frm_techniqce_9t.aspx">แบบสอบถามประเภทการสร้างทาง</a></li>
            <li class="active"> ท3.1 : งานท่อกลม คสล.</li>
          </ol>
        </section>

 
       <section class="content" runat ="server">
         

              <div class="row">
            <div class="col-md-12">
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">กรอกข้อมูล</a></li>
                  <li><a href="#tab_2" data-toggle="tab">แผ่นพับ</a></li>
                  
                 
                  <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1">
              
                  <div class="row">
               
                <div class="col-xs-12">
                    <div class="box box-solid box-info"><br />
                    <b class="text-info">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อมูลผู้ทำโครงการ</b> 
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                             <div class="box-body">
        
                                <div class="box-body">
                                 <div class="row">

                                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label pull-left">โครงการก่อสร้างถนนสาย :</label>
                      <div class="col-sm-8">
                        <input type="email" class="form-control" id="input" placeholder="โครงการสำรวจอสังหาริมทรัพย์ สาย นย.3007 แยก ทล.305-บ.คลอง33">
                      </div>
                    </div>
                                    <div class="col-md-6">
                                        <form class="form-horizontal">
                                        <div class="box-body">

                       <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label"></label>
                      <div class="col-sm-9">
                        
                      </div>
                    </div><br />
                    
                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">อำเภอ :</label>
                        <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">เมือง</option>
                      <option>Alaska</option>
                      <option>California (disabled)</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select></div>
                  </div><!-- /.form-group -->
                  
                  </div><!-- /.box-body -->
                  
                </form>
                </div>

                                    <div class="col-md-6">
                     
                   <form class="form-horizontal">
                  <div class="box-body"><br />
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label"></label>
                      <div class="col-sm-9">
                        
                      </div>
                    </div>

                        <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">จังหวัด :</label>
                        <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">นครราชสีมา</option>
                      <option>Alaska</option>
                      <option>California (disabled)</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select></div>
                  </div><!-- /.form-group --> 
                    
                    
                  </div><!-- /.box-body -->
                  
                </form>
                
                </div>
    
                                </div>
                                </div>
                              </div>

                            <b class="text-info">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้อมูลโครงการ</b>
                            </li>
                     
          <div class="box-body">
        
            <div class="box-body">
              <div class="row">

                    <div class="col-md-6">
                      <form class="form-horizontal">
                        <div class="box-body">

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ชื่อผู้ควบคุมงาน :</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control text-left" id="inputPassword3" placeholder="นางทัศนีย์ ทนงแผลง">
                      </div>
                    </div>
                    
                            <div class="box-body">
                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label"></label>
                      <div class="col-sm-9">
                        
                      </div>
                    </div></div>

                            <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้มีส่วนร่วม :</label>
                      <div class="col-sm-8">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="นายศักรินทร์ แสนธิ">
                      </div>
                    </div>
                    
                            <div class="form-group">
                           <label for="inputPassword3" class="col-sm-4 control-label">ที่อยู่ :</label>
                              <div class="col-sm-7">
                              <div class="form-group">
                               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <textarea class="form-control" rows="3" placeholder="จังหวัดนครราชสีมา"></textarea>
                              </div>
                            </div>
                          </div>

                      </div></form>
                </div>

                    <div class="col-md-6">
                      <form class="form-horizontal">
                         <div class="box-body">

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">โทรศัพท์ :</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control" id="phone" placeholder="043-5457766">
                      </div>
                    </div>

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">วันที่ร่วมกิจกรรม :</label>
                      <div class="col-sm-8">
                        <input type="password" class="form-control" id="date" placeholder="22/04/2557">
                      </div>
                    </div>

                           <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">ตำบล :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">xxxxxxxx</option>
                      <option>Alaska</option>
                      <option>California (disabled)</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select></div>
                  </div><!-- /.form-group -->
                           <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">อำเภอ :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">เมือง</option>
                      <option>Alaska</option>
                      <option>California (disabled)</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select></div>
                  </div><!-- /.form-group -->
                           <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">นครราชสีมา</option>
                      <option>Alaska</option>
                      <option>California (disabled)</option>
                      <option>Delaware</option>
                      <option>Tennessee</option>
                      <option>Texas</option>
                      <option>Washington</option>
                    </select></div>
                  </div><!-- /.form-group -->
                  
                         </div><!-- /.box-body -->
                       </form>
                </div>
             </div>                 
              <div class="pull-right">
                                        <button type="submit" class="btn btn-info">บึกทึกข้อมูล</button>
                                        <button type="submit" class="btn btn-warning">ยกเลิก</button>
                                    </div><!-- /.box-footer -->
            </div>
         </div>
                    
                    </ul>
              
                   </div><!-- /.form-box -->
   
                </div>
           </div>
                    </div>

                  <div class="tab-pane" id="tab_2">
                   
                    <div class="row">

            <div class="col-xs-1">
              </div>

                   <div class="col-xs-10">

                      <div class="box box-solid box-warning">
                        <div class="box-body box-profile">
                  
                            <h3 class="profile-username">แผ่นพับ ท3.1 : งานท่อกลม คสล.</h3>
                           
                            <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">
                                    <div class="form-group">
                                                <h5 class="text-blue">&nbsp;&nbsp;&nbsp;1. ท่อที่นำมาใช้งานมีลักษณะเป็นอย่างไร</h5>
                                            
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;มีผิวเรียบ ไม่มีรอยร้าว </label><br/>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มีผิวเรียบ มีรอยร้าว</label><br/>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;มีตรา มอก. รับรอง </label><br/>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มีตรา มอก. รับรอง </label><br/>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มี</label><br/>
                                                </div><center><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                  <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                  <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br /></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                       <h5 class="text-blue">&nbsp;&nbsp;&nbsp;2. มีการตรวจสอบสภาพท่อที่นำมาใช้งานหรือไม่</h5>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;มีการตรวจสอบ</label><br/>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มีการตรวจสอบ </label><br />
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" />ขูดไม่ออก </label><br />
                                             </div>
                                            <center><br /><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br /></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue">&nbsp;&nbsp;&nbsp;3. การเทปูนรองท่อได้บดอัดดินท้องร่องหรือไม่</h5>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;บดอัด</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่บดอัด</label><br/>
                                                <label for="inputName" class="col-sm-3 control-label text-right">กิโลเมตรที่ :</label>
                                                       <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                                            <div class="input-group-addon"> <i class="fa fa-road"></i></div>
                                                         </div><!-- /.input group -->
                                                        </div><br />
                                          
                                      </div> <center><br /><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br /></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                            <h5 class="text-blue">&nbsp;&nbsp;&nbsp;4. มีการทดสอบความแน่นดินในท้องร่องที่ขุดหรือไม่</h5>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;ทดสอบ</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่ทดสอบ</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ทดสอบบางจุด</label><br/>
                                        
                                    </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                         <h5 class="text-blue">&nbsp;&nbsp;&nbsp;5. ก่อนวางท่อได้เทปูน รองท่อและกั้นแบบเทปูนหรือไม่</h5>
                                         
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;เทปูนรอง </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่เทปูนรอง </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;มีการกั้นแบบ</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มีการกั้นแบบ</label><br/>
                                        
                                        </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue">&nbsp;&nbsp;&nbsp;6. ขณะนำทำท่อแต่ละท่อนมาวางต่อกันมีการสอปูนภายในรางลิ้นท่อหรือไม่</h5>
                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;มี </label><br/>
                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มี</label><br/>
                                     </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <center><br /><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br />
                                       <h5 class="text-blue">&nbsp;&nbsp;&nbsp;7. การวางท่อแต่ละแถวมีลักษณะเป็นแนวเส้นตรงหรือไม่</h5>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox"> แนวตรง</label><br/>
                                                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" checked="checked""> แนวไม่ตรง </label>
                                    </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue">&nbsp;&nbsp;&nbsp;8. มีการยาวางแนวท่อก่อนฝังกลบหรือไม่</h5>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;ยาแนวภายนอก </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ยาแนวภายใน </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่มีการยาแนว</label><br/>
                                    </div></li>

                                     <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue">&nbsp;&nbsp;&nbsp;9. มีการเทปูนล็อคข้างท่อหรือไม่</h5> 
                                              <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;บดอัด</label><br/>
                                              <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่บดอัด</label><br/>
                                                <label for="inputName" class="col-sm-3 control-label text-right">กิโลเมตรที่ :</label>
                                                       <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                                            <div class="input-group-addon"> <i class="fa fa-road"></i></div>
                                                         </div><!-- /.input group -->
                                                        </div><br />
                                           
                                    </div><center><br /><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br /></li>

                                     <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue">&nbsp;&nbsp;&nbsp;10. การวางท่อมากกว่า 1 แถว ระยะห่างระหว่างแถว</h5>
                                            <label for="inputName" class="col-sm-3 control-label text-right">ระยะ 1-2 ห่าง:</label>
                                            <div class="col-sm-3">
                                            <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'"placeholder="1 กิโลเมตร">
                                            </div><br /><br />
                                              
                                             <label for="inputName" class="col-sm-3 control-label text-right">ระยะ 2-3 ห่าง:</label>
                                                    <div class="col-sm-3">
                                                    <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" placeholder="1 กิโลเมตร">
                                                    </div><!-- /.input group --><br />
                                       </div><center><br /><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br /></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                         <h5 class="text-blue">&nbsp;&nbsp;&nbsp;11. การถมข้างท่อใช้วัสดุอะไร มีการบดอัดเป็นขั้นและมีการทดสอบความแน่นแต่ละชั้นหรือไม่</h5>
                                          
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;ใช้ทรายถม</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ใช้ดินถม </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;บออัดเป็นชั้นๆ</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;บดอัดเฉพาะด้านบน </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ทดสอบแต่ละชั้น </label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่ทดสอบ </label><br/>
                                       
                                        </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                          <h5 class="text-blue">&nbsp;&nbsp;&nbsp;12. ท่อที่ไม่มีการป้องกันการกัดเซาะของน้ำบริเวณปลายท่อจะเกิดความเสียหายตามที่ปรากฎในรูปข้างล่าง ในถนนสายนี้ท่านพบเห็นบ้างหรือไม่</h5>
                                           <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;ไม่พบ</label><br />
                                           <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;พบ</label><br />
                                              <label for="inputName" class="col-sm-3 control-label text-right">กิโลเมตรที่ :</label>
                                                       <div class="col-sm-3">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                                            <div class="input-group-addon"> <i class="fa fa-road"></i></div>
                                                         </div><!-- /.input group -->
                                                        </div><br />
                                           
                                            </div><center><br /><img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <img src="dist/img/drr-1/9t1.png" WIDTH=250 HEIGHT=130></center><br /></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                          <h5 class="text-blue">&nbsp;&nbsp;&nbsp;13. มีการติตั้งป้ายเตือน เครื่องหมายจราจรชั่วคราวหรือไม่</h5>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red" checked="checked">&nbsp;&nbsp;&nbsp;มี  จำนวนเพียงพอ</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;มี  จำนวนไม่เพียงพอ</label><br/>
                                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" class="flat-red">&nbsp;&nbsp;&nbsp;ไม่ติดตั้ง</label><br/>
                                         
                                     </div></li>

                                </ul>

                                <div class="pull-right">
                                    <button class="btn btn-info">บันทึกข้อมูล</button> 
                                    <button type="submit" class="btn btn-warning">ยกเลิก</button>
                                </div>
                  
                  
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>

                     <div class="col-xs-1">
                    </div>     

                </div>

                  </div><!-- /.tab-pane -->
                  
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            </div><!-- /.col -->

           
          </div> <!-- /.row -->

 </section><!-- /.content -->

    </div>
      
</asp:Content>
