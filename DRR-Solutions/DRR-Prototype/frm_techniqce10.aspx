﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce10.aspx.cs" Inherits="DRR_Citizen.frm_techniqce10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
       <section class="content" runat ="server">
   

           <div class="row">
               <div class="col-md-12">
                <div class="box box-solid box-primary">
                <div class="box-body">
                <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลประกาศตามระเบียบการ (CPM_UT0422)</b></h4></div>
              <div class="box-body">
                <div class="row">
             
                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-condensed">
                     <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                     </tr>
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ชื่อการดำเนินงาน :</b></td>
                     <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="ทางหลวงชนบท เร่งสร้างถนน ทล.118–บ้านทุ่งยาว หนุนโครงการพระราชดำริ" ReadOnly ="true"></asp:TextBox></div></div></td>
                     </tr>
                 
                     <tr>
    		          <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		          <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;">
                    <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="30/01/2559" ReadOnly ="true"></asp:TextBox>
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
    		         </tr>
                    
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการ :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="1/04/2559" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
                     </tr>

                   <tr>
    		         <td class="actions" style=" text-align :right ;"><b>จากวันที่-ถึงวันที่ :</b></td>
                     <td><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                     <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div></div></div> 

                         <div class="col-sm-5"> <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox8" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                     <div class="input-group-addon"><i class="fa fa-calendar"></i>
                     </div></div></div> 
                     </td>
                    </tr>

                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>สถานที่ดำเนินการ :</b></td>
                     <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox7" runat="server" class="form-control text-left" placeholder="ห้องประชุม ชั้น 3" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                  
                    <tr>
    		        <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
                      <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                      <textarea class="form-control" rows="9" placeholder="ทางหลวงชนบท เร่งสร้างถนนสาย ทล.118–บ้านทุ่งยาว จ.เชียงราย ระยะทางกว่า 36 กม. หวัง แก้ไขปัญหาการเดินทางของประชาชนที่มีความยากลำบาก สนับสนุนการท่องเที่ยว-โครงการพระราชดำริ... วันที่ 1 เม.ย. 59 นายพิศักดิ์ จิตวิริยะวศิน อธิบดีกรมทางหลวงชนบท เปิดเผยว่า กรมทางหลวงชนบทมีนโยบายที่จะสนับสนุนทางเข้าโครงการพระราชดำริและการท่องเที่ยว อำเภอเวียงป่าเป้า ดังนั้นจึงได้จัดสรรงบประมาณในการก่อสร้างถนนสาย ทล.118 - บ้านทุ่งยาว จ.เชียงราย กว่า 36 กิโลเมตร เพื่อสนับสนุนการท่องเที่ยวและแก้ไขปัญหาในการเดินทางของประชาชน สนับสนุนโครงการพระราชดำริ ทั้งนี้ ถนนพื้นที่ดังกล่าว เป็นเขตติดต่อระหว่างจังหวัดเชียงรายและจังหวัดเชียงใหม่ และพื้นที่ส่วนใหญ่เป็นภูเขาสูงชันมีอุทยานแห่งชาติตั้งอยู่ในพื้นที่หลายแห่ง จึงเป็นสถานที่ท่องเที่ยว
                      สำหรับนักท่องเที่ยวและมีประชากรตั้งหมู่บ้านอยู่ตามภูเขาสูงหลายหมู่บ้าน โดยมีอาชีพทางด้านเกษตรกรรม โดยในการขนส่งพืชผลทางการเกษตร มีความยากลำบาก เนื่องจากถนนเดิมเป็นถนนดินและแคบยากต่อการสัญจรมากในฤดูฝน ประกอบกับบริเวณโครงการมีโครงการพระราชดำริตั้งอยู่ภายในบริเวณโครงการ" disabled></textarea>
                     </div></div></td></tr>
                    
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
                      <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
                      <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="ปัจจุบันโครงการดังกล่าว ได้รับการจัดตั้งงบประมาณประจำปีงบประมาณ 2559" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <!-----------------------------------------แนบเอกสาร------------------------------------>               
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>แนบเอกสาร :</b>
                         <br /> <br />
                           <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td> 
                         
                         <%--<div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment"> </div>--%>
    		        
                      <td>
                          <div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                      <div class="timeline-body"><br />
                                <div class="attachments">
							<ul><li>
									<span class="label label-info">pdf</span> <b>รายงาน.pdf</b> <i>(2.5MB)</i>
                                     &nbsp;&nbsp;&nbsp;<span class="label label-danger"> <i class="fa fa-close" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span>
									<span class="quickMenu">
										<a href="#" class="glyphicons search"><i></i></a>
										<a href="#" class="glyphicons share"><i></i></a>
										<a href="#" class="glyphicons cloud-download"><i></i></a>
									</span>
								</li>
								<li>
									<span class="label label-info">pdf</span> <b>รายชื่อ.pdf</b> <i>(5KB)</i>
                                     &nbsp;&nbsp;&nbsp;<span class="label label-danger"> <i class="fa fa-close" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span>
									<span class="quickMenu">
										<a href="#" class="glyphicons search"><i></i></a>
										<a href="#" class="glyphicons share"><i></i></a>
										<a href="#" class="glyphicons cloud-download"><i></i></a>
									</span>
								</li>
								
							</ul>		
						</div>
                    </div></div> </td> 
                    </tr>

<!-----------------------------------------แนบรูปภาพ------------------------------------> 
                     <tr>
    		        <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                            <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                      <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                      <div class="input-group" style="width: 100%;">
                      <div class="timeline-body"><br />
                            <img src="dist/img/news/t4.jpg" width="150" height="150" /> 
                          
                             <img src="dist/img/news/t2.jpg" width="150" height="150">
                      
                    </div></div>
                       </div></td> 
                    </tr>
                  </table>
           </div><!-- /.box-body -->
 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div> 
                </div><!-- /.box-body -->
        
                 </div><!-- ./col -->
                  
                </div>
                </div>
              </div>

               </div>    
            </div>
                 
        </section><!-- /.content -->

    </div>
</asp:Content>
