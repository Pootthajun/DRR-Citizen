﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_process1.aspx.cs" Inherits="DRR_Citizen.frm_config_process1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
      

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">

                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลกระบวนงาน (CPM_UT0203)</b></h4></div>
                          <div class="box-body">
          <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มกระบวนงาน</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">

                      <th class="actions " style=" text-align :center ;">รหัสกระบวนงาน</th>
                      <th class="actions " style=" text-align :center ;">ชื่อกระบวนงาน</th>
                      <th class="actions" style=" text-align :center ;">แก้ไข</th>
                     </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">01</td> 
                      <td class="actions" style=" text-align :center ;">วางแผน</td> 
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_process2.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">02</td> 
                      <td class="actions " style=" text-align :center ;">สำรวจและออกแบบ </td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                   
                    <tr>
                      <td class="actions" style=" text-align :center ;">03</td> 
                      <td class="actionsg" style=" text-align :center ;">เสนอแผนงปม.ให้กรมพิจารณา</td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                  <tr>
                      <td class="actions " style=" text-align :center ;">04</td> 
                      <td class="actions" style=" text-align :center ;">เวนคืนอสังหาริมทรัพย์ </td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                   <tr>
                      <td class="actionsg" style=" text-align :center ;">05</td> 
                      <td class="actions " style=" text-align :center ;">จัดจ้างก่อสร้าง</td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">06</td> 
                      <td class="actions " style=" text-align :center ;">การก่อสร้าง </td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                   <tr>
                      <td class="actions" style=" text-align :center ;">07</td> 
                      <td class="actions " style=" text-align :center ;">คืนค้ำประกันสัญญา </td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                   
                  </table>
                              </div>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
          </div></div>
        </section><!-- /.content -->
      </div>

</asp:Content>
