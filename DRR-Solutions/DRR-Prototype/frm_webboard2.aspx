﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_webboard2.aspx.cs" Inherits="DRR_Citizen.frm_webboard2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   
<div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
           
          <div class="col-md-12">  

      <div class="box box-widget widget-user-2">
                 <div class="widget-user-header bg-teal-active">
                  <div class="widget-user-image">
                    <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username">Adul</h3>
                  <h5 class="widget-user-desc"> <strong><i class="fa fa-user margin-r-5"></i>Newbie</strong></h5> 
                     
                </div>
             
             <div class="box box-solid box-teal">

     
            <div class="box-footer no-padding">
                  <ul class="nav nav-stacked">
                    <li><a href="#">เนินสะดุดชะลอความเร็ว ทำเองได้มั๊ย<span class="pull-right badge bg-blue">  เมื่อ: ตุลาคม 05, 2012, 04:03:20 pm »</span><span class="pull-right badge bg-yellow">(อ่าน 2082 ครั้ง) </span><span class="pull-right badge bg-green">กระทู้: 1   ชื่อจริง: อดุลย์</span></a></li>
                <li>    <textarea class="textarea" placeholder="สวัสดีครับ

                        ผมมีเรื่องอยากจะสอบถามเรื่องเนินสะดุด ถนนในหมู่บ้าน
                        เรื่องก็คือวัยรุ่นในหมู่บ้านจำนวนมาก ขับขี่รถมอเตอร์ไซค์น่าหวาดเสียว เร็ว เสียงดัง เป็นที่รบกวนผู้คนโดยทั่วไป และเส้นทางที่ใช้คือหน้าร้านขายของชำประจำหมู่บ้านเพราะเส้นทางตรงได้ระยะประลองความเร็ว ช่วงเช้า/เย็นผู้คนจะออกมาจ่ายตลาดซื้อกับข้าวกันจำนวนมาก เกรงจะเกิดอันตราย เคยเกิดอุบัติเหตุ ชนหมา ล้ม บาดเจ็บ แต่ยังไม่เคยชนใคร(ทุกคนก็ต้องระวังตัว) ผมไม่อยากให้เกิดอุบัติเหตุจนต้องสูญเสีย เคยปรึกษากับ อบต. ว่าจะทำเนินสะดุดได้หรือไม่ ระยะประมาณ 50 เมตรก่อนถึงร้านขายของชำ เพื่อให้วัยรุ่นต้องชะลอความเร็ว เพราะที่อื่นๆเขาก็มีกัน อบต. บอกว่าไม่สามารถทำได้ เพราะถ้ารถมอเตอร์ไซค์ล้ม คนทำเนินสะดุดจะมีความผิด อ้าว! แล้วยังงี้ถ้าลูกหลานโดนรถมอเตอร์ไซค์ชนตาย จะว่ายังงัย ก็ได้แต่รับรู้ว่าต้องไปบอกพ่อแม่เด็กวัยรุ่นเอาเอง ทั้งรุ่นเล็ก รุ่นใหญ่ และที่กำลังจะเป็นอีก ไม่รู้เท่าไร คนแถวนี้ก็แปลก ใครไปตำหนิลูกหลานไม่ได้ ไม่ห้ามปราม หนำซ้ำยังจะชังน้ำหน้าเราอีก เรื่องราวก็ประมาณนี้ละครับ   จึงอยากเรียนถามกรมทางหลวงชนบทว่า ถ้าจะทำเนินสะดุดในกรณีอย่างนี้ กรมทางหลวงชนบทจะกรุณาสร้างให้ได้หรือไม่ ต้องทำเรื่องเสนออย่างไร ใครเป็นคนเสนอ อยากให้ช่วยแนะนำด้วย 

                        เส้นทางปัญหาอยู่ที่
                        ม.7 บ้านบัวแดง ต.หม้อยหมอนทอง อ.กำแพงแสน จ.นครปฐม ทางหลวงชนบทหมายเลข 4040

                        ขอบคุณครับ

                        อดุลย์
                        คนบัวแดง" style="width: 100%; height: 300px; font-size: 12px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea></li>  
                  </ul>
                </div>

        
            </div><!-- /.box-body -->
        
              </div>

              <div class="box box-widget widget-user-2">
                <div class="widget-user-header bg-gray-active">
                  <div class="widget-user-image">
                    <img class="img-circle" src="../dist/img/user6-128x128.jpg" alt="User Avatar">
                  </div><!-- /.widget-user-image -->
                  <h3 class="widget-user-username">adminapp</h3>
                  <h5 class="widget-user-desc"> <strong><i class="fa fa-user margin-r-5"></i>Approve Moderator</strong></h5> 
                     
                </div>
               <div class="box box-solid box-teal">

                  <ul class="nav nav-stacked">

                    <li><a href="#">Re: เนินสะดุดชะลอความเร็ว ทำเองได้มั๊ย <span class="pull-right badge bg-orange"> « Reply #1 เมื่อ: ตุลาคม 12, 2012, 03:55:30 pm »</span></a></li>
                <li>    <textarea class="textarea" placeholder="เรียนผู้ใช้นามว่า Adul  ศูนย์ระฆัง กรมทางหลวงชนบท ได้ตรวจสอบแล้วปรากฎว่า ถนนสายดังกล่าวเป็นถนนที่อยู่ในความรับผิดชอบขององค์การบริหารส่วนตำบลห้วยหมอนทอง และข้อร้องเรียนดังกล่าว ทางศูนย์ระฆังฯ ได้ประสานแจ้งให้องค์การบริหารส่วนตำบลห้วยหมอนทองทราบแล้ว เพื่อจะได้ดำเนินการในส่วนที่เกี่ยวข้องต่อไป ขอขอบคุณที่ท่านได้แสดงข้อคิดเห็นมา ณ โอกาสนี้" 
                    style="width: 100%; height: 120px; font-size: 12px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea></li>  
                  </ul>
             
            </div><!-- /.box-body -->
          </div>
             <%-- <div class="box box-solid box-teal">

     
                  <ul class="nav nav-stacked">
                      <li><h3 class="widget-user-username">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h3>
                  <h5 class="widget-user-desc"> <strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-user margin-r-5"></i>ชื่อ-นามสกุล</strong></h5></li>
                    <li><a href="#">เรื่อง</a></li>
                <li>    <textarea class="textarea" placeholder="ตอบ..." 
                    style="width: 100%; height: 120px; font-size: 12px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea></li>  
                  </ul>
                </div>--%>

        
            </div><!-- /.box-body -->
              
            
           </div>
        
    </section>
   </div>

</asp:Content>
