﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_timeline2.aspx.cs" Inherits="DRR_Citizen.frm_timeline2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

     <div class="content-wrapper" style="min-height: 1096px;">
       
<!-- Main content ------------------------------------------------------------------------->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
                   <div class="box box-solid box-primary">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลโครงการที่เกี่ยวกับสายทาง (CPM_UT0304)</b></h4></div>
          
             <div class="box-body">
                 <div class="row">

                <!-- Project name ----------------------------->
                <section id="advice">
                    
                        <div class="col-md-12">
                           <div class="box-body">
                        <div class="row">

                    <form class="form-horizontal">
                      <div class="col-md-12">
                      <div class="box-body">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">ชื่อโครงการ :</label>
                        <div class="col-sm-10">
                           <asp:TextBox ID="input1" runat="server" class="form-control text-left" placeholder="แยกทางหลวงหมายเลข11 (กม.ที่3+800) - บ้านทองเอน" ReadOnly ="true"></asp:TextBox>
                        </div> 
                      </div>
                     </div></div>

                      <div class="col-md-4">
                             <div class="box-body">
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label"><small>ปีงบประมาณ</small></label>
                        <div class="col-sm-8">
                               <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="2557" ReadOnly ="true"></asp:TextBox></div>
                       
                       </div><!-- /.form-group --> 
                       </div></div>
                        
                      <div class="col-md-4">
                             <div class="box-body">
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label"><small>รหัสสายทาง</small></label>
                        <div class="col-sm-8">
                          <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --> 
                       </div></div>
                 
                      <div class="col-md-4">
                             <div class="box-body">
                   
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-6 control-label text-right"><small>ระยะทาง(กิโลเมตร)</small></label>
                        <div class="col-sm-6">
                            <input type="email" class="form-control" id="inputkm" placeholder="-" disabled></div>
                       </div><!-- /.form-group --> 
                       </div><!-- /.box-body --> </div>

                      <div class="col-md-6">
                    
                        <div class="box-body">

                                <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">สังกัด </label>
                      <div class="col-sm-8">
                                       <asp:TextBox ID="TextBox11" runat="server" class="form-control text-left" placeholder="สำนักงานทางหลวงชนบทที่ 2 (สระบุรี)" ReadOnly ="true"></asp:TextBox></div>
                      </div><br />
                   
                           <div class="form-group"><br />
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">หน่วยงาน </label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox12" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทสิงห์บุรี" ReadOnly ="true"></asp:TextBox></div>
                      </div></div>
                      
                      </div><br />
               
                      <div class="col-md-6">
                    
                        <div class="box-body">
                    
                     <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right"><small>หัวหน้าชุดซ่อมคนที่ 1 </small></label>
                      <div class="col-sm-8">
                           
                         <asp:TextBox ID="TextBox13" runat="server" class="form-control text-left" placeholder="นายถวิล จิตร์บำรุง" ReadOnly ="true"></asp:TextBox></div>
                     
                      </div><br />
                   
                     <div class="form-group"><br />
                      <label for="inputPassword3" class="col-sm-4 control-label text-right"><small>หัวหน้าชุดซ่อมคนที่ 2</small> </label>
                      <div class="col-sm-8">
                          
                      <asp:TextBox ID="TextBox14" runat="server" class="form-control text-left" placeholder="นายอาณัติ  สะสมทรัพย์" ReadOnly ="true"></asp:TextBox></div>
                      </div></div>
                   </div>
                     
                      <div class="col-md-12">
                      <div class="box-body">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-3 control-label">ความก้าวหน้าโครงการ  (80%) </label>
                        <div class="col-sm-9">
                           
                         <div class="box-body text-center">
                            <div class="progress">
                            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                            <span class="sr-only">100% Complete</span>
                         </div>
                        </div>
                    </div><!-- /.box-body --> </div> 
                      </div>
                        <div class="col-md-6">
                    
                        <div class="box-body">
                    
                            <h5 class="header pull-left"><b> สรุปกิจกรรมการมีส่วนร่วม</b></h5>
                       <div class="box-body no-padding">
                            <table class="table table-bordered tab-content table-hover">
    	                        <tr>
                                <th class="actions bg-gray" style=" text-align :center ;"><b>ระดับ</b></th>
    		                    <th class="actions bg-gray" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></th>
                                <th class="actions bg-gray" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></th>
    		                    <td class="actions bg-gray" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></td>
    		                    <td class="actions bg-gray" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></td>
    		                    <td class="actions bg-gray" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><b>จำนวนครั้ง</b></td>
    		                    <td class="actions" style=" text-align :center ;"><b>2</b></td>
                                <td class="actions" style=" text-align :center ;"><b>0</b></td>
                                <td class="actions" style=" text-align :center ;"><b>1</b></td>
                                <td class="actions" style=" text-align :center ;"><b>0</b></td>
                                <td class="actions" style=" text-align :center ;"><b>0</b></td>
    		                    </tr>
                                
                            </table>
                         </div>
                      </div></div>

                        <div class="col-md-6">
                    
                        <div class="box-body">

                            <h5 class="header pull-left"><b>ตารางสรุปแบบสอบถาม</b></h5>

                       <div class="box-body no-padding">
                             <table class="table table-bordered tab-content table-hover">
    	                        <tr>
    		                    <th class="actions bg-primary" style=" text-align :center ;"><small>จำนวนที่แจกทั้งหมด</small></th>
                                <th class="actions bg-primary" style=" text-align :center ;"><small>จำนวนที่ต้องแก้ไข</small></th>
    		                    <th class="actions bg-primary" style=" text-align :center ;"><small>จำนวนที่แก้ไขแล้ว</small></th>
    		                    <th class="actions bg-primary" style=" text-align :center ;"><small>จำนวนที่ยังไม่แก้ไข</small></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><b>12</b></td>
                                <td class="actions" style=" text-align :center ;"><b>4</b></td>
                                <td class="actions" style=" text-align :center ;"><b>2</b></td>
                                <td class="actions" style=" text-align :center ;"><b>2</b></td>
    		                    </tr>
                                
                            </table>
                         </div>
                         
                      </div></div>
                     </div>
            </div>
                   
                    </form>
                
                  </div>
                </div>
                       </div>
                </section> 
           
                </div>
            </div>
            </div><!-- /.box-body -->

                            <div class="panel-body">
                            <ul class="nav nav-tabs">
                            <li class="active"><a href="#detail" data-toggle="tab" aria-expanded="true">รายละเอียดโครงงาน</a></li>
                            <li><a href="#plan" data-toggle="tab" aria-expanded="false">ต้นทาง-ปลายทาง</a></li>
                            <li><a href="#act" data-toggle="tab" aria-expanded="false">การมีส่วนร่วม</a></li>
                            <li><a href="#stakeholders" data-toggle="tab" aria-expanded="false">ผู้มีส่วนได้ส่วนเสีย</a></li>
                            <li><a href="#question" data-toggle="tab" aria-expanded="false">แผ่นพับ</a></li>
                            </ul>
    
<!--------- Tab ------------------------------------------------------>

       <div class="tab-content">

        <div class="tab-pane active" id="detail">
            
         <br /> <div class="box box-solid box-primary">

       <div class="box-body">
             <div class="box-body">
                 <div class="row">

                    <form class="form-horizontal">
                        
                      <div class="col-md-6">
                    
                        <div class="box-body">

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">เลขที่สัญญา </label>
                      <div class="col-sm-8">
                        <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                        
                      </div>
                    </div>
                    
                             <div class="form-group">
                      <label for="inputEmail3" class="col-sm-5 control-label">เริ่มต้นสัญญาวันที่ </label>
                      <div class="col-sm-7">
                        <div class="input-group">
                        <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                      </div>
                    </div>

                      </div></div>
               
                      <div class="col-md-6">
                    
                         <div class="box-body">

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">วันที่ลงสัญญา </label>
                      <div class="col-sm-8">
                       <div class="input-group">
                           <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div>
                      </div>
                    </div>

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">สิ้นสุดสัญญาวันที่ </label>
                      <div class="col-sm-8">
                        <div class="input-group">
                            <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div>
                      </div>
                    </div>

                         </div></div>

                      <div class="col-md-6">
                    
                        <div class="box-body">
                            <div class="form-group">
                        <label for="inputName" class="col-sm-6 control-label">งบประมาณทั้งสื้น(ตามสัญญา)</label>
                        <div class="col-sm-6">
                            <asp:TextBox ID="TextBox15" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                         </div> 
                      </div>

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-5 control-label text-left">วันเบิกเงินงวดสุดท้าย </label>
                      <div class="col-sm-7">
                                    <div class="input-group">
                                <asp:TextBox ID="TextBox16" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div>
                    </div></div>
                    
                             <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">รับประกันงาน </label>
                       <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;"disabled>
                      <option selected="selected" >1  ปี</option>
                      <option>2  ปี</option>
                    </select></div>
                    </div>
                    
                         <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">สังกัด </label>
                      <div class="col-sm-8">
                        <div class="input-group">
                        <asp:TextBox ID="TextBox17" runat="server" class="form-control text-left" placeholder="สำนักงานทางหลวงชนบทที่ 2 (สระบุรี)" ReadOnly ="true"></asp:TextBox>
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->     
                      </div>
                    </div><br />

                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ชื่อผู้ควบคุมที่ 1 </label>
                      <div class="col-sm-8">
                           <div class="input-group">
                               <asp:TextBox ID="TextBox18" runat="server" class="form-control text-left" placeholder="นายถวิล  จิตร์บำรุง" ReadOnly ="true"></asp:TextBox>
                             
                       <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                      </div>
                    </div>
                     <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ชื่อผู้ควบคุมที่ 2 </label>
                      <div class="col-sm-8">
                           <div class="input-group">
                               <asp:TextBox ID="TextBox19" runat="server" class="form-control text-left" placeholder="นายอาณัติ  สะสมทรัพย์" ReadOnly ="true"></asp:TextBox>
                             
                       <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                      </div>
                    </div>
                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">สังกัด </label>
                      <div class="col-sm-8">
                          <div class="input-group">
                              <asp:TextBox ID="TextBox20" runat="server" class="form-control text-left" placeholder="สำนักงานทางหลวงชนบทที่ 2 (สระบุรี)" ReadOnly ="true"></asp:TextBox>
                           
                       <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                      </div>
                    </div>

                         <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">เบอร์โทรศัพท์ </label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox21" runat="server" class="form-control text-left" placeholder="086-9434842" ReadOnly ="true"></asp:TextBox>
                        
                      </div>
                    </div>
                      </div></div>
               
                      <div class="col-md-6">
                    
                         <div class="box-body">
                                <div class="form-group">
                        <label for="inputName" class="col-sm-6 control-label">งบประมาณทั้งสื้น(ตาม พรบ.) </label>
                        <div class="col-sm-6">
                        <asp:TextBox ID="TextBox22" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                         </div> 
                      </div>

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-5 control-label">วันส่งงานงวดสุดท้าย </label>
                      <div class="col-sm-7">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBox23" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                                 <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                                    </div>
                             </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-5 control-label">วันคืนค้ำประกันสัญญา</label>
                                 <div class="col-sm-7">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBox24" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                      
                                    <div class="input-group-addon">
                                     <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                                    </div>
                                </div>

                        <div class="form-group">   
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">หน่วยงาน</label>
                      <div class="col-sm-8">
                          <div class="input-group">
                               <asp:TextBox ID="TextBox25" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทสิงห์บุรี" ReadOnly ="true"></asp:TextBox>
                       <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                                    
                      </div>
                    </div>
                             
                             <div class="form-group"><br />
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ตำแหน่ง</label>
                      <div class="col-sm-8">
                          <div class="input-group">
                               <asp:TextBox ID="TextBox26" runat="server" class="form-control text-left" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                            <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                      </div>
                    </div>
                               <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ตำแหน่ง</label>
                      <div class="col-sm-8">
                          <div class="input-group">
                          <asp:TextBox ID="TextBox27" runat="server" class="form-control text-left" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                            <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                      </div>
                    </div>

                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">หน่วยงาน</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox28" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทสิงห์บุรี" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>

                         </div></div>
                      
                    </form>
                
                  
                </div>
            </div></div>
            </div><!-- /.box-body -->
        
       </div>

         <div class="tab-pane" id="plan">
        
        <div class="box-body">
        
         <div class="box-body">
              <div class="row">
             <div class="row no-print"><center>
                   <button class="btn btn-success pull-right" style="margin-right: 20px;"> เพิ่มข้อมูล</button></center> </div> <br />  
                 <div class="box-body no-padding">
                    <table class="table table-bordered tab-content table-hover">
    	            <tr>
                      <TH COLSPAN="1" class="actions bg-blue" style=" text-align :center ;">ช่วงที่</th>
                      <TH COLSPAN="3" class="actions bg-blue" style=" text-align :center ;">จุดเริ่มต้น</th>
                      <TH COLSPAN="3" class="actions bg-blue" style=" text-align :center ;">จุดสิ้นสุด</th>
                      <TH COLSPAN="2" class="actions bg-blue" style=" text-align :center ;">ดำเนินการ</th>
                    </tr>
                         <tr>
                      <th class="actions bg-blue" style=" text-align :center ;"><small></small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>ละติจูด</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>ละติจูด</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>กิโลเมตรที่</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>ลองติจูด</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>ลองติจูด</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>กิโลเมตรที่</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>แก้ไข</small></th>
                      <th class="actions bg-blue" style=" text-align :center ;"><small>ลบ</small></th>
                    </tr>
                     
                      <tr>
    		              <td class="actions" style=" text-align :center ;"><b>1</b></td>
    		              <td class="actions" style=" text-align :center ;"><b> 15.85000</b></td>
                          <td class="actions" style=" text-align :center ;"><b>104.633331</b></td>
                          <td class="actions" style=" text-align :center ;"><b>4</b></td>
                          <td class="actions" style=" text-align :center ;"><b> 15.85000</b></td>
                          <td class="actions" style=" text-align :center ;"><b>104.633331</b></td>
                          <td class="actions" style=" text-align :center ;"><b>20</b></td>
                          <td class="actions" style=" text-align :center ;"><button class="btn btn-warning btn-sm" data-widget="add" data-toggle="tooltip" title="แก้ไข"><i class="fa fa-pencil"></i></button></td>
                          <td class="actions" style=" text-align :center ;"><button class="btn btn-danger btn-sm" data-widget="add" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></button></td>

    		           </tr>
                     <tr>
    		              <td class="actions" style=" text-align :center ;"><b>2</b></td>
    		              <td class="actions" style=" text-align :center ;"><b>14.583333</b></td>
                          <td class="actions" style=" text-align :center ;"><b> 100.44999</b></td>
                          <td class="actions" style=" text-align :center ;"><b>21</b></td>
                          <td class="actions" style=" text-align :center ;"><b>14.583333</b></td>
                          <td class="actions" style=" text-align :center ;"><b> 100.44999</b></td>
                          <td class="actions" style=" text-align :center ;"><b>50</b></td>
                          <td class="actions" style=" text-align :center ;"><button class="btn btn-warning btn-sm" data-widget="add" data-toggle="tooltip" title="แก้ไข"><i class="fa fa-pencil"></i></button></td>
                          <td class="actions" style=" text-align :center ;"><button class="btn btn-danger btn-sm" data-widget="add" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></button></td>
    		           </tr>
                       <tr>
    		              <td class="actions" style=" text-align :center ;"><b>3</b></td>
    		              <td class="actions" style=" text-align :center ;"><b>14.350000</b></td>
                          <td class="actions" style=" text-align :center ;"><b>100.566666</b></td>
                          <td class="actions" style=" text-align :center ;"><b>51</b></td>
                          <td class="actions" style=" text-align :center ;"><b>14.350000</b></td>
                          <td class="actions" style=" text-align :center ;"><b>100.566666</b></td>
                          <td class="actions" style=" text-align :center ;"><b>75</b></td>
                          <td class="actions" style=" text-align :center ;"><button class="btn btn-warning btn-sm" data-widget="add" data-toggle="tooltip" title="แก้ไข"><i class="fa fa-pencil"></i></button></td>
                          <td class="actions" style=" text-align :center ;"><button class="btn btn-danger btn-sm" data-widget="add" data-toggle="tooltip" title="ลบ"><i class="fa fa-trash"></i></button></td>
    		           </tr>
                        
                            </table>
                    

                         </div><br /><br />
           
                  <form class="form-horizontal">
                       <div class="form-group">
                        
                       <br /><label class="col-sm-2 control-label">แผนที่ :</label>
                      <div class="col-sm-8">
                      <img id="zoom_05" src="../../dist/img/line.jpg" style="width: 100%;" data-zoom-image="../../dist/img/drr-2/map.png"> 
                    </div></div>
                     </form>
            
              </div><!-- /.row -->
            </div><!-- /.box-body -->
                              

        </div><!-- /.box-body -->
        </div>

         <div class="tab-pane" id="act">
            <div class="row">
            <div class="col-md-12">
              <div class="box box-solid">
               
                <div class="box-body">
                  <div class="box-group" id="accordion">
                   
                    <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5><a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><b>วางแผน   <span class="badge bg-fuchsia">2</span> </b></a></h5></div>
                      <div id="collapse1" class="panel-collapse collapse in">
                        <div class="box-body"> 
                     
                          <div class="timeline-item">
				        <li class="list-group-item">
					 <div class="timeline-body"><span class="time pull-left"><i class="fa fa-star text-yellow"></i>
				          <i class=" fa fa-calendar"></i> 07/10/2558 - 09:57</span><br />
						<h5 class="text-info"><b class="pull-left">ติดป้ายประชาสัมพันธ์ : ขทช.ลำปาง ติดตั้งป้ายประชาสัมพันธ์ประกาศผู้อำนวยการทางหลวงชนบท  </b><a class="pull-right"><img src="dist/img/TrashIt_icon_trnspt.png" width="30" height="30" style="margin-right: 10px;" data-widget="ลบข้อมูล" /> </a>
                        <a href="frm_techniqce1_2.aspx" class="pull-right"><img src="dist/img/edit_property.png" width="30" height="30" style="margin-right: 20px;" data-widget="แก้ไขข้อมูล" /></a></h5>
                          <br /><br /><b>รายละเอียดโครงการ</b> : แขวงทางหลวงชนบทลำปาง ได้ติดตั้งป้ายประชาสัมพันธ์ประกาศผู้อำนวยการทางหลวงชนบท เรื่อง ห้ามใช้ยานพาหนะที่มีน้ำหนัก น้ำหนักบรรทุก หรือน้ำหนักลงเพลาเกินกว่าที่ได้กำหนด หรือโดยที่ยานพาหนะนั้นอาจทำให้ทางหลวงเสียหาย เดินบนทางหลวงชนบทในเขตความรับผิดชอบของกรมทางหลวงชนบท
                         
                    </div></li>
                        <li class="list-group-item">
                         <div class="timeline-body"><span class="time pull-left"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i>
				          <b><a class="text-red">วันที่วางแผน</a></b>  <i class=" fa fa-calendar"></i> 11/02/2559 </span><br />
						<h5 class="text-info"><b class="pull-left"> จัดประชุม : ประชุมสัญจร ครั้งที่ 2/2559   </b><a class="pull-right"><img src="dist/img/TrashIt_icon_trnspt.png" width="30" height="30" style="margin-right: 10px;" data-widget="ลบข้อมูล" /> </a>
                        <a href="frm_techniqce16_2.aspx" class="pull-right"><img src="dist/img/edit_property.png" width="30" height="30" style="margin-right: 20px;" data-widget="แก้ไขข้อมูล" /></a></h5>
                          <br /><br /><b>รายละเอียดโครงการ</b> :เพื่อติดตามผลการเบิกจ่ายงบประมาณปี 2559  การเบิกจ่ายเงินกันไว้เบิกเหลื่อมปี 2558 และติดตามผลการปฏิบัติงานปี 2558 ผู้อำนวยการสำนักงานทางหลวงชนบทที่ 6 มอบหมายให้ นายสิทธิชัย  จันทน์เทศ ผู้อำนวยการแขวงทางหลวงชนบทร้อยเอ็ด เป็นประธานการประชุมในครั้งนี้ พร้อมด้วย ผอ.กลุ่ม/ส่วน , ผอ.ขทช.และผอ.บทช.ในสังกัด สำนักงานทางหลวงชนบทที่ 6 (ขอนแก่น) และเจ้าหน้าที่ที่เกี่ยวข้องเข้าร่วมประชุม ณ ห้องประชุมแขวงทางหลวงชนบทขอนแก่น
                         
                    </div></li><br />
					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
				    </div>
                       
                      </div>
                    </div></div>

                    <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5 > <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><b> สำรวจออกแแบบ <span class="badge bg-fuchsia">0</span>  </b></a></h5>
                      </div>
                      <div id="collapse2" class="panel-collapse collapse">
                        <div class="box-body">
                            <div class="timeline-item"> <b><h4 class="text-red">รอดำเนินการ......</h4></b></div>

					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
                        </div>
                      </div>
                    </div>

                    <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5><a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><b>เสนอแผน งปม.ให้กรมพิจารณา  <span class="badge bg-fuchsia">0</span>  </b></a></h5>
                      </div>
                      <div id="collapse3" class="panel-collapse collapse">
                         <div class="box-body">
                            <div class="timeline-item"> <b><h4 class="text-red">รอดำเนินการ......</h4></b></div>

					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
                        </div>
                      </div>
                    </div>

                     <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><b>เวนคืนอสังหาริมทรัพย์  <span class="badge bg-fuchsia">0</span> </b></a></h5>
                      </div>
                      <div id="collapse4" class="panel-collapse collapse">
                        <div class="box-body">
                            <div class="timeline-item"> <b><h4 class="text-red">รอดำเนินการ......</h4></b></div>

					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
                        </div>
                      </div>
                    </div>

                    <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5><a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><b>จัดจ้างก่อสร้าง  <span class="badge bg-fuchsia">0</span>  </b></a> </h5>
                      </div>
                      <div id="collapse5" class="panel-collapse collapse">
                         <div class="box-body">
                            <div class="timeline-item"> <b><h4 class="text-red">รอดำเนินการ......</h4></b></div>

					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
                        </div>
                      </div>
                    </div>

                    <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5> <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><b> การก่อสร้าง  <span class="badge bg-fuchsia">0</span>  </b></a></h5>
                      </div>
                      <div id="collapse6" class="panel-collapse collapse">
                         <div class="box-body">
                            <div class="timeline-item"> <b><h4 class="text-red">รอดำเนินการ......</h4></b></div>

					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
                        </div>
                      </div>
                    </div>

                    <div class="box box-solid box-default">
                      <div class="box-header with-border">
                        <h5><a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><b>คืนค้ำประกันสัญญา  <span class="badge bg-fuchsia">0</span>  </b></a></h5>
                      </div>
                      <div id="collapse7" class="panel-collapse collapse">
                         <div class="box-body">
                            <div class="timeline-item"> <b><h4 class="text-red">รอดำเนินการ......</h4></b></div>

					 <div class="row no-print">
                        <button class="btn btn-defult pull-right" style="margin-right: 20px;"><a href="frm_add_techniqce_total.aspx">เพิ่มกิจกรรม</a></button>
                    </div><br />
                        </div>
                      </div>
                    </div>

                  </div>
                </div><!-- /.box-body -->
              </div>
            </div>
       </div></div>
       <div class="tab-pane" id="stakeholders">
     
        <br /><div class="box box-solid box-primary">
       <div class="box-body">
             <div class="box-body">
                 <div class="row">
   
                     <div class="col-md-6">
                         <form class="form-horizontal">
                         <div class="box-body"><br />
                   
                    <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label"><small>ตำแหน่ง/สายงาน/อาชีพ :</small></label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">หน่วยงานราชการ</option>
                      <option>หน่วยงานรัฐวิสาหกิจ</option>
                      <option>หน่วยงานท้องถิ่น</option>
                      <option>หน่วยงานเอกชน/ภาครัฐเอกชน</option>
                      <option>กลุ่ม/ชุมชน</option>
                      <option>ผู้อาศัยสองข้างทาง/ใกล้เคียง</option>
                      <option>ผู้ใช้เส้นทางทั่วไป</option>
                      <option>ผู้ประกอบการ/ร้านค้า</option>
                      <option>องค์กรอิสระ/มูลนิธิ</option>
                      <option>สื่อมวลชน</option>
                      <option>ผู้นำชุมชน</option>
                    </select></div>
                  </div><!-- /.form-group -->

                     <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label"><small>อยู่ในพื้นที่ :</small></label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ก่อสร้าง</option>
                      <option>ใกล้เคียง</option>
                      <option>อื่นๆ</option>
                    </select></div>
                  </div><!-- /.form-group -->
             
                     
                  </div><!-- /.box-body -->
                  
                </form>
                </div>

                     <div class="col-md-6">
                     
                   <form class="form-horizontal">
                  <div class="box-body"><br />
                  
                        <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label"><small>ผลกระทบ :</small></label>
                        <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">โดยตรง</option>
                      <option>โดยอ้อม</option>
                      <option>ไม่กระทบแต่สนใจ</option>
                    </select></div>
                  </div><!-- /.form-group --> 
                     <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">ประเภท  :</label>
                        <div class="col-sm-9">
                      <select class="form-control select3" style="width: 100%;">
                      <option selected="selected">Key</option>
                      <option>Primary</option>
                      <option>Secondary</option>
                    </select></div>
                  </div><!-- /.form-group -->
                  </div><!-- /.box-body -->
                 </form></div>
               
                      <div class="col-md-12">
                     
                  <div class="box-body">
                   
                      <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label text-right">ค้นหา :</label>
                        <div class="col-sm-9">
                    <div class="input-group" style="width: 100%;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="ค้นหาตามชื่อ-นามสกุล,เบอร์โทรศัพท์,อิทธิพล,บทบาท...">
                     
                    </div>
                  </div></div><!-- /.form-group -->
                    
                  </div><!-- /.box-body -->
                 </div>
               </div>
            </div> <div class="box-body">
         <div class="row no-print"><center>
                   
                        <button class="btn btn-success pull-right" style="margin-right: 40px;">เพิ่มข้อมูล</button>
                        <button class="btn btn-default pull-right" style="margin-right: 20px;"> ค้นหา</button></center>
                </div><br />
             <div class="box-body">
                 <div class="box-body no-padding">
                    <table class="table table-condensed table-bordered">
                    <tr class="bg-blue">
                      <th class="actions" style=" text-align :center ;"><small>ชื่อ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>นามสกุล</small></th>
                      <th class="actions" style=" text-align :center ;"><small>เบอร์โทร</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ตำแหน่ง/สายงาน/อาชีพ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>อยู่ในพื้นที่</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ผลกระทบ</small></th> 
                      <th class="actions" style=" text-align :center ;"><small>อิทธิพล/บทบาท</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ประเภท <br />key/primary/secondary</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ความคิดเห็น <br />ไม่มี/บวก/ลบ</small></th>
                      <TH COLSPAN="2" class="actions" style=" text-align :center ;"><small>ดำเนินการ</small></th> 
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>นายธีรเจต</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2"><small>ภู่ระหงษ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>097656766</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>ประธานชุมชน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_stakeholders2.aspx"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยอ้อม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Secondary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายชัยวัฒน์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ชื่นโกสุม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>089-8535532</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>รองประธานหมู่บ้าน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยตรง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Primary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายดุษฎี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>สุวัฒนยากร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>084-8031296</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>เลขา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยตรง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มาก</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Primary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นางสาวอัจฉรา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>วงศ์เอก</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>093-1312534</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>เลขา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยอ้อม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Secondary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายนิวัฒน์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>พาตะนันท์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>053-461628</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ประชาชน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยอ้อม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Secondary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายชัยพัฒน์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ใชยสวัสดิ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>053-461161</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ประชาชน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ใกล้เคียง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยตรง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มาก</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Primary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายธีรภัทร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>คัชมาตย์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>053-461089</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ประชาชน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยตรง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Primary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายกษิเดช</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ทองสะสม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>055-468961</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ประชาชน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ใกล้เคียง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยตรง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มาก</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Primary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>นายอาณัติ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>รักธรรม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>083-465089</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ประชาชน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยตรง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>Primary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                    
                    
                  </table>
                </div><!-- /.box-body -->
               
                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนหลัง</a></li>
                  </ul>
                </div>
              </div><!-- /.box -->
            </div> </div>
            </div><!-- /.box-body -->
        
             
          

              </div>
        <div class="tab-pane" id="question">
     <div class="box-body">  
                   
                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
        
         <div class="row no-print"><center>
                    <div class="box-header with-border">
             <a data-toggle="modal" href="#normalModal" class="btn btn-default pull-right" style="margin-right: 20px;">เลือกแผ่นพับที่ต้องการใช้</a></div>
                </div><br />
                <div id="normalModal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">แผ่นพับ</h4>
      </div>
      <div class="modal-body">
         <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ประเภท</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>แผ่นพับ</small></th>
                    </tr>
<!--------------------------------------Chioce 1------------------------------------------------------------>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชาชนมีส่วนร่วม</small></td>
                    </tr>
                   
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 2</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานดิน</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 3.1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานท่อกลม กสล.</small></td>
                    </tr>
<!--------------------------------------Chioce 2------------------------------------------------------------>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 3.2</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานท่อลอดเหลี่ยม</small></td>
                    </tr>
                     
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 4</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานลูกรัง	</small></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 5.1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานหินคลุก</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 5.2</small></td>
                      <td class="actions" style=" text-align :center ;"><small>การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</small></td>
                    </tr>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 6.1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานผิวทางแอสฬลต์คอนกรีต</small></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 6.2</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานผิวทางคอนกรีต</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 6.3</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานผิวทางลาดยางเคพซีล</small></td>
                    </tr>
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 7</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</small></td>
                    </tr>
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ส 1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชาชนมีส่วนร่วม</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ส 2</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานสำรวจเพื่อการก่อสร้างและการวางผัง</small></td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ส 3</small></td>
                      <td class="actions" style=" text-align :center ;"><small>งานฐานรากสะพาน</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ส 4</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชาชนมีส่วนร่วม</small></td>
                    </tr>
                  </table>
      </div>
      <div class="modal-footer"><ul class="pagination pagination-sm no-margin pull-left">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
        <button type="button" class="btn btn-info">เลือก</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                 <div class="box-body no-padding">
                   <table class="table table-condensed table-bordered">
                    <tr class="bg-blue">
                      <TH COLSPAN="2" class="actions" style=" text-align :center ;"><small>แผ่นพับ</small></th>
                      <TH COLSPAN="4" class="actions" style=" text-align :center ;"><small>จำนวน</small></th>
                      <TH COLSPAN="5" class="actions" style=" text-align :center ;"><small>สถานะ</small></th>  
                    </tr>
                    
                    <tr class="bg-blue">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>สีแผ่นพับ</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ประเภท</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เพิ่ม</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>จำนวนที่แจก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>จำนวนข้อที่ต้องแก้ไข</small></th> 
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>จำนวนที่ยังไม่แก้ไข</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ข้อที่</small></th>  
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>สถานะ</small></th>
                      <TH COLSPAN="4" class="actions" style=" text-align :center ;"><small>ดำเนินการ</small></th> 
                    </tr>
<!--------------------------------------Chioce 1------------------------------------------------------------>
                       <tr>
                       <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><small>ซ 1</small></a></td>
                      <th class="actions" style=" text-align :center ;"><a href="frm_question_t.aspx"><span class="label label-success"><i class="fa fa-plus" data-widget="edit" data-toggle="tooltip" title="เพิ่ม"></i></span></a></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><small>12</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><small> 4 </small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><small> 2 </small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><small> ข้อ 2 </small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx"><small>แก้ไขแล้ว</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                     
                     <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"><small></small></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><small> ข้อ 4 </small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx"><small>แก้ไขแล้ว</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                     <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"><small></small></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><small> ข้อ 6 </small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx"><small>ยังไม่แก้ไข</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>

                       <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"><small></small></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><small> ข้อ 7 </small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx"><small>ยังไม่แก้ไข</small></a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
              
              </div>
            </div>
        
           <div class="modal-footer">
        <button type="button" class="btn btn-info">บันทึกข้อมูล</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button></div>
           </div>
      </div>
         </div>
        
          </div><!-- /.row -->
              </div>
        </div>
          </div>
        </section><!-- /.content -->
      </div>

     <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>

</asp:Content>
 