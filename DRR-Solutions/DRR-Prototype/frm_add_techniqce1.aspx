﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_add_techniqce1.aspx.cs" Inherits="DRR_Citizen.frm_add_techniqce1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
   <div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">

     <div class="col-md-12">
             <div class="box box-solid box-primary">
                <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลเทคนิคการมีส่วนร่วม (CPM_UT0209)</b></h4></div>
                 
                <div class="box-body">
                    <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มเทคนิคการมีส่วนร่วม</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                                <th class="actions" style=" text-align :center ;"><b>รหัส</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>ชื่อเทคนิคการมีส่วนร่วม</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>ระดับการมีส่วนร่วม</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>ดำเนินการ</b></th>
                                </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0101</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;แผ่นป้ายประชาสัมพันธ์</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><a href="frm_add_techniqce2.aspx"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></a></th>
    		                    </tr>

                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0102</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;Website</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0103</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;E-mail</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0104</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;จดหมายข่าว</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0105</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;แถลงข่าว</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0106</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;วิทยุ</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0107</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;โทรทัศน์</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0108</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;หนังสือพิมพ์</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0109</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;Social Listening</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0110</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ประกาศตามระเบียบราชการ</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0111</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สัมภาษณ์รายบุคคล</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0112</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สำรวจความคิดเห็น</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0201</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สนทนากลุ่มย่อย</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0202</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สายด่วน/สายตรง</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                   </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0203</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;พบปะอย่างไม่เป็นทางการ</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"><i class="fa fa-star text-yellow"></i></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0204</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;จัดประชุม</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"><i class="fa fa-star text-yellow"></i></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0301</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สัมมนาเชิงปฏิบัติการ</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0302</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;เวทีสาธารณะ</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0303</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;คณะทำงานเพื่อแลกเปลี่ยนข้อมูล</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                                </tr>
                                
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0304</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ประชุมระดมสมอง</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0305</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ประชุมกลุ่มย่อยในการวางแผน</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0306</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ประชุมระดมความคิดเห็น</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0401</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;คณะที่ปรึกษาร่วมภาคประชาชน</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0402</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;คณะกรรมการร่วมภาคประชาชน</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                                </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;">0501</td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;การทำประชามติ</td>
    		                    <td class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></b></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                                </tr>
                            </table> <br />
                        </div> <br /></div>
                      </div> </div>

    </div>
    </section>
   </div>
</asp:Content>
