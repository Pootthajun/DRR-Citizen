﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_p.aspx.cs" Inherits="DRR_Citizen.frm_question_p" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>

<div class="content-wrapper">
   
<!-- Main content ----------------------------------------------------------->
        <section class="content">

            <div class="row">
          <div class="col-md-12">
            
                <div class="box box-solid box-info ">
                    
                      <div class="box-body">
             <div class="box-body">
                 <div class="row">
                         <form class="form-horizontal">
                                        <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">ค้นหาแผ่นพับ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option ">ประเภท ท</option>
                      <option ">ประเภท บ</option>
                      <option>ประเภท ส</option>
                      <option selected="selected">ประเภท ป</option>
                      <option>ประเภท ซ</option>
                      <option>อื่น</option>
                    </select></div>
                    </div>
                
                  
                  </div><!-- /.box-body -->
                  
                </form>

                </div></div>
                  </div></div>      
              <div class="box box-solid box-info ">  
         <div class="box-body">
               <br /><div class="row">
              <div class="col-md-1"></div>

                   <div class="col-md-10">

                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p1.aspx">ป 1 </a></td>
                      <td><a href="frm_techniqce_9p1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p2.aspx">ป 2 </a></td>
                      <td><a href="frm_techniqce_9p2.aspx">งานดิน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                     <td><a href="frm_techniqce_9t1.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p31.aspx">ป 3 </a></td>
                      <td><a href="frm_techniqce_9p3.aspx">งานท่อกลม กสล.</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p32.aspx">ป 4 </a></td>
                      <td><a href="frm_techniqce_9p4.aspx">งานลูกรัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                  <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p51.aspx">ป 5.1</a></td>
                      <td><a href="frm_techniqce_9p51.aspx">งานหินคลุก</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p52.aspx">ป 5.2</a></td>
                      <td><a href="frm_techniqce_9p52.aspx">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p61.aspx">ป 5.3 </a></td>
                      <td><a href="frm_techniqce_9p61.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p61.aspx">ป 6.1 </a></td>
                      <td><a href="frm_techniqce_9p61.aspx">งานผิวทางแอสฬลต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p62.aspx">ป 6.2</a></td>
                      <td><a href="frm_techniqce_9p62.aspx">งานผิวทางคอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p63.aspx">ป 6.3 </a></td>
                      <td><a href="frm_techniqce_9p63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p7.aspx">ป 7</a></td>
                      <td><a href="frm_techniqce_9p7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p8.aspx">ป 8 </a></td>
                      <td><a href="frm_techniqce_9p8.aspx">งงานสำรวจเพื่อการก่อสร้างและการวางผัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                      <tr>
                       <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p9.aspx">ป 9</a></td>
                      <td><a href="frm_techniqce_9p9.aspx">งานรากฐานสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p10.aspx">ป 10 </a></td>
                      <td><a href="frm_techniqce_9p10.aspx">งานตอม่อสะพาน</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p11.aspx">ป 11</a></td>
                      <td><a href="frm_techniqce_9p11.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p12.aspx">ป 12 </a></td>
                      <td><a href="frm_techniqce_9p12.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p13.aspx">ป 13</a></td>
                      <td><a href="frm_techniqce_9p13.aspx">งานทางเท้าและราวสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p14.aspx">ป 14 </a></td>
                      <td><a href="frm_techniqce_9p14.aspx">งานพื้นถนนคอนกรีตเสริมเหล็กเพื่อพาดคอสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                   <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p15.aspx">ป 15</a></td>
                      <td><a href="frm_techniqce_9p15.aspx">งานส่วนประกอบอื่นๆ</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->

              <div class="col-md-1"></div>

            </div><!-- /.box-body --><br />
          
        </div></div> 
            
            </div>
        </div>

    </section>
   </div>

</asp:Content>

