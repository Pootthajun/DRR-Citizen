﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="config.aspx.cs" Inherits="DRR_Citizen.config" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
     <div class="content-wrapper" style="min-height: 1096px;">
      
     <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>

<!-- Main content ----------------------------------------------------------->
     <section class="content">
        <div class="row">
          <div class="col-md-12">
             
           <div class="box box-solid box-info">
     
             <div class="box-body">
              <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลแผ่นพับ</b></h4></div>
               <div class="box-body">
                 <div class="row">
                
                    <div class="box-body">
                       
                  <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label text-right">ค้นหาโครงการ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ก่อสร้างทาง</option>
                      <option>ก่อสร้างสะพาน</option>
                      <option>บำรุงรักษา</option>
                      <option>อำนวยความปลอดภัย</option>
                      <option>ซ่อมบำรุง</option>
                      <option>อื่น</option>
                    </select>
                      </div>
                </div></div>


                <div class="box-body">
          <div class="row">
           <div class="col-md-1"></div>  
            <div class="col-md-10">
             <div class="box-body">
              
              
              <div id="tabs  bg-lime-active"">
                 <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><span><i class="fa fa-circle text text-green"></i> Active</span></a></li>
                  <li><a href="#tab_2" data-toggle="tab"><span><i class="fa fa-circle text text-red"></i> Inactive</span></a></li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                     <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มแผ่นพับ</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">แผ่นพับ</th>
                      <th class="actions" style=" text-align :center ;">เวอร์ชั่น</th>
                      <th class="actions" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                       <th class="actions" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_add_techniqce.aspx">ท 1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                      <td><a href="frm_add_techniqce.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td><a href="frm_add_techniqce.aspx">27/02/2559</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t2.aspx">ท 2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                      <td><a href="frm_question_t2.aspx">งานดิน</a></td> 
                     <td><a href="frm_techniqce_t2.aspx">30/09/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t31.aspx">ท 3.1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t31.aspx">งานท่อกลม กสล.</a></td> 
                      <td><a href="frm_question_t31.aspx">06/05/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t32.aspx">ท 3.2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t32.aspx">งานท่อลอดเหลี่ยม</a></td> 
                      <td><a href="frm_techniqce_t32.aspx">29/09/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t4.aspx">ท 4</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t4.aspx">งานลูกรัง</a></td> 
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t51.aspx">ท 5.1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t51.aspx">งานหินคลุก</a></td> 
                      <td><a href="frm_question_9t1.aspx">07/07/2558</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t52.aspx">ท 5.2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t52.aspx">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</a></td> 
                       <td><a href="frm_question_9t1.aspx">09/10/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t53.aspx">ท 5.3 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t53.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                       <td><a href="frm_question_9t1.aspx">03/11/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_t61.aspx">ท 6.1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t61.aspx">งานผิวทางแอสฬลต์คอนกรีต</a></td> 
                       <td><a href="frm_question_9t1.aspx">15/01/2559</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t62.aspx">ท 6.2</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t62.aspx">งานผิวทางคอนกรีต</a></td> 
                       <td><a href="frm_question_9t1.aspx">31/03/2559</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t63.aspx">ท 6.3 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                       <td><a href="frm_question_t63.aspx">25/07/2558</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t7.aspx">ท 7</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td> 
                       <td><a href="frm_question_t7.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">Active</span></td>
                    </tr>
                  </table>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_2">
                    <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ประเภท</th>
                      <th class="actions" style=" text-align :center ;">เวอร์ชั่น</th>
                      <th class="actions" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                       <th class="actions" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="#">ท 1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="#">ประชนมีส่วนร่วม</a></td> 
                      <td><a href="#">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">Inactive</span></td>
                    </tr>
                   
                       <tr>
                     <td class="actions" style=" text-align :center ;"><a href="#">ท 2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="#">งานดิน</a></td> 
                     <td><a href="#">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">Inactive</span></td>
                    </tr>
                  </table>
                  </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
            



            </div><!-- /.box-body -->
           </div>
          <div class="col-md-1"></div>
         </div>
        </div>
                </div><!-- /.box-body -->
             
         


       </div></div>     
         </div> 
       </div>
    </div>
    </section>
   </div>
</asp:Content>
