﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_department2.aspx.cs" Inherits="DRR_Citizen.frm_config_department2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
             <div class="box box-solid box-primary">

       <div class="box-body">
             <div class="box-body">
                 <div class="row">
                    <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลหน่วยงาน (CPM_UT0202)</b></h4></div>
                    <form class="form-horizontal">
                        <div class="col-md-6">
           
                        <div class="box-body">

                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">สังกัดหน่วย :</label>
                      <div class="col-sm-8">
                        <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="สำนักงานทางหลวงชนบทที่ 1 (ปทุมธานี) " ReadOnly ="true"></asp:TextBox>
                      </div><br />
                    </div>
                     
                      </div>

                       </div>

                        <div class="col-md-6">
           
                        <div class="box-body">

                   
                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">หน่วยดำเนินงาน :</label>
                      <div class="col-sm-8"> 
                        <asp:TextBox ID="input1" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทนนทบุรี " ReadOnly ="true"></asp:TextBox>
                        <asp:TextBox ID="input2" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทปทุมธานี " ReadOnly ="true"></asp:TextBox>
                        <asp:TextBox ID="input3" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทพระนครศรีอยุธยา " ReadOnly ="true"></asp:TextBox>
                        <asp:TextBox ID="input4" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทสมุทรปราการ " ReadOnly ="true"></asp:TextBox>
                        <asp:TextBox ID="input5" runat="server" class="form-control text-left" placeholder="แขวงทางหลวงชนบทอ่างทอง " ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                      
                        </div>  
                   
                      </div></form></div>
                     <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  </div><!-- /.box -->
                    
                
                  
                </div>
            </div>
            </div><!-- /.box-body -->
            
               <!-- /.box -->
            </div>
          
        </section><!-- /.content -->
      </div>
</asp:Content>
