﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace DRR_Citizen
{
    public partial class frm_config_techniqce1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Type_Peoject();

                DT_Type1();

            }
        }


        public void  Type_Peoject()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ValueF");
            dt.Columns.Add("Display");
            DataRow row;


            row = dt.NewRow();
            row["ValueF"] = "ท";
            row["Display"] = "ก่อสร้างทาง";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "ส";
            row["Display"] = "ก่อสร้างสะพาน";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "บ";
            row["Display"] = "อำนวยความปลอดภัย";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "4";
            row["Display"] = "ซ่อมบำรุง";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "5";
            row["Display"] = "อื่นๆ";
            dt.Rows.Add(row);
            
            ddl_type.DataTextField = "Display";
            ddl_type.DataValueField = "ValueF";
            ddl_type.DataSource = dt;
            ddl_type.DataBind();

        }


        public void DT_Type1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("type");
            dt.Columns.Add("version");
            dt.Columns.Add("detail");
            dt.Columns.Add("date");
            dt.Columns.Add("status");
            DataRow row;


            row = dt.NewRow();
            row["type"] = "ท 1";
            row["version"] = "V1.0";
            row["detail"] = "ประชนมีส่วนร่วม";
            row["date"] = "27/02/2016";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 2";
            row["version"] = "V1.0";
            row["detail"] = "งานดิน";
            row["date"] = "2/05/2015";
            row["status"] = "inactive";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 2";
            row["version"] = "V1.0";
            row["detail"] = "งานดิน";
            row["date"] = "30/09/2015";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 3.1";
            row["version"] = "V1.0";
            row["detail"] = "งานท่อกลม กสล.";
            row["date"] = "06/05/2015";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 3.2";
            row["version"] = "V1.0";
            row["detail"] = "งานท่อลอดเหลี่ยม";
            row["date"] = "29/09/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ท 4";
            row["version"] = "V1.0";
            row["detail"] = "งานลูกรัง";
            row["date"] = "21/05/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ท 5.1";
            row["version"] = "V1.0";
            row["detail"] = "งานหินคลุก";
            row["date"] = "07/07/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ท 5.2";
            row["version"] = "V1.0";
            row["detail"] = "การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่";
            row["date"] = "09/10/2014";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 5.3";
            row["version"] = "V1.0";
            row["detail"] = "การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่";
            row["date"] = "03/11/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ท 6.1";
            row["version"] = "V1.0";
            row["detail"] = "งานผิวทางแอสฬลต์คอนกรีต";
            row["date"] = "15/01/2016";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 6.2";
            row["version"] = "V1.0";
            row["detail"] = "งานผิวทางคอนกรีต";
            row["date"] = "31/03/2016";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 6.3";
            row["version"] = "V1.0";
            row["detail"] = "งานผิวทางลาดยางเคพซีล";
            row["date"] = "25/07/2015";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ท 7";
            row["version"] = "V1.0";
            row["detail"] = "งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย";
            row["date"] = "18/08/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            //--------------------------ประเภท ส.

                 row = dt.NewRow();
            row["type"] = "ส 1";
            row["version"] = "V1.0";
            row["detail"] = "ประชนมีส่วนร่วม";
            row["date"] = "27/02/2016";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ส 2";
            row["version"] = "V1.0";
            row["detail"] = "งานสำรวจเพื่อการก่อสร้างและการวางผัง";
            row["date"] = "2/05/2015";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ส 3";
            row["version"] = "V1.0";
            row["detail"] = "งานฐานรากสะพาน";
            row["date"] = "06/05/2015";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ส 4";
            row["version"] = "V1.0";
            row["detail"] = "งานตอม่อสะพาน";
            row["date"] = "29/09/2015";
            row["status"] = "active";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["type"] = "ส 5";
            row["version"] = "V1.0";
            row["detail"] = "งานพื้นสะพานแบบหล่อในพื้นที่";
            row["date"] = "07/07/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ส 6";
            row["version"] = "V1.0";
            row["detail"] = "งานสะพานแบบคอนกรีตอัดแรง";
            row["date"] = "09/10/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ส 7";
            row["version"] = "V1.0";
            row["detail"] = "งานทางเท้าและราวสะพาน";
            row["date"] = "07/07/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ส 8";
            row["version"] = "V1.0";
            row["detail"] = "งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน";
            row["date"] = "15/01/2015";
            row["status"] = "active";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["type"] = "ส 9";
            row["version"] = "V1.0";
            row["detail"] = "งานส่วนประกอบอื่นๆ";
            row["date"] = "31/03/2016";
            row["status"] = "active";
            dt.Rows.Add(row);


            //--------------------------ประเภท บ.

            row = dt.NewRow();
            row["type"] = "บ 1";
            row["version"] = "V1.0";
            row["detail"] = "ประชนมีส่วนร่วม";
            row["date"] = "31/03/2016";
            row["status"] = "active";
            dt.Rows.Add(row);

            string type = ddl_type.SelectedValue.ToString();
            DataView dv = new DataView(dt);
            dv.RowFilter = "(type like '%" + type + "%')";
            GridView1.DataSource = dv;
            GridView1.DataBind();

        }

        protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            DT_Type1();
        }
    }
}