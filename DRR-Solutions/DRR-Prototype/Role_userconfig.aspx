﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="Role_Userconfig.aspx.cs" Inherits="DRR_Citizen.Role_Userconfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
<script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();

        //Datemask dd/mm/yyyy
        $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
        //Datemask2 mm/dd/yyyy
        $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
        //Money Euro
        $("[data-mask]").inputmask();

        //Date range picker
        $('#reservation').daterangepicker();
        //Date range picker with time picker
        $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
        //Date range as a button
        $('#daterange-btn').daterangepicker(
            {
              ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
              },
              startDate: moment().subtract(29, 'days'),
              endDate: moment()
            },
        function (start, end) {
          $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }
        );

        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
          checkboxClass: 'icheckbox_minimal-blue',
          radioClass: 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
          checkboxClass: 'icheckbox_minimal-red',
          radioClass: 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
          checkboxClass: 'icheckbox_flat-green',
          radioClass: 'iradio_flat-green'
        });

        //Colorpicker
        $(".my-colorpicker1").colorpicker();
        //color picker with addon
        $(".my-colorpicker2").colorpicker();

        //Timepicker
        $(".timepicker").timepicker({
          showInputs: false
        });
      });
    </script>

    <title>โครงการทั้งหมด | กรมทางหลวงชนบท</title>

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>หน้าหลัก</a></li>
             <li class="active">ข้อมูลผู้ใช้งาน</li>
          </ol>
        </section><br />
		

<!-- Main content ----------------------------------------------------------->
        <section class="content">

         <div class="row">
          <div class="col-md-12">  

              <div class="box box-solid box-default ">
       
         <div class="box-body">
              <div class="row">

                <div class="col-md-6"><br />
                     <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">ระบบ</label>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="ค้นหาตามระบบงาน,รหัสระบบงาน">
                      </div>
                    </div>
                </div><!-- /.col -->

                <div class="col-md-6"><br />
                   <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">บทบาท</label>
                      <div class="col-sm-9">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="">
                      </div>
                    </div>
                </div><!-- /.row -->

            </div><!-- /.box-body --><br />
           <center><button class="btn btn-success">ค้นหาข้อมูล</button></center><br />
        </div></div>


              <div class="box box-solid box-info">
                <div class="box-header">
                  <h6 class="box-title">ตารางข้อมูล อสทช.</h6>
                  <div class="box-tools">
                    <div class="input-group" style="width: 250px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
                </div><!-- /.box-header -->
               <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped table-hover">
                    <tr>
                       <th class="actions" style=" text-align :center ;">แก้ไข</th>
                       <th class="actions" style=" text-align :center ;">ระบบ</th>
                       <th class="actions" style=" text-align :center ;">บทบาท</th>
                       <th class="actions" style=" text-align :center ;">ผู้ใช้งาน</th>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><span class="label label-info"><i class="fa fa-pencil"</i></span></td>
                      <td>ระบบการมีส่วนร่วมภาคประชาชน</td>
                      <td>อสทช.12</td>
                      <td class="actions" style=" text-align :center ;">1</td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><span class="label label-info"><i class="fa fa-pencil"</i></span></td>
                      <td>ระบบการมีส่วนร่วมภาคประชาชน</td>
                      <td>อสทช.9</td>
                       <td class="actions" style=" text-align :center ;">3</td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><span class="label label-info"><i class="fa fa-pencil"</i></span></td>
                      <td>ระบบการมีส่วนร่วมภาคประชาชน</td>
                      <td>อสทช.6</td>
                       <td class="actions" style=" text-align :center ;">4</td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><span class="label label-info"><i class="fa fa-pencil"</i></span></td>
                      <td>ระบบการมีส่วนร่วมภาคประชาชน</td>
                      <td>อสทช.4</td>
                       <td class="actions" style=" text-align :center ;">0</td>
                    </tr>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><span class="label label-info"><i class="fa fa-pencil"</i></span></td>
                      <td>ระบบการมีส่วนร่วมภาคประชาชน</td>
                      <td>อสทช.3</td>
                       <td class="actions" style=" text-align :center ;">0</td>
                    </tr>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><span class="label label-info"><i class="fa fa-pencil"</i></span></td>
                      <td>ระบบการมีส่วนร่วมภาคประชาชน</td>
                      <td>อสทช.2</td>
                       <td class="actions" style=" text-align :center ;">1</td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
                    <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&raquo;</a></li>
                  </ul>
                </div>
        </div><!-- /.box-body -->

              </div>

             </div>

         </div>
            </section>
    </div>

    </asp:Content>