﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_timeline1.aspx.cs" Inherits="DRR_Citizen.frm_timeline1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

     <div class="content-wrapper" style="min-height: 1096px;">
       
<!-- Main content ------------------------------------------------------------------------->
        <section class="content">
         <div class="box-body">
          <div class="box-body">
           <div class="row"> 
            <div class="box box-solid box-primary">
     
              <div class="box-body">
               <div class="box-header with-border"><h4 class="pull-left "><b>จัดการข้อมูลโครงการ (CPM_UT0302)</b></h4></div>
                 <div class="box-body">
              <div class="row">

                 <div class="box-body">
                 <div class="box-body">
                  <div class="row">
                   <form class="form-horizontal"> 
                     <div class="box box-solid box-info">
                     <div class="box-body">
                     <div class="box-body">
                     <div class="row">
                    <div class="col-md-12">   
                  <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">ชื่อโครงการ :</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="input1" runat="server" class="form-control" placeholder="ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) " ReadOnly ="true"></asp:TextBox>
                         </div> 
                      </div>
                     </div>

                  <div class="col-md-12">
                                
                        <div class="row">
                          <form class="form-horizontal"> 

                       <br /> <div class="col-md-12">
                    
                        <div class="box-body">
                    
                         <div class="box box-solid box-primary">
                            <div class="box-header">
                                 <h5 class="box-title" style=" text-align :center ;">สรุปกิจกรรมการมีส่วนร่วม</h5>
                             </div><!-- /.box-header -->
                         
                           <div class="box-body no-padding"style="overflow-x:auto;">
                           <table class="table table-bordered table-condensed">
                               <tr class="bg-info">
                                <th class="actions" style=" text-align :center ;"><b>ระดับ</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b><i class="fa fa-star text-yellow"></i></b></th>
                                <th class="actions" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></th>
    		                    <td class="actions" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></td>
    		                    <td class="actions" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></td>
    		                    <td class="actions" style=" text-align :center ;"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><b>จำนวนครั้ง</b></td>
    		                    <td class="actions" style=" text-align :center ;"><b>3</b></td>
                                <td class="actions" style=" text-align :center ;"><b>0</b></td>
                                <td class="actions" style=" text-align :center ;"><b>1</b></td>
                                <td class="actions" style=" text-align :center ;"><b>0</b></td>
                                <td class="actions" style=" text-align :center ;"><b>0</b></td>
    		                    </tr>
                                
                            </table>
                         </div></div>

                            
                       </div> <div class="col-md-6"><br />
                         <div class="box box-solid box-primary">
                            <div class="box-header" style=" text-align :center ;"> <h5 class="box-title">สถานะแผ่นพับ</h5> </div><!-- /.box-header -->
                             <div class="box-body no-padding"style="overflow-x:auto;">
                              <table class="table table-bordered table-hover">
                                 <tr class="bg-info">
    		                    <th class="actions" style=" text-align :center ;"><b>จำนวนที่แจกทั้งหมด</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>จำนวนที่ต้องแก้ไข</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>จำนวนที่แก้ไขแล้ว</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>จำนวนที่ยังไม่แก้ไข</b></th>
                               </tr> 
                               <tr>
    		                    <td class="actions" style=" text-align :center ;"><b>37</b></td>
                                <td class="actions" style=" text-align :center ;"><b>7</b></td>
                                <td class="actions" style=" text-align :center ;"><b>4</b></td>
                                <td class="actions" style=" text-align :center ;"><b>3</b></td>
    		                    </tr>
                                
                            </table> 
                         </div>
                     </div></div>
                            
                            
                             <div class="col-md-6"><br />
                         <div class="box box-solid box-primary">
                            <div class="box-header" style=" text-align :center ;"> <h5 class="box-title">สถานะการส่งรายงาน</h5> </div><!-- /.box-header -->
                             <div class="box-body no-padding"style="overflow-x:auto;">
                              <table class="table table-bordered table-hover">
                                <tr class="bg-info">
                                <th class="actions" style=" text-align :center ;"><b>สถานะ ทชจ.</b></th>
                                <th class="actions" style=" text-align :center ;"><b>สถานะสำนัก</b></th>
                                <th class="actions" style=" text-align :center ;"><b>สถานะ สอร.</b></th>
                                <th class="actions" style=" text-align :center ;"><b>ผู้รายงาน</b></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                                </tr>
                            </table>
                      </div>

                        </div></div></div>

                       </form>
                      </div>
                      </div>
                   </div><!-- /.row -->
                 </div><!-- /.box-body -->
                  
                
               </div></div>

                      
                    </form>

                   </div></div></div>
                    </div></div>
                 <div class="box-body">
                        <div class="row">

                      <div id="tabs  bg-lime-active"">
                            <ul class="nav nav-tabs">
                            <li class="active"><a href="#detail" data-toggle="tab" aria-expanded="true"><span><i class="fa fa-file-text text-green"></i> รายละเอียดโครงการ</span></a></li>
                            <li><a href="#plan" data-toggle="tab" aria-expanded="false"><span><i class="fa fa-road text text-green"></i> บันทึกต้นทาง-ปลายทาง</span></a></li>
                            <li><a href="#question" data-toggle="tab" aria-expanded="false"><span><i class="fa fa-clone  text-green"></i> จัดการแผ่นพับ</span></a></li>
                            <li><a href="#act" data-toggle="tab" aria-expanded="false"><span><i class="fa fa-users text-green"></i> บันทึกการมีส่วนร่วม</span></a></li>
                            </ul>
    
<!--------- Tab ------------------------------------------------------>

                     
       <div class="tab-content">

        <div class="tab-pane active" id="detail">
            
       <div class="box-body">
             <div class="box-body">
                 <div class="row">
                    <form class="form-horizontal"> 
                    
                     <div class="box box-solid box-primary">
                       <div class="box-body">   
                       <div class="box-header with-border"><h5 class="pull-left text-blue"><b>ข้อมูลโครงการ </b></h5></div>
                      <div class="col-md-6">
                    
                        <div class="box-body">
                             <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label text-left">ปีงบประมาณ:</label>
                        <div class="col-sm-8">
                         <asp:TextBox ID="TextBox2" runat="server" class="form-control" placeholder="2557 " ReadOnly ="true"></asp:TextBox>
                         
                       </div><!-- /.form-group --> 
                       </div>
                            <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label text-left">ระยะทาง (กิโลเมตร):</label>
                        <div class="col-sm-8">
                           <asp:TextBox ID="TextBox5" runat="server" class="form-control" placeholder="- " ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group -->  

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">เลขที่สัญญา :</label>
                      <div class="col-sm-8">
                        <asp:TextBox ID="TextBox11" runat="server" class="form-control" placeholder="01/2557" ReadOnly ="true"></asp:TextBox>
                        
                      </div>
                    </div>
                    
                             <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label text-left">เริ่มต้นสัญญาวันที่ :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                        <asp:TextBox ID="TextBox12" runat="server" class="form-control" placeholder="23/01/2013" ReadOnly ="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                      </div>
                    </div>
                       <div class="form-group">
                        <label for="inputName" class="col-sm-4 control-label text-left">งบประมาณทั้งสื้น(ตามสัญญา) :</label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="TextBox1" runat="server" class="form-control" placeholder="12,600,000.00" ReadOnly ="true"></asp:TextBox>
                         </div> 
                      </div>

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">วันเบิกเงินงวดสุดท้าย :</label>
                      <div class="col-sm-8">
                                    <div class="input-group">
                                <asp:TextBox ID="TextBox9" runat="server" class="form-control" placeholder="2/28/2014" ReadOnly ="true"></asp:TextBox>
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div>
                    </div></div>
                    
                             <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label text-left">รับประกันงาน :</label>
                       <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;"disabled>
                      <option selected="selected" >1  ปี</option>
                      <option>2  ปี</option>
                    </select></div>
                    </div>
                    
                         <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">สังกัดหน่วย :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                        <asp:TextBox ID="TextBox10" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                        <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->     
                      </div>
                    </div>

                             <div class="form-group">
                               <label for="inputPassword3" class="col-sm-4 control-label text-left">URL :</label>
                                  <div class="col-sm-8">
                       
                        <asp:TextBox ID="TextBox23" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                   
                            </div></div>

                      </div>

                     </div>

                       <div class="col-md-6">
                    
                         <div class="box-body">

                             <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label text-left">รหัสสายทาง :</label>
                        <div class="col-sm-8">
                           <asp:TextBox ID="TextBox3" runat="server" class="form-control" placeholder="- " ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --> 

                             <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label text-left">ความก้าวหน้าโครงการ :</label>
                        <div class="col-sm-8">
                           <asp:TextBox ID="TextBox4" runat="server" class="form-control" placeholder="100% " ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --> 

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">วันที่ลงสัญญา :</label>
                      <div class="col-sm-8">
                       <div class="input-group">
                           <asp:TextBox ID="TextBox13" runat="server" class="form-control" placeholder="11/25/2013" ReadOnly ="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div>
                      </div>
                    </div>

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">สิ้นสุดสัญญาวันที่ :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                            <asp:TextBox ID="TextBox14" runat="server" class="form-control" placeholder="4/24/2014" ReadOnly ="true"></asp:TextBox>
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div>
                      </div>
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="col-sm-4 control-label text-left">งบประมาณทั้งสื้น(ตาม พรบ.) :</label>
                        <div class="col-sm-8">
                        <asp:TextBox ID="TextBox18" runat="server" class="form-control" placeholder="-" ReadOnly ="true"></asp:TextBox>
                         </div> 
                      </div>

                           <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">วันส่งงานงวดสุดท้าย :</label>
                      <div class="col-sm-8">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBox19" runat="server" class="form-control" placeholder="3/3/2014" ReadOnly ="true"></asp:TextBox>
                                 <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                                    </div>
                             </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-4 control-label">วันคืนค้ำประกันสัญญา :</label>
                                 <div class="col-sm-8">
                                    <div class="input-group">
                                        <asp:TextBox ID="TextBox20" runat="server" class="form-control" placeholder="4/24/2014" ReadOnly ="true"></asp:TextBox>
                      
                                    <div class="input-group-addon">
                                     <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                                    </div>
                                </div>

                        <div class="form-group">   
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">หน่วยดำเนินงาน :</label>
                      <div class="col-sm-8">
                          <div class="input-group">
                               <asp:TextBox ID="TextBox21" runat="server" class="form-control" placeholder="แขวงทางหลวงชนบทสตูล" ReadOnly ="true"></asp:TextBox>
                       <div class="input-group-addon">
                            <i class="fa fa-search"></i>
                        </div></div><!-- /.input group -->
                                    
                      </div>
                    </div>
                               <div class="form-group">
                               <label for="inputPassword3" class="col-sm-4 control-label text-left">QR code โครงการ :</label>
                                  <div class="col-sm-8">
                                    <div class="input-group">
                                  <a class="pull-right"><img src="dist/img/button/qrcode_url_xl_500.jpg" width="150" height="150" style="margin-right: 10px;" data-widget="ลบข้อมูล" /> </a>
                               </div>
                            </div></div>

                         </div></div></div> 
                       </div>
                     
                     <div class="box box-solid box-primary"> 
                         <div class="box-body"><div class="box-header with-border"><h5 class="pull-left text-blue"><b>ข้อมูลผู้ควบคุม </b></h5></div>
             

                      <div class="col-md-6">
                    
                        <div class="box-body">
                      
                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ชื่อผู้ควบคุมที่ 1 :</label>
                      <div class="col-sm-8">
                          
                               <asp:TextBox ID="TextBox29" runat="server" class="form-control" placeholder="นายเดชา พรหมมี" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                    
                         <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ตำแหน่ง :</label>
                      <div class="col-sm-8">
                               <asp:TextBox ID="TextBox37" runat="server" class="form-control" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                      </div></div>
                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">หน่วยดำเนินงาน :</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="แขวงทางหลวงชนบทสตูล" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                      </div></div>
               
                      <div class="col-md-6">
                    
                         <div class="box-body">
                         
                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">เบอร์โทรศัพท์ :</label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox32" runat="server" class="form-control" placeholder="0869434842" ReadOnly ="true"></asp:TextBox>
                        
                      </div>
                    </div>
                             
                     <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">สังกัดหน่วย :</label>
                      <div class="col-sm-8">
                              <asp:TextBox ID="TextBox31" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                     
                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">หมายเหตุ :</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox39" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>

                         </div></div></div></div>

                   <%--                    <div class="box box-solid box-primary"> 
                         <div class="box-body">  
                      <div class="col-md-6">
                    
                        <div class="box-body">
                      
                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ชื่อผู้ควบคุมที่ 2 :</label>
                      <div class="col-sm-8">
                          
                               <asp:TextBox ID="TextBox7" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                    
                         <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ตำแหน่ง :</label>
                      <div class="col-sm-8">
                               <asp:TextBox ID="TextBox8" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                      </div></div>
                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">หน่วยดำเนินงาน :</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox15" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                      </div></div>
               
                      <div class="col-md-6">
                    
                         <div class="box-body">
                         
                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">เบอร์โทรศัพท์ :</label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox16" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                        
                      </div>
                    </div>
                             
                     <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">สังกัดหน่วย :</label>
                      <div class="col-sm-8">
                              <asp:TextBox ID="TextBox17" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                     
                             <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">หมายเหตุ :</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox22" runat="server" class="form-control" placeholder="" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>

                         </div></div></div></div>--%>

                  </form>
                </div>
            </div>
            </div><!-- /.box-body -->
        
       </div>

        <div class="tab-pane" id="plan">
         <div class="box-body">
        <div class="box box-solid box-primary">

              <div class="box-body">
         <div class="box-body">
           
                  <div class="row">
                  <div class="box-body">
                 <div class="box-body no-padding">
                    <table class="table table-bordered tab-content table-hover">
    	            
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ช่วงที่</th>
                      <th class="actions" style=" text-align :center ;">เริ่ม(ละติจูด)</th>
                      <th class="actions" style=" text-align :center ;">เริ่ม(ลองติจูด)</th>
                      <th class="actions" style=" text-align :center ;">สิ้นสุด(ละติจูด)</th>
                      <th class="actions" style=" text-align :center ;">สิ้นสุด(ลองติจูด</th>
                      <th class="actions" style=" text-align :center ;">เริ่มกิโลเมตรที่</th>
                      <th class="actions" style=" text-align :center ;">สิ้นสุดกิโลเมตรที่</th>
                      <th class="actions" style=" text-align :center ;">แก้ไข</th>
                      <th class="actions" style=" text-align :center ;">ลบ</th>
                    </tr>
                     
                      <tr>
    		              <td class="actions" style=" text-align :center ;">1</td>
    		              <td class="actions" style=" text-align :center ;"> 0.00000</td>
                          <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">3.025</td>
                          <td class="actions" style=" text-align :center ;">8.025</td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>

    		           </tr>
                     <tr>
    		              <td class="actions" style=" text-align :center ;">2</td>
    		              <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">0.00000</td>
                          <td class="actions" style=" text-align :center ;">15.025</td>
                          <td class="actions" style=" text-align :center ;">24.025</td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                      <th class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span></th>

    		           </tr>
                   
                        
                            </table>
                    

                         </div><br /><br />
                 
                  <form class="form-horizontal">
                       <div class="form-group">
                        
                       <br /><label class="col-sm-2 control-label">แผนที่ :</label>
                      <div class="col-sm-8">
                      <img id="zoom_05" src="../../dist/img/line.jpg" style="width: 100%;" data-zoom-image="../../dist/img/drr-2/map.png"> 
                    </div></div>
                     </form>
             </div> </div><!-- /.row -->
            </div><!-- /.box-body -->
          </div></div>                   

        </div><!-- /.box-body -->
        </div>

        <div class="tab-pane" id="question">
     
               <div class="box-body">  
                   
                 <div class="box box-solid box-primary">
                       <div class="box-body">
                     
                <div class="row no-print">
           <div class="box-header with-border">
             <a data-toggle="modal" href="#normalModal" class="btn btn-vk pull-right" style="margin-right: 20px;">เลือกแผ่นพับที่ต้องการใช้</a></div>
             </div><br />
                <div id="normalModal" class="modal fade">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">แผ่นพับ</h4>
                        </div>
                        <div class="modal-body">

                 <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_t" data-toggle="tab">ประเภท ท</a></li>
                  <li><a href="#tab_b" data-toggle="tab">ประเภท บ</a></li>
                  <li><a href="#tab_p" data-toggle="tab">ประเภท ป</a></li>
                </ul>

                <div class="tab-content">

<!--------------------------------------ประเภท 1------------------------------------------------------------>
                  <div class="tab-pane active" id="tab_t">
                     <table class="table table-condensed table-bordered">
                   
                     <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">เลือก</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ประเภท</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">แผ่นพับ</th>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"checked></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t1.aspx">ท 1</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t1.aspx">ประชาชนมีส่วนร่วม</a></td>
                    </tr>
                   
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t2.aspx">ท 2</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t2.aspx">งานดิน</a></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t31.aspx">ท 3.1</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t31.aspx">งานท่อกลม กสล.</a></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t32.aspx">ท 3.2</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t32.aspx">งานท่อลอดเหลี่ยม</a></td>
                    </tr>
                     
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t4.aspx">ท 4</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t4.aspx">งานลูกรัง	</a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t51.aspx">ท 5.1</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t51.aspx">งานหินคลุก</a></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t52.aspx">ท 5.2</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t52.aspx">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</a></td>
                    </tr>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t61.aspx">ท 6.1</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t61.aspx">งานผิวทางแอสฬลต์คอนกรีต</a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t62.aspx">ท 6.2</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t62.aspx">งานผิวทางคอนกรีต</a></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t63.aspx">ท 6.3</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t63.aspx">งานผิวทางลาดยางเคพซีล</a></td>
                    </tr>
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t7.aspx">ท 7</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_t7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s1.aspx">ส 1</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s1.aspx">ประชาชนมีส่วนร่วม</a></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s2.aspx">ส 2</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s2.aspx">งานสำรวจเพื่อการก่อสร้างและการวางผัง</a></td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s3.aspx">ส 3</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s3.aspx">งานฐานรากสะพาน</a></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s4.aspx">ส 4</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s4.aspx">งานตอม่อสะพาน</a></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s5.aspx">ส 5</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s5.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s6.aspx">ส 6</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s6.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s7.aspx">ส 7</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s7.aspx">งานทางเท้าและราวสะพาน</a></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s8.aspx">ส 8</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s8.aspx">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาด</a></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s9.aspx">ส 9</a></td>
                      <td class="actions" style=" text-align :left ;"><a href="frm_question_s9.aspx">งานส่วนประกอบอื่นๆ</a></td>
                    </tr>
                  </table>
                  </div>
                    
<!--------------------------------------ประเภท 2------------------------------------------------------------>
                 <%-- <div class="tab-pane" id="tab_s">
                      <table class="table table-condensed table-bordered">
                   
                     <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">เลือก</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ประเภท</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">แผ่นพับ</th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 1</td>
                      <td class="actions" style=" text-align :center ;">ประชาชนมีส่วนร่วม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 2</td>
                      <td class="actions" style=" text-align :center ;">งานสำรวจเพื่อการก่อสร้างและการวางผัง</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 3</td>
                      <td class="actions" style=" text-align :center ;">งานฐานรากสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 4</td>
                      <td class="actions" style=" text-align :center ;">งานตอม่อสะพาน</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 5</td>
                      <td class="actions" style=" text-align :center ;">งานพื้นสะพานแบบหล่อในพื้นที่</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 6</td>
                      <td class="actions" style=" text-align :center ;">งานสะพานแบบคอนกรีตอัดแรง</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 7</td>
                      <td class="actions" style=" text-align :center ;">งานทางเท้าและราวสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 8</td>
                      <td class="actions" style=" text-align :center ;">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาด</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 9</td>
                      <td class="actions" style=" text-align :center ;">งานส่วนประกอบอื่นๆ</td>
                    </tr>

                  </table>

                  </div><!-- /.tab-pane -->--%>
                     
<!--------------------------------------ประเภท 3------------------------------------------------------------>
                  <div class="tab-pane" id="tab_b">
                          <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">เลือก</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ประเภท</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">แผ่นพับ</th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 1</td>
                      <td class="actions" style=" text-align :left ;">ประชนมีส่วนร่วม</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 2.1</td>
                      <td class="actions" style=" text-align :left ;">งานท่อกลม คสล.</td> 
                   
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                     <td class="actions" style=" text-align :center ;">บ 2.2</td>
                      <td class="actions" style=" text-align :left ;">งานท่อลอดเหลี่ยม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 3 </td>
                      <td class="actions" style=" text-align :left ;">งานงานดิน</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 4</td>
                      <td class="actions" style=" text-align :left ;">งานลูกรัง </td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 5.1</td>
                      <td class="actions" style=" text-align :left ;">งานหินคลุก</td> 
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 5.2 </td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 5.3</td>
                      <td class="actions" style=" text-align :left ;">งานซ่อมสร้างผิวทางแอสฟัลต์คอนกรีต</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 6.1</td>
                      <td class="actions" style=" text-align :left ;">งานไพร์มโคท</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 6.2 </td>
                      <td class="actions" style=" text-align :left ;">งานแทคโคท</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 6.3</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางลาดยางเคพซีล</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 6.4</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางแอสฟัลต์คอนกรีต</td> 
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 6.5</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางคอนกรต</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 6.6 </td>
                      <td class="actions" style=" text-align :left ;">งานเสริมผิวลาดยางแอลฟัลต์คอนกรีต</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 7</td>
                      <td class="actions" style=" text-align :left ;">งานเครื่องหมายจราจร และสิ่งอำนวยความปลอดภัย</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.1</td>
                      <td class="actions" style=" text-align :left ;">งานสำรวจสะพาน</td> 
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.2</td>
                      <td class="actions" style=" text-align :left ;">งานฐานรากสะพาน</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 8.3</td>
                      <td class="actions" style=" text-align :left ;">งานตอม่อสะพาน</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 8.4</td>
                      <td class="actions" style=" text-align :left ;">งานพื้นสะพานแบบหล่อในพื้นที่</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.5</td>
                      <td class="actions" style=" text-align :left ;">งานสะพานแบบคอนกรีตอัดแรง</td> 
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.6</td>
                      <td class="actions" style=" text-align :left ;">งานทางเท้าและราวสะพาน</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 8.7 </td>
                      <td class="actions" style=" text-align :left ;">งานพื้นคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 8.8</td>
                      <td class="actions" style=" text-align :left ;">งานส่วนประกอบอื่นๆ ของสะพาน </td> 
                    </tr>

                    
                  </table>

<!--------------------------------------ประเภท 4------------------------------------------------------------>

                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_p">
                     <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ประเภท</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>แผ่นพับ</small></th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 1</td>
                      <td class="actions" style=" text-align :left ;">ประชาชนมีส่วนร่วม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 2</td>
                      <td class="actions" style=" text-align :left ;">งานดิน</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 3</td>
                      <td class="actions" style=" text-align :left ;">งานท่อกลม คสล.</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 4</td>
                      <td class="actions" style=" text-align :left ;">งานลูกรัง</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 5.1</td>
                      <td class="actions" style=" text-align :left ;">งานหินคลุก</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 5.2</td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลูกรังกลับมาใช้ใหม่</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><input type="checkbox"></td>
                      <td class="actions" style=" text-align :center ;">ป 5.3</td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 6.1</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางแอสฟัลต์คอนกรีต</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 6.2</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางคอนกรีต</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 6.3</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางลาดยางเคพซีล</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ป 7</small></td>
                      <td class="actions" style=" text-align :left ;"><small>งานเครื่องหมายจราจร และสิ่งอำนวยความสะดวกปลอดภัย</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 8</td>
                      <td class="actions" style=" text-align :left ;">งานสำรวจเพื่อก่อสร้างและการวางผัง</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 9</td>
                      <td class="actions" style=" text-align :left ;">งานรากฐานสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 10</td>
                      <td class="actions" style=" text-align :left ;">งานตอม่อสะพาน</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ป 11</small></td>
                      <td class="actions" style=" text-align :left ;">งานพื้นสะพานแบบหล่อในพื้นที่</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 12</td>
                      <td class="actions" style=" text-align :left ;">งานสะพานแบบคอนกรีตอัดแรง</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 13</td>
                      <td class="actions" style=" text-align :left ;">งานทางเท้าและราวสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 14</td>
                      <td class="actions" style=" text-align :left ;">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ป 15</small></td>
                      <td class="actions" style=" text-align :left ;">งานส่วนประกอบอื่นๆ</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 16</td>
                      <td class="actions" style=" text-align :left ;">จุดตัดรถไฟและทางจักรยาน</td>
                    </tr>

                  </table>

<!--------------------------------------ประเภท 5------------------------------------------------------------>
                  </div><!-- /.tab-pane -->

                 
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->         
          </div>
                <div class="modal-footer"><ul class="pagination pagination-sm no-margin pull-left">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
        <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
        <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                 <div class="box-body no-padding">
                   <table class="table table-condensed table-bordered">
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="2" class="actions" style=" text-align :center ;">แผ่นพับ</th>
                      <TH COLSPAN="4" class="actions" style=" text-align :center ;">จำนวน</th>
                      <TH COLSPAN="3" class="actions" style=" text-align :center ;">สถานะ</th>  
                    </tr>
                    
                     <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">สีแผ่นพับ</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ประเภท</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">เพิ่ม</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">จำนวนที่แจก</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">จำนวนข้อที่<br />ต้องแก้ไข</th> 
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">จำนวนที่ยัง<br />ไม่แก้ไข</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ข้อที่</th>  
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">สถานะ</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">แก้ไข</th>
                    </tr>
<!--------------------------------------Chioce 1------------------------------------------------------------>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#myModalT1" data-toggle="modal">ท 1</a></td>
                      <th class="actions" style=" text-align :center ;"><a href="frm_question_t1.aspx"><span class="label label-info"><i class="fa fa-plus" data-widget="add" data-toggle="tooltip" title="เพิ่ม"></i></span></a></th>
                      <td class="actions" style=" text-align :center ;">25</td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"> 3</span> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success"> 1 </span> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"> ข้อ 4</a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t1_detail.aspx">แก้ไขแล้ว</a></td>
                      <th class="actions" style=" text-align :center ;"><a href="frm_question_t1_detail.aspx"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></a></th>
                     
                    </tr>
                   
                       <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t1_detail.aspx">ข้อ 6 </a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t1_detail.aspx">ยังไม่แก้ไข</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                    </tr>

                       <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t1_detail.aspx">ข้อ 7</a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_t1_detail.aspx">ยังไม่แก้ไข</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                     
                    </tr>

                        <div id="myModalT1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 id="myModalLabel1" class="modal-title"> ชื่อแผ่นพับ : ท1</h4>
                                                   
                                                </div>
                                                <div class="modal-body">
                                                    <h6> ชื่อแผ่น : ประชาชนมีส่วนร่วม</h6>
                                                    <h6>สีแผ่นพับ : สีแดง</h6>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
<!--------------------------------------Chioce 2------------------------------------------------------------>
                <br />    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#myModalT2" data-toggle="modal">ท 2</a></td>
                      <th class="actions" style=" text-align :center ;"><a href="frm_question_t.aspx"><span class="label label-info"><i class="fa fa-plus" data-widget="add" data-toggle="tooltip" title="เพิ่ม"></i></span></a></th>
                      <td class="actions" style=" text-align :center ;">12</td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"> 4</span> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success"> 2 </span> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"> ข้อ 2 </a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx">แก้ไขแล้ว</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                    </tr>
                     
                     <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"><small></small></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx">ข้อ 4 </a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx">แก้ไขแล้ว</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                   </tr>

                     <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"><small></small></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx">ข้อ 6</a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx">ยังไม่แก้ไข</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                    </tr>

                       <tr>
                      <TH COLSPAN="6" class="actions" style=" text-align :center ;"><small></small></th>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"> ข้อ 7</a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx">ยังไม่แก้ไข</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                  </tr>

                       <div id="myModalT2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 id="myModalLabel2" class="modal-title"> ชื่อแผ่นพับ : ท2</h4>
                                                   
                                                </div>
                                                <div class="modal-body">
                                                    <h6> ชื่อแผ่นพับ : งานดิน</h6>
                                                    <h6>สีแผ่นพับ : สีแดง</h6>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>

                       <!--------------------------------------Chioce 2------------------------------------------------------------>
                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#myModalT4" data-toggle="modal">ท 4</a></td>
                      <th class="actions" style=" text-align :center ;"><a href="frm_question_t.aspx"><span class="label label-info"><i class="fa fa-plus" data-widget="add" data-toggle="tooltip" title="เพิ่ม"></i></span></a></th>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"> 0 </span> </td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success"> 0 </span> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx">-</a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t5.aspx">-</a></td>
                      <th class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil" data-widget="edit" data-toggle="tooltip" title="แก้ไข"></i></span></th>
                    </tr>
                     
                     <div id="myModalT4" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 id="myModalLabel4" class="modal-title"> ชื่อแผ่นพับ : ท4</h4>
                                                   
                                                </div>
                                                <div class="modal-body">
                                                    <h6> ชื่อแผ่นพับ : งานลูกรัง</h6>
                                                    <h6>สีแผ่นพับ : สีแดง</h6>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>
                  </table>
                </div><!-- /.box-body -->
              
            </div></div>

           
           </div></div>
       
      
        <div class="tab-pane" id="act">
            <div class="row">
               
                <div class="box-body">
                 <div class="box-body">
                     <div class="box box-solid box-primary">
                  <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel box box-warning">

                      <div class="box-header with-border">
                        <h5 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> วางแผน <span class="badge bg-yellow">2</span></a> </h5>
                      </div>

                      <div id="collapse1" class="panel-collapse collapse in">
                        <div class="box-body">

                     <ul class="timeline timeline-inverse">
                       <li class="time-label">
                        <span class="bg-red">กิจกรรม</span>
                           <a data-toggle="modal" href="#normalModal2" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">ติดป้ายประชาสัมพันธ์ :ห้ามใช้ยานพาหนะที่มีน้ำหนัก น้ำหนักบรรทุก หรือน้ำหนักลงเพลาเกินกว่าที่ได้กำหนด </a> </h3>
                          <div class="timeline-body">
                          <span class="time pull-left"><i class="fa fa-star text-yellow"></i>
				          <i class=" fa fa-calendar"></i> 05/16/2016 </span><br />
                           <b>รายละเอียดกิจกรรม</b> : แขวงทางหลวงชนบทสตูล ได้ติดตั้งป้ายประชาสัมพันธ์ประกาศผู้อำนวยการทางหลวงชนบท เรื่อง ห้ามใช้ยานพาหนะที่มีน้ำหนัก น้ำหนักบรรทุก......
                          </div>
                          <div class="timeline-footer">
                            <a class="btn btn-bitbucket btn-xs" href="frm_techniqce1.aspx">ดูรายละเอียด/แก้ไข</a>
                            <a class="btn btn-danger btn-xs" href="#">ลบข้อมูล</a>
                          </div>
                        </div>
                      </li>
                      <!-- END timeline item -->
                     
                      <!-- timeline item -->
                      <li>
                        <i class="fa fa-users bg-yellow"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">จัดประชุม : ประชุมสัญจร ครั้งที่ 2/2016</a></h3>
                          <div class="timeline-body">
                              <span class="time pull-left"><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i><i class="fa fa-star text-yellow"></i>
				          <b><a class="text-red">วันที่วางแผน</a></b>  <i class=" fa fa-calendar"></i> 11/02/2016 </span><br />
						  <b class="pull-left text-blue"> จัดประชุม : ติดตามการดำเนินงานตามโครงการพัฒนาระบบควบคุมคุณภาพวัสดุและการก่อสร้างทาง </b><br>
                           <b>รายละเอียดกิจกรรม</b> :ได้จัดประชุมสัญจร ครั้งที่ 2/2559  เพื่อติดตามผลการเบิกจ่ายงบประมาณปี 2559  การเบิกจ่ายเงินกันไว้เบิกเหลื่อมปี 2558 และติดตามผลการปฏิบัติงานปี 2558....
                          </div>
                           <div class="timeline-footer">
                            <a class="btn btn-bitbucket btn-xs" href="frm_techniqce16.aspx">ดูรายละเอียด/แก้ไข</a>
                            <a class="btn btn-danger btn-xs" href="#">ลบข้อมูล</a>
                          </div>
                        </div>
                      </li>
                      <!-- END timeline item -->
                  
                    </ul>

                         </div>
                      </div>
                    </div>


                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"> สำรวจออกแแบบ <span class="badge bg-yellow">1</span></a> </h4></div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="box-body">
                           <ul class="timeline timeline-inverse">
                      <li class="time-label">
                        <span class="bg-red">กิจกรรม</span><a data-toggle="modal" href="#normalModal" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">แผ่นพับ</a> </h3>
                          <div class="timeline-body">
                          <span class="time pull-left"><i class="fa fa-star text-yellow"></i>
				          <i class=" fa fa-calendar"></i> 07/10/2015 - 09:57</span><br />
                           <b>รายละเอียดกิจกรรม</b> : 
                              <div class="row no-print">
                                <div class="box-header with-border">
                                    <a data-toggle="modal" href="#normalModal1" class="btn btn-vk pull-right" style="margin-right: 20px;">เลือกแผ่นพับที่ต้องการใช้</a></div>
                                </div><br />
                            <br /><a class="text-blue">
                              แผ่นพับ ท1 การมีส่วนร่วมภาคประชาชน <br />
                              แผ่นพับ ท4  งานลูกรัง<br />
                              แผ่นพับ ท5  งานหินคลุก</a>

                          </div>
                          <div class="timeline-footer">
                            <a class="btn btn-bitbucket btn-xs" href="frm_techniqce1_2.aspx">ดูรายละเอียด/แก้ไข</a>
                            <a class="btn btn-danger btn-xs" href="#">ลบข้อมูล</a>
                          </div>
                        </div>
                      </li>
                      <!-- END timeline item -->
                 
                    </ul>

                        </div>
                      </div>
                    </div>

                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">เสนอแผน งปม.ให้กรมพิจารณา<span class="badge bg-yellow">0</span></a></h4></div>
                      <div id="collapse3" class="panel-collapse collapse">
                        <div class="box-body">
                             <ul class="timeline timeline-inverse">
                       <li class="time-label">
                        <span class="bg-red">กิจกรรม</span><a data-toggle="modal" href="#normalModal2" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">เพิ่มกิจกรรม</a> </h3>
                          <div class="timeline-body">
                         
				            <br /><a class="text-blue">  รอดำเนินการ....</a>
                             
                        
                          </div>
                         
                        </div>
                      </li>
                      <!-- END timeline item -->
                 
                    </ul>

                        </div>
                      </div>
                    </div>

                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">เวนคืนอสังหาริมทรัพย์<span class="badge bg-yellow">0</span></a></h4></div>
                      <div id="collapse4" class="panel-collapse collapse">
                        <div class="box-body">
                             <ul class="timeline timeline-inverse">
                      <li class="time-label">
                        <span class="bg-red">กิจกรรม</span><a data-toggle="modal" href="#normalModal2" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">เพิ่มกิจกรรม</a> </h3>
                          <div class="timeline-body">
                         
				            <br /><a class="text-blue">  รอดำเนินการ....</a>
                             
                        
                          </div>
                         
                        </div>
                      </li>
                      <!-- END timeline item -->
                 
                    </ul>

                        </div>
                      </div>
                    </div>

                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">จัดจ้างก่อสร้าง<span class="badge bg-yellow">0</span></a></h4></div>
                      <div id="collapse5" class="panel-collapse collapse">
                        <div class="box-body">
                             <ul class="timeline timeline-inverse">
                       <li class="time-label">
                        <span class="bg-red">กิจกรรม</span><a data-toggle="modal" href="#normalModal2" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">เพิ่มกิจกรรม</a> </h3>
                          <div class="timeline-body">
                         
				            <br /><a class="text-blue">  รอดำเนินการ....</a>
                             
                        
                          </div>
                         
                        </div>
                      </li>
                      <!-- END timeline item -->
                 
                    </ul>

                        </div>
                      </div>
                    </div>

                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">การก่อสร้าง<span class="badge bg-yellow">0</span></a></h4></div>
                      <div id="collapse6" class="panel-collapse collapse">
                        <div class="box-body">
                             <ul class="timeline timeline-inverse">
                      <li class="time-label">
                        <span class="bg-red">กิจกรรม</span><a data-toggle="modal" href="#normalModal2" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">เพิ่มกิจกรรม</a> </h3>
                          <div class="timeline-body">
                         
				            <br /><a class="text-blue">  รอดำเนินการ....</a>
                             
                        
                          </div>
                         
                        </div>
                      </li>
                      <!-- END timeline item -->
                 
                    </ul>

                        </div>
                      </div>
                    </div>

                    <div class="panel box box-warning">
                      <div class="box-header with-border">
                        <h4 class="box-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">คืนค้ำประกันสัญญา<span class="badge bg-yellow">0</span></a></h4></div>
                      <div id="collapse7" class="panel-collapse collapse">
                        <div class="box-body">
                             <ul class="timeline timeline-inverse">
                      <li class="time-label">
                        <span class="bg-red">กิจกรรม</span><a data-toggle="modal" href="#normalModal2" class="btn bg-aqua btn-xs pull-right">เพิ่มกิจกรรม</a>
                      </li>
                    
                      <li>
                        <i class="fa fa-list-alt bg-blue"></i>
                        <div class="timeline-item">
                          <h3 class="timeline-header"><a href="#">เพิ่มกิจกรรม</a> </h3>
                          <div class="timeline-body">
                         
				            <br /><a class="text-blue">  รอดำเนินการ....</a>
                             
                        
                          </div>
                         
                        </div>
                      </li>
                      <!-- END timeline item -->
                 
                    </ul>

                        </div>
                      </div>
                    </div>

<!-------------------------------เพิ่มกิจกรรม----------------------------------------------->
                         <div id="normalModal2" class="modal fade">
                           <div class="modal-dialog">
                              <div class="modal-content">
                              <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">เลือกกิจกรรมการมีส่วนร่วม</h4>
                        </div>
                        <div class="modal-body">

                 <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1-1" data-toggle="tab">ระดับ 1</a></li>
                  <li><a href="#tab_2-1" data-toggle="tab">ระดับ 2</a></li>
                  <li><a href="#tab_3-1" data-toggle="tab">ระดับ 3</a></li>
                  <li><a href="#tab_4-1" data-toggle="tab">ระดับ 4</a></li>
                  <li><a href="#tab_5-1" data-toggle="tab">ระดับ 5</a></li>
                </ul>

                <div class="tab-content">

<!--------------------------------------ประเภท 1------------------------------------------------------------>
                  <div class="tab-pane active" id="tab_1-1">
                     <table class="table table-condensed table-bordered">
                   
                     <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ชื่อกิจกรรม</small></th>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"checked></label></td>
                      <td class="actions" style=" text-align :center ;"> <a href="frm_add_techniqce_total.aspx"><small>แผ่นป้ายประชาสัมพันธ์</small></a></td>
                    </tr>
                   
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small> Web site</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>E-mail</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>จดหมายข่าว</small></td>
                    </tr>
                     
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>แถลงข่าว</small></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>วิทยุ</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>โทรทัศน์</small></td>
                    </tr>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>หนังสือพิมพ์</small></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>แผ่นพับ</small></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ประกาศตามระเบียบราชการ</small></td>
                    </tr>
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>Social listening</small></td>
                    </tr>
                    
                  </table>
                  </div>
                    
<!--------------------------------------ประเภท 2------------------------------------------------------------>
                  <div class="tab-pane" id="tab_2-1">
                      <table class="table table-condensed table-bordered">
                   
                     <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ชื่อกิจกรรม</small></th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>สัมภาษณ์รายบุคคล</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>สำรวจควมคิดเห็น</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>สนทนากลุ่มย่อย</small></td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>สายด่วน/สายตรง</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>พบประอย่างไม่เป็นทางการ</small></td>
                    </tr>


                  </table>

                  </div><!-- /.tab-pane -->

<!--------------------------------------ประเภท 3------------------------------------------------------------>
                  <div class="tab-pane" id="tab_3-1">
                          <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ชื่อกิจกรรม</small></th>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                     <td class="actions" style=" text-align :center ;">การจัดประชุม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">สัมมนาเชิงปฎิบัติการ</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">เวธีสาธารณะ</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">คณะทำงานเพื่อแลกเปลี่ยนข้อมูล</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                     <td class="actions" style=" text-align :center ;">ประชุมระดมสมอง </td>
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">ประชุมกลุ่มย่อย</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                        <td class="actions" style=" text-align :center ;">ประชุมระดมความคิด</td>
                    </tr>

                  </table>
                  </div><!-- /.tab-pane -->
<!--------------------------------------ประเภท 4------------------------------------------------------------>
                  <div class="tab-pane" id="tab_4-1">
                     <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ชื่อกิจกรรม</small></th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>คณะที่ปรึกษาร่วมภาคประชาชน</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>คณะกรรมการร่วมภาคประชาชน</small></td>
                    </tr>
                
                  </table>
                  </div><!-- /.tab-pane -->
<!--------------------------------------ประเภท 5------------------------------------------------------------>
                  <div class="tab-pane" id="tab_5-1">
                      <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ชื่อกิจกรรม</small></th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>การทำประชามติ</small></td>
                    </tr>
                  </table>
                  </div><!-- /.tab-pane -->
                    
                </div><!-- /.tab-content -->
                </div><!-- nav-tabs-custom -->         
                </div>
                <div class="modal-footer"><ul class="pagination pagination-sm no-margin pull-left">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
        <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
        <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

                          <div id="normalModal1" class="modal fade">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">แผ่นพับ</h4>
                        </div>
                        <div class="modal-body">

                 <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_t" data-toggle="tab">ประเภท ท</a></li>
                  <li><a href="#tab_b" data-toggle="tab">ประเภท บ</a></li>
                  <li><a href="#tab_p" data-toggle="tab">ประเภท ป</a></li>
                </ul>

                <div class="tab-content">

<!--------------------------------------ประเภท 1------------------------------------------------------------>
                  <div class="tab-pane active" id="tab_t/1">
                     <table class="table table-condensed table-bordered">
                   
                     <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">เลือก</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ประเภท</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">แผ่นพับ</th>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"checked></label></td>
                      <td class="actions" style=" text-align :center ;">ท 1</td>
                      <td class="actions" style=" text-align :left ;">ประชาชนมีส่วนร่วม</td>
                    </tr>
                   
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked></label></td>
                      <td class="actions" style=" text-align :center ;">ท 2</td>
                      <td class="actions" style=" text-align :left ;">งานดิน</td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 3.1</td>
                      <td class="actions" style=" text-align :left ;">งานท่อกลม กสล.</td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 3.2</td>
                      <td class="actions" style=" text-align :left ;">งานท่อลอดเหลี่ยม</td>
                    </tr>
                     
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked></label></td>
                      <td class="actions" style=" text-align :center ;">ท 4</td>
                      <td class="actions" style=" text-align :left ;">งานลูกรัง	</td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 5.1</td>
                      <td class="actions" style=" text-align :left ;">งานหินคลุก</td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 5.2</td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</td>
                    </tr>
                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 6.1</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางแอสฬลต์คอนกรีต</td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 6.2</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางคอนกรีต</td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 6.3</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางลาดยางเคพซีล</td>
                    </tr>
                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ท 7</td>
                      <td class="actions" style=" text-align :left ;">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 1</td>
                      <td class="actions" style=" text-align :left ;">ประชาชนมีส่วนร่วม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 2</td>
                      <td class="actions" style=" text-align :left ;">งานสำรวจเพื่อการก่อสร้างและการวางผัง</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 3</td>
                      <td class="actions" style=" text-align :left ;">งานฐานรากสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 4</td>
                      <td class="actions" style=" text-align :left ;">งานตอม่อสะพาน</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 5</td>
                      <td class="actions" style=" text-align :left ;">งานพื้นสะพานแบบหล่อในพื้นที่</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 6</td>
                      <td class="actions" style=" text-align :left ;">งานสะพานแบบคอนกรีตอัดแรง</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 7</td>
                      <td class="actions" style=" text-align :left ;">งานทางเท้าและราวสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 8</td>
                      <td class="actions" style=" text-align :left ;">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาด</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ส 9</td>
                      <td class="actions" style=" text-align :left ;">งานส่วนประกอบอื่นๆ</td>
                    </tr>
                  </table>
                  </div>
          
<!--------------------------------------ประเภท 3------------------------------------------------------------>
                  <div class="tab-pane" id="tab_b/1">
                          <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">เลือก</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">ประเภท</th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;">แผ่นพับ</th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 1</td>
                      <td class="actions" style=" text-align :left ;">ประชนมีส่วนร่วม</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 2.1</td>
                      <td class="actions" style=" text-align :left ;">งานท่อกลม คสล.</td> 
                   
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                     <td class="actions" style=" text-align :center ;">บ 2.2</td>
                      <td class="actions" style=" text-align :left ;">งานท่อลอดเหลี่ยม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 3 </td>
                      <td class="actions" style=" text-align :left ;">งานงานดิน</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 4</td>
                      <td class="actions" style=" text-align :left ;">งานลูกรัง </td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 5.1</td>
                      <td class="actions" style=" text-align :left ;">งานหินคลุก</td> 
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 5.2 </td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 5.3</td>
                      <td class="actions" style=" text-align :left ;">งานซ่อมสร้างผิวทางแอสฟัลต์คอนกรีต</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 6.1</td>
                      <td class="actions" style=" text-align :left ;">งานไพร์มโคท</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 6.2 </td>
                      <td class="actions" style=" text-align :left ;">งานแทคโคท</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 6.3</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางลาดยางเคพซีล</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 6.4</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางแอสฟัลต์คอนกรีต</td> 
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 6.5</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางคอนกรต</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 6.6 </td>
                      <td class="actions" style=" text-align :left ;">งานเสริมผิวลาดยางแอลฟัลต์คอนกรีต</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 7</td>
                      <td class="actions" style=" text-align :left ;">งานเครื่องหมายจราจร และสิ่งอำนวยความปลอดภัย</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.1</td>
                      <td class="actions" style=" text-align :left ;">งานสำรวจสะพาน</td> 
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.2</td>
                      <td class="actions" style=" text-align :left ;">งานฐานรากสะพาน</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 8.3</td>
                      <td class="actions" style=" text-align :left ;">งานตอม่อสะพาน</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 8.4</td>
                      <td class="actions" style=" text-align :left ;">งานพื้นสะพานแบบหล่อในพื้นที่</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.5</td>
                      <td class="actions" style=" text-align :left ;">งานสะพานแบบคอนกรีตอัดแรง</td> 
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">บ 8.6</td>
                      <td class="actions" style=" text-align :left ;">งานทางเท้าและราวสะพาน</td> 
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      
                     <td class="actions" style=" text-align :center ;">บ 8.7 </td>
                      <td class="actions" style=" text-align :left ;">งานพื้นคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</td> 
                   
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                       <td class="actions" style=" text-align :center ;">บ 8.8</td>
                      <td class="actions" style=" text-align :left ;">งานส่วนประกอบอื่นๆ ของสะพาน </td> 
                    </tr>

                    
                  </table>

<!--------------------------------------ประเภท 4------------------------------------------------------------>

                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_p/1">
                     <table class="table table-condensed table-bordered">
                   
                    <tr class="bg-blue-gradient">
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เลือก</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>ประเภท</small></th>
                      <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>แผ่นพับ</small></th>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 1</td>
                      <td class="actions" style=" text-align :left ;">ประชาชนมีส่วนร่วม</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 2</td>
                      <td class="actions" style=" text-align :left ;">งานดิน</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 3</td>
                      <td class="actions" style=" text-align :left ;">งานท่อกลม คสล.</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 4</td>
                      <td class="actions" style=" text-align :left ;">งานลูกรัง</td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 5.1</td>
                      <td class="actions" style=" text-align :left ;">งานหินคลุก</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 5.2</td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลูกรังกลับมาใช้ใหม่</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><input type="checkbox"></td>
                      <td class="actions" style=" text-align :center ;">ป 5.3</td>
                      <td class="actions" style=" text-align :left ;">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 6.1</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางแอสฟัลต์คอนกรีต</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 6.2</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางคอนกรีต</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 6.3</td>
                      <td class="actions" style=" text-align :left ;">งานผิวทางลาดยางเคพซีล</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ป 7</small></td>
                      <td class="actions" style=" text-align :left ;"><small>งานเครื่องหมายจราจร และสิ่งอำนวยความสะดวกปลอดภัย</small></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 8</td>
                      <td class="actions" style=" text-align :left ;">งานสำรวจเพื่อก่อสร้างและการวางผัง</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 9</td>
                      <td class="actions" style=" text-align :left ;">งานรากฐานสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 10</td>
                      <td class="actions" style=" text-align :left ;">งานตอม่อสะพาน</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ป 11</small></td>
                      <td class="actions" style=" text-align :left ;">งานพื้นสะพานแบบหล่อในพื้นที่</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 12</td>
                      <td class="actions" style=" text-align :left ;">งานสะพานแบบคอนกรีตอัดแรง</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 13</td>
                      <td class="actions" style=" text-align :left ;">งานทางเท้าและราวสะพาน</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 14</td>
                      <td class="actions" style=" text-align :left ;">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</td>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><small>ป 15</small></td>
                      <td class="actions" style=" text-align :left ;">งานส่วนประกอบอื่นๆ</td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;">ป 16</td>
                      <td class="actions" style=" text-align :left ;">จุดตัดรถไฟและทางจักรยาน</td>
                    </tr>

                  </table>

<!--------------------------------------ประเภท 5------------------------------------------------------------>
                  </div><!-- /.tab-pane -->

                 
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->         
             </div>
                <div class="modal-footer"><ul class="pagination pagination-sm no-margin pull-left">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
        <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
        <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
                  </div>
                </div></div><!-- /.box-body -->
               </div>     

            </div>

        </div>       
          </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
              </div>
          </div>
         </div></div></div>
               </div></div></div> 
        </section><!-- /.content -->
      </div>

     <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
</asp:Content>
