﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_b7.aspx.cs" Inherits="DRR_Citizen.frm_question_b7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper" style="min-height: 1096px;">
       
        <!-- Main content -->
        <section class="content">

            <div class="row">
            <div class="col-md-12">  

                 <div class="box box-solid box-primay">
     
                    <div class="box-body">
              <div class="nav-tabs-custom"> <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลแบบสอบถาม บ7 (CPM_UT0604)</b></h4></div>
                  <div id="tabs bg-info"> 
                   <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><span><i class="fa fa-sticky-note text text-green"></i> กรอกข้อมูล</span></a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><span><i class="fa fa-clone text text-green"></i> แบบสอบถาม</span></a></li>
                </ul>
                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1"><br />
   <!--------------------------------------ข้อมูลโครงการ------------------------------------------------>  
                     <b class="text-primary">ข้อมูลโครงการ </b>       
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">

                         <table class="table">
    	                       
                         <tr>
                        <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">โครงการก่อสร้างถนนสาย :</label>
                              <div class="col-sm-10">
                            <asp:TextBox ID="input1" runat="server" class="form-control" placeholder="ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) " ReadOnly ="true"></asp:TextBox>
                         </div> 
                             </div><!-- /.form-group --></td>
    		             </tr>
                       </table>

                          <table class="table">
    	                       
                         <tr>
                         <td class="actions" style=" text-align :left ;">  <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">อำเภอ :</label>
                           <div class="col-sm-8">
                           <select class="form-control select2" style="width: 100%;" disabled>
                            <option selected="selected" >-</option>
                            <option>ชลบุรี</option>
                            <option>นครราชสีมา</option>
                         </select></div>
                         </div><!-- /.form-group --> </td>

                          <td class="actions" style=" text-align :left ;">   <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                             <div class="col-sm-8">
                             <select class="form-control select2" style="width: 100%;" disabled>
                             <option selected="selected" >สตูล</option>
                             <option>ชลบุรี</option>
                             <option>นครราชสีมา</option>
                            </select></div>
                           </div><!-- /.form-group --></td>
                          </tr>
                        </table>
                       
                           </div></div>
                        </div></div>
                   
  <!--------------------------------------ข้อมูลผู้ควบคุมงาน------------------------------------------------>
                     <b class="text-primary">ข้อมูลผู้ควบคุมงาน</b>                           
                

                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้ควบคุมงาน :</label>
                              <div class="col-sm-8">
                          
                               <asp:TextBox ID="TextBox29" runat="server" class="form-control" placeholder="นายเดชา พรหมมี" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                                <div class="col-sm-8">
                               <asp:TextBox ID="TextBox37" runat="server" class="form-control" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                              <asp:TextBox ID="TextBox31" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                      </div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                             <div class="col-sm-8">
                           <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="แขวงทางหลวงชนบทสตูล" ReadOnly ="true"></asp:TextBox>
                      </div>
                            </div><!-- /.form-group --></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้วิศวกรโยธา :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                  </table>
                      
                           </div></div>
                        </div></div>
                 

                     <b class="text-primary">ข้อมูลผู้มีส่วนร่วม</b>
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้มีส่วนร่วม :</label>
                               <div class="col-sm-7">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">วันที่ร่วมตรวจสอบ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">บ้านเลขที่/หมู่ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                         </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;">
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">ตำบล :</label>
                          <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                        </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                          <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">โทรศัพท์ :</label>
                        <div class="col-sm-8">
                    <input type="text"  class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> 
                          <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">ที่ทำงาน/หน่วยงาน :</label>
                        <div class="col-sm-7">
                    <input type="text" class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">ถนน/ซอย :</label>
                           <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">อำเภอ :</label>
                           <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">จังหวัด :</label>
                           <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">รหัสไปรษณีย์ :</label>
                           <div class="col-sm-7">
                           <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --> </td>
                    <td class="actions" style=" text-align :left ;"></td>
                    </tr>
                   
                  </table>
                      
                           </div></div>
                        </div></div>

                <a href="#tab_2" data-toggle="tab" aria-expanded="true"> <button class="btn btn-success pull-right">บันทึกถัดไป</button></a>
                  
                <br /><br /></div><!-- /.box-body -->
                 
                  <div class="tab-pane" id="tab_2">
                   
                    <div class="row">
       

                   <div class="col-xs-12">

                      <div class="box box-solid box-primary">
                        <div class="box-body box-profile">
                  
                            <h3 class="profile-username">แบบสอบถาม บ7 : งานเครื่องหมายจราจร และสิ่งอำนวยความปลอดภัย</h3>
                           

                            <ul class="list-group list-group-unbordered">

<!-------------------------------------------------------Choice-1--------------------------------->
                                    <li class="list-group-item">

                           <h5 class="text-blue"><b>1.  มีการตรวจขนาดและความหนาป้ายจราจรหรือไม่<a class="pull-right">ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                      
                           <div class="col-xs-1"></div>

                           <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td><input type="checkbox"> มีการตรวจ </td>
                                 <td><input type="checkbox"> ไม่มีการตรวจ </td>
    		                      </tr>
                              
                                </table>

                                  </div>   
                                           
                             <div class="col-xs-1"></div>
                          </div></li>
  
 <!-------------------------------------------------------Choice-2--------------------------------->                                                                 
                                    <li class="list-group-item">
                                   
                                        <h5 class="text-blue"><b>2. มีการตรวจวัดค่าสะท้อนแสงป้ายจราจรหรือไม่ <a class="pull-right">ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                               <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                                <div class="col-xs-1"></div>
                             <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td><input type="checkbox"> มีการตรวจ </td>
                                 <td><input type="checkbox"> ไม่มีการตรวจ </td>
    		                      </tr>
                              
                                </table>
                                  </div> 
                           <div class="col-xs-1"></div>  
                         </div></li>

<!-------------------------------------------------------Choice-3--------------------------------->
                                    <li class="list-group-item">
                                   
                                        <h5 class="text-blue"><b>3. มีการตรวจวัดขนาดเสาป้ายจราจรหรือไม่ <a class="pull-right">ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                                <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                  <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td><input type="checkbox"> มีการตรวจ </td>
                                 <td><input type="checkbox"> ไม่มีการตรวจ </td>
    		                      </tr>
                              
                                </table>
                              </div>
                           <div class="col-xs-1"></div>
                         </div></li>

<!-------------------------------------------------------Choice-4--------------------------------->
                                    <li class="list-group-item">
                                    
                                        <h5 class="text-blue"><b>4. การมองเห็นป้ายจราจร (ตำแหน่งติดตั้งป้าย)<a class="pull-right">ข้าม</a></b></h5>

                                      <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed"> 
                                <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                               <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td><input type="checkbox"> ชัดเจน </td>
                                 <td><input type="checkbox"> ไม่ชัดเจน</td>
    		                      </tr>
                              
                                </table>
                              </div>
                           <div class="col-xs-1"></div>
                         </div></li>

<!-------------------------------------------------------Choice-5--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>5.มีป้ายแจ้งสถานที่ติดต่อเมื่อถนนชำรุดเสียหาย<a class="pull-right">ข้าม</a></b></h5>
                            <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div> 
                             <div class="col-xs-10">
                           
                                 <table class="table table-bordered table-condensed">
                                <tr class="bg-info">
                                  <td width="50%"><input type="checkbox"> มี </td>
                                  <td><input type="checkbox"> ไม่มี </td>
                                </tr>
                               
                                 </table>
                               
                              </div>
                           <div class="col-xs-1"></div>
                           
                         </div></li>

<!-------------------------------------------------------Choice-6--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>6.ข้างหลังป้ายมีเบอร์สายด่วน โทร 1146<a class="pull-right">ข้าม</a></b></h5>
                            <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                      <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  มี </td> 
                                 <td width="50%"><input type="checkbox"> ไม่มี</td>
    		                      </tr>

                                 
                                </table>
                                      
                                 <div class="col-xs-1"></div>
 
                                </div>
                           
                         </div></li>
<!-------------------------------------------------------Choice-7--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>7. การตรวจวัดสิ่งอำนวยความสะดวกปลอดภัย<a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                               <table class="table table-bordered table-condensed">
                                <h5 class="text-blue"><b>7.1 มีการตวจหักโค้งหรือไม่</b></h5>
                                  <tr class="bg-info">
                                   <td width="50%"><input type="checkbox" checked="checked"> มีการตรวจวัด</td>
                                   <td><input type="checkbox">  ไม่มีการตรวจวัด </td>
                                 </tr>

                                </table>
                                 <table class="table table-bordered table-condensed">
                                <h5 class="text-blue"><b>7.2 มีการตวจหลักกิโลเมตรหรือไม่</b></h5>
                                  <tr class="bg-info">
                                   <td width="50%"><input type="checkbox" checked="checked"> มีการตรวจวัด</td>
                                   <td><input type="checkbox">  ไม่มีการตรวจวัด </td>
                                 </tr>

                                </table>

                                <table class="table table-bordered table-condensed">
                                <h5 class="text-blue"><b>7.3 มีการตวจวัดราวเหล็กก้นโค้ง</b></h5>
                                  <tr class="bg-info">
                                   <td width="50%"><input type="checkbox" checked="checked"> มีการตรวจวัด</td>
                                   <td><input type="checkbox">  ไม่มีการตรวจวัด </td>
                                 </tr>

                                </table>
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li> 
                                
 <!-------------------------------------------------------Choice-8--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>8. การตรวจวัดขนาดสีตีเส้น<a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                        <h5 class="text-blue"><b>8.1 มีการตรวจวัดหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                <tr class="bg-info">
                                   <tr class="bg-info">
                                   <td><input type="checkbox">  ไม่มี </td>
                                   </tr>
                                   <tr class="bg-info">
                                   <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-5 control-label text-right">วัดได้หนา (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                </tr>
                               </tr>
                                    <tr class="bg-info">
                                   <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-5 control-label text-right">วัดได้กว้าง (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                   </tr>
                                <tr class="bg-info">
                                   <td><input type="checkbox">  ไม่มี </td>
                               </tr>
                                </table>
                                
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li>  

<!-------------------------------------------------------Choice-9--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>9. การตรวจวัดการสะท้อนแสงสีตีเส้น<a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                        <h5 class="text-blue"><b>9.1 เครื่องวัดค่าสะท้อนแสงเส้นจราจร</b></h5>
                                 <table class="table table-bordered table-condensed">
                                <tr class="bg-info">
                                   <td width="50%"><input type="checkbox" checked="checked"> มี</td>
                                   <td><input type="checkbox">  ไม่มี </td>
                               </tr>
                                </table>

                              </div>
                            <div class="col-xs-1"></div>
                         </div></li>  

<!-------------------------------------------------------Choice-10--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>10. การสะท้อนแสงของป้ายและสีตีเส้นขณะขับขี่ตอนกลางคืนเป็นอย่างไร  <a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                               <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  ชัดเจน </td>
                                 <td width="50%"><input type="checkbox">  ไม่ชัดเจน </td>
    		                   </tr>

                                </table>
                                
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li>  

<!-------------------------------------------------------Choice-11--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>11. ควรติดตั้งป้ายจราจรเพิ่มเติม<a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                               <tr class="bg-info">
                                   <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-5 control-label text-right">บริเวณกิโลเมตรที่ (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                </tr>
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  ไม่ต้องติดตั้งเพิ่ม </td>
    		                   </tr>

                                </table>
                                
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li>  
<!-------------------------------------------------------Choice-12--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>12. ข้อเสนอแนะ<a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                               <tr class="bg-info">
                                 <td COLSPAN="2"><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                                 <textarea class="form-control" rows="4" placeholder="เนื่องจาก" ></textarea>
                                </div></div></td></tr>
                                </table>
                                
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li>  

                                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกแล้วทำต่อ</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div> 
                      </ul></div> 

                           
                 
                  
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>

                      
                 </div>
                </div><!-- /.tab-pane -->

               </div></div>
                </div>
              
              </div><!-- nav-tabs-custom -->
            </div>
         </div>
        </section><!-- /.content -->
      </div>


</asp:Content>