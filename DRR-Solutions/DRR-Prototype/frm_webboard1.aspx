﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_webboard1.aspx.cs" Inherits="DRR_Citizen.frm_webboard1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
       <style>
      .color-palette {
        height: 35px;
        line-height: 35px;
        text-align: center;
      }
      .color-palette-set {
        margin-bottom: 15px;
      }
      .color-palette span {
        display: none;
        font-size: 12px;
      }
      .color-palette:hover span {
        display: block;
      }
      .color-palette-box h4 {
        position: absolute;
        top: 100%;
        left: 25px;
        margin-top: -40px;
        color: rgba(255, 255, 255, 0.8);
        font-size: 12px;
        display: block;
        z-index: 7;
      }
    </style>
 

<div class="content-wrapper">
     
<!--------------- Main content ------------------------------------------------------------------->
        <section class="content">
           <div class="row">
             <div class="col-md-12">  

                 <div class="box box-solid box-primary">
     
                    <div class="box-body">
                      <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลเว็บบอร์ด (CPM_UT0704)</b></h4> </div>

                  <div id="tabs bg-primary">
                   <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab"><span><i class="fa fa-group text text-green"></i> ข้อมูลภายในกรม</span></a></li>
                     <li><a href="#tab_2" data-toggle="tab"><span><i class="fa fa-comments text text-green"></i> ข้อเสนอแนะจากประชาชน</span></a></li>
                     
                   </ul>
                 <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                      
                    <div class="box-body">
                     <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> สอบถาม</i></a></div>

                    <div class="box-body no-padding">
                     <table class="table table-bordered table-striped table-hover">
                       <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;"></th>
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">วันที่ลงประกาศ</th>
                      <th class="actions" style=" text-align :center ;">อ่าน/ตอบ</th>
                    </tr> 

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_webboard2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>สอบถามเรื่องการทำแผ่นพับว่าต้องทำทั้งหมดหรือไม่ </small></a></td>
                      <td><a href="#"><small>18/10/2556, 10:55</small></a></td>
                      <td><a href="#"><small>อ่าน 739 ครั้ง/ตอบ 20 ครั้ง</small></a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>เสนอแนะการจัดการรถ บนถนนกัลปพฤกษ์ </small></a></td>
                      <td><a href="#"><small>13/12/2556 08:40</small></a></td>
                      <td><a href="#"><small>อ่าน 1007 ครั้ง/ตอบ 15 ครั้ง</small></a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>ทำไมถนนสมัยนี้พังเร็ว </small></a></td>
                      <td><a href="#"><small>10/12/2556 09:48</small></a></td>
                      <td><a href="#"><small>อ่าน 1326 ครั้ง/ตอบ 10 ครั้ง</small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>เนินสะดุดชะลอความเร็ว ทำเองได้มั๊ย </small></a></td>
                      <td><a href="#"><small>8/12/2556 03:55</small></a></td>
                      <td><a href="#"><small>อ่าน 1126 ครั้ง/ตอบ 12 ครั้ง<//small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>อยากให้ช่วยพิจารณาด้วยครับ</small> </a></td>
                      <td><a href="#"><small>17/12/2556 01:54</small></a></td>
                      <td><a href="#"><small>อ่าน 1085 ครั้ง/ตอบ 2 ครั้ง</small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>สอบถามเรื่องการขยายถนนพุทธสาคร (ทางหลวงชนบท สค.1018) </small></a></td>
                      <td><a href="#"><small>22/11/2556 10:08</small></a></td>
                      <td><a href="#"><small>อ่าน 1430 ครั้ง / ตอบ 1 ครั้ง</small></a></td>
                     </tr>
                      
                
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>สวนหย่อมใต้สะพานภูมิพล กลายเป็นแหล่งเสื่อมโทรม </small></a></td>
                      <td><a href="#"><small>18/11/2556 10:25</small></a></td>
                      <td><a href="#"><small>อ่าน 1675 ครั้ง/ ตอบ 3 ครั้ง</small></a></td>
                    </tr>

                   

                     
                  </table>
                </div><!-- /.box-body -->
                
                    <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนหลัง</a></li>
                  </ul> 
                </div>
           
                   </div>
                  
                    <div class="tab-pane" id="tab_2">
     
                  <div class="box-body">
                     <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มข้อเสนอแนะ</i></a></div>

                    <div class="box-body no-padding">
                     <table class="table table-bordered table-striped table-hover">
                       <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;"></th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                      <th class="actions" style=" text-align :center ;">รูปภาพ</th>
                      <th class="actions" style=" text-align :center ;">ข้อเสนอแนะ</th>
                      <th class="actions" style=" text-align :center ;">พิกัด</th>
                      <th class="actions" style=" text-align :center ;">วันที่ลงประกาศ</th>
                      <th class="actions" style=" text-align :center ;">อ่าน/ตอบ</th>
                    </tr> 

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="frm_webboard2.aspx"><small>ขอบคุณที่กรมทางหลวงชนบทจัดทำเว็บบอร์ด ไว้รับเรื่องครับ </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>18/10/2012 10:55:33 am</small></a></td>
                      <td><a href="#"><small>อ่าน 739 ครั้ง/ตอบ 20 ครั้ง</small></a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>เสนอแนะการจัดการรถ บนถนนกัลปพฤกษ์ </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>13/12/2556 08:40</small></a></td>
                      <td><a href="#"><small>อ่าน 1007 ครั้ง/ตอบ 15 ครั้ง</small></a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>ทำไมถนนสมัยนี้พังเร็ว </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>10/12/2556 09:48</small></a></td>
                      <td><a href="#"><small>อ่าน 1326 ครั้ง/ตอบ 10 ครั้ง</small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="#"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>เนินสะดุดชะลอความเร็ว ทำเองได้มั๊ย </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>8/12/2556 03:55</small></a></td>
                      <td><a href="#"><small>อ่าน 1126 ครั้ง/ตอบ 12 ครั้ง<//small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="#"><small>อยากให้ช่วยพิจารณาด้วยครับ</small> </a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>17/12/2556 01:54</small></a></td>
                      <td><a href="#"><small>อ่าน 1085 ครั้ง/ตอบ 2 ครั้ง</small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>สอบถามเรื่องการขยายถนนพุทธสาคร (ทางหลวงชนบท สค.1018) </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>22/11/2556 10:08</small></a></td>
                      <td><a href="#"><small>อ่าน 1430 ครั้ง / ตอบ 1 ครั้ง</small></a></td>
                     </tr>
                      
                
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>สวนหย่อมใต้สะพานภูมิพล กลายเป็นแหล่งเสื่อมโทรม </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>18/11/2556 10:25</small></a></td>
                      <td><a href="#"><small>อ่าน 1675 ครั้ง/ ตอบ 3 ครั้ง</small></a></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>ถนนทางหลวงชนบท นฐ 3004 สร้างไม่เสร็จ </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><a href="Webboard-2.aspx"><small>13/11/2015 02:23</small></a></td>
                      <td><a href="#"><small>อ่าน 1848 ครั้ง/ตอบ 4 ครั้ง</small></a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>อยากขอให้ทางหลวงชนบท ช่วยราดยางไหล่ทางบนถนน ฉช.3001 เพิ่มเติมหลังปรับผิวจราจรแล้วเสร็จครับ </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>2/10/2556 12:40</small></a></td>
                      <td><a href="#"><small>อ่าน 1237 ครั้ง/ตอบ 1 ครั้ง</small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>โค้งอันตรายหน้า วัดเดิมเจ้า และ รร.วัดเดิมเจ้า ไม่มีสัญญาณหรือป้ายเตือน </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>12/7/2556 03:33</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><small>อ่าน 1008 ครั้ง/ ตอบ 3 ครั้ง</small></a></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="Webboard-2.aspx"><i class="fa fa-comment"></i></a></td>
                      <td><a href="Webboard-2.aspx"><small>ขอความร่วมมือ </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small></small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="frm_webboard2.aspx"><small> </small></a></td>
                      <td><a href="#"><small>8/7/2556 06:38</small></a></td>
                      <td><a href="#"><small>อ่าน 983 ครั้ง/ตอบ 2 ครั้ง</small></a></td>
                    </tr>

                     
                  </table>
                </div><!-- /.box-body -->
                
                  <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนหลัง</a></li>
                  </ul> 
                </div>
            
                </div>
                 
             </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
                     </div><!-- nav-tabs-custom -->  
                 </div>
            </div></div>
         </section>
      </div>

</asp:Content>
