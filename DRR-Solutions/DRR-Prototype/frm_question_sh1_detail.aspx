﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_sh1_detail.aspx.cs" Inherits="DRR_Citizen.frm_question_sh1_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          
          <ol class="breadcrumb">
            <li><a href="#">หน้าหลัก</a></li>
             <li class="active">รายละเอียกแบบสอบถาม ซ1</li>
          </ol><br />
        </section>
		

<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
          <div class="col-md-12">  

               <div class="box box-solid box-primary">
               <div class="box-header with-border"><h5 class="pull-left"><b>สถานะแบบสอบถาม : ซ1 (รวม 10 )</b></h5></div>
               <div class="box-body">

                 <div class="box-body no-padding">
                   <table class="table table-bordered tab-content table-hover">
                      
                    <tr>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>เลือก</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>สีแบบสอบถาม</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>ชื่อ</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>นามสกุล</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>เบอร์โทร</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>วันที่ดำเนินการ</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>วันที่บันทึกข้อมูล</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>สถานะแบบสอบถาม</small></th> 
                      <th class="actions bg-gray" style=" text-align :center ;"><small>สถานะดำเนินการ</small></th> 
                      <th class="actions bg-gray" style=" text-align :center ;"><small>วันที่แก้ไข</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>ข้อที่แก้ไข</small></th>
                      <th class="actions bg-gray" style=" text-align :center ;"><small>วิธีการแก้ไข</small></th>
                      
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ดาโหด </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>เส็นทุ่ง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>เอกชัย </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ลิ้มเจริญอนันต์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>065-4353874</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>วิเชียร </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>เจ๊ะแอ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>054-4353094</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แก้ไขแล้ว</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>11/12/2014</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>จงรักษ์ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กำสัมฤทธิ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>080-4353200</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แก้ไขแล้ว</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>13/12/2014</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 4</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ออลาระ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>พัฒชา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>084-4357634</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ยังไม่แก้ไข</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 6</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>มนตรีย </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>หมานละงู</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ยังไม่แก้ไข</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>สาธิต </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ลารีน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>089-4353654</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>
                    
                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ยูฮัน </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>บูเทศ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-5463788</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>อามีน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กอลาบันหลง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>086-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>จงรักษ์ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กำสัมฤทธิ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>075-4365234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small><label><input type="checkbox"></label></small></a></td>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_sh1_detail.aspx"><span class="label label-warning">สีเหลือง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ออลาระ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>พัฒชา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2013</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353434</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                  </table>
                </div><!-- /.box-body -->
               
                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
                </div>
             
            </div>
           </div>
        </div>
     </div>
    </section>
   </div>
</asp:Content>
