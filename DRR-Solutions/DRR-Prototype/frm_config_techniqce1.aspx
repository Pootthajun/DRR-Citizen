﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="frm_config_techniqce1.aspx.cs" Inherits="DRR_Citizen.frm_config_techniqce1"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
     <div class="content-wrapper" style="min-height: 1096px;">
      
     <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>

<!-- Main content ----------------------------------------------------------->
        <section class="content">


         <div class="row">
          <div class="col-md-12">
             <asp:ScriptManager id="ScriptManager1" runat="server">
        </asp:ScriptManager>
             <asp:UpdatePanel id="UpdatePanel1" runat="server">
            <ContentTemplate>
               <div class="box box-solid box-primary">
     
                      <div class="box-body">
          <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลแผ่นพับ (CPM_UT0207)</b></h4></div>
           <div class="box-body">
                 <div class="row">
                         <form class="form-horizontal">
                    <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label text-right">ค้นหาแผ่นพับ :</label>
                      <div class="col-sm-7">
                    <%--<select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ก่อสร้างทาง</option>
                      <option>ก่อสร้างสะพาน</option>
                      <option>บำรุงรักษา</option>
                      <option>อำนวยความปลอดภัย</option>
                      <option>ซ่อมบำรุง</option>
                      <option>อื่น</option>
                    </select>--%>
                          <asp:DropDownList ID="ddl_type" runat="server" class="form-control select2" style="width: 100%;" AutoPostBack="True" OnSelectedIndexChanged="ddl_type_SelectedIndexChanged"></asp:DropDownList>
                      </div>
                    </div>
                
                  
                  </div><!-- /.box-body -->
                  
                </form>

                </div><div class="box-body">
               <br /><div class="row">
              <div class="col-md-1"></div>

                   <div class="col-md-10">

                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <%--<table class="table table-bordered table-condensed">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_add_techniqce1.aspx">ท 1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_add_techniqce1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td><a href="frm_add_techniqce1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t2.aspx">ท 2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t2.aspx">งานดิน</a></td> 
                     <td><a href="frm_techniqce_t2.aspx">2/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                       <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t2.aspx">ท 2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                      <td><a href="frm_question_t2.aspx">งานดิน</a></td> 
                     <td><a href="frm_techniqce_t2.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t31.aspx">ท 3.1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t31.aspx">งานท่อกลม กสล.</a></td> 
                      <td><a href="frm_question_t31.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t32.aspx">ท 3.2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t32.aspx">งานท่อลอดเหลี่ยม</a></td> 
                      <td><a href="frm_techniqce_t32.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t4.aspx">ท 4</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t4.aspx">งานลูกรัง</a></td> 
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t51.aspx">ท 5.1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t51.aspx">งานหินคลุก</a></td> 
                      <td><a href="frm_question_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t52.aspx">ท 5.2 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t52.aspx">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</a></td> 
                       <td><a href="frm_question_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t53.aspx">ท 5.3 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t53.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                       <td><a href="frm_question_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_t61.aspx">ท 6.1 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t61.aspx">งานผิวทางแอสฬลต์คอนกรีต</a></td> 
                       <td><a href="frm_question_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t62.aspx">ท 6.2</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t62.aspx">งานผิวทางคอนกรีต</a></td> 
                       <td><a href="frm_question_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t63.aspx">ท 6.3 </a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                       <td><a href="frm_question_t63.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_question_t7.aspx">ท 7</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_question_t7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td> 
                       <td><a href="frm_question_t7.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  </table>--%>

<asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-condensed">
                               <Columns>
                                   <asp:BoundField DataField="type" HeaderText="ประเภท">
                                   <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                   <ItemStyle CssClass="actions" HorizontalAlign="Center" VerticalAlign="Middle" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="version" HeaderText="เวอร์ชั่น">
                                   <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                   <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="detail" HeaderText="ชื่อแผ่นพับ">
                                   <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="date" HeaderText="วันที่ใช้">
                                   <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                   <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                   </asp:BoundField>
                                   <asp:BoundField DataField="status" HeaderText="สถานะ">
                                   <HeaderStyle  HorizontalAlign="Center" VerticalAlign="Middle" />
                                   <ItemStyle CssClass="label label-success" HorizontalAlign="Center" VerticalAlign="Middle" />
                                   </asp:BoundField>
                               </Columns>
                               <HeaderStyle CssClass="actions bg-primary" />
                           </asp:GridView>
           </div><!-- /.box-body -->

                           

                        </div><!-- /.box-body -->
        
                     
<!------------------------------------------ประเภท ส------------------------------->
                     <%--  <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s1.aspx">ส 1 </a></td>
                      <td><a href="frm_techniqce_9s1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s2.aspx">ส 2 </a></td>
                      <td><a href="frm_techniqce_9s2.aspx">งานสำรวจเพื่อการก่อสร้างและการวางผัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                     <td><a href="frm_techniqce_9t1.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s3.aspx">ส 3 </a></td>
                      <td><a href="frm_techniqce_9s3.aspx">งานฐานรากสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s4.aspx">ส 4</a></td>
                      <td><a href="frm_techniqce_9s4.aspx">งานตอม่อสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t4.aspx">ท 4</a></td>
                      <td><a href="frm_techniqce_9t4.aspx">งานลูกรัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s5.aspx">ส 5</a></td>
                      <td><a href="frm_techniqce_9s5.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s6.aspx">ส 6 </a></td>
                      <td><a href="frm_techniqce_9s6.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s7.aspx">ส 7 </a></td>
                      <td><a href="frm_techniqce_9s7.aspx">งานทางเท้าและราวสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s8.aspx">ส 8</a></td>
                      <td><a href="frm_techniqce_9s8.aspx">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s9.aspx">ส 9</a></td>
                      <td><a href="frm_techniqce_9s9.aspx">งานส่วนประกอบอื่นๆ</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->--%>
          

<!------------------------------------------ประเภท บ------------------------------->
                     <%--  <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b1.aspx">บ 1 </a></td>
                      <td><a href="frm_techniqce_9b1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b21.aspx">บ 2.1 </a></td>
                      <td><a href="frm_techniqce_9b21.aspx">งานท่อกลม กสล.</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                     <td><a href="frm_techniqce_9t1.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b22.aspx">บ 2.2 </a></td>
                      <td><a href="frm_techniqce_9b22.aspx">งานท่อลอดเหลี่ยม</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b3.aspx">บ 3 </a></td>
                      <td><a href="frm_techniqce_9b3.aspx">งานดิน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b4.aspx">บ 4</a></td>
                      <td><a href="frm_techniqce_9b4.aspx">งานลูกรัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p51.aspx">บ 5.1</a></td>
                      <td><a href="frm_techniqce_9p51.aspx">งานหินคลุก</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p52.aspx">บ 5.2 </a></td>
                      <td><a href="frm_techniqce_9p52.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t53.aspx">ท 5.3 </a></td>
                      <td><a href="frm_techniqce_9t53.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b53.aspx">บ 5.3 </a></td>
                      <td><a href="frm_techniqce_9b53.aspx">งานซ่อมสร้างผิวทางแอสฟัลต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b61.aspx">บ 6.1 </a></td>
                      <td><a href="frm_techniqce_9b61.aspx">งานไพร์มโคท</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b62.aspx">บ 6.2</a></td>
                      <td><a href="frm_techniqce_9b62.aspx">งานแทคโคท</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b63.aspx">บ 6.3 </a></td>
                      <td><a href="frm_techniqce_9b63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b64.aspx">บ 6.4</a></td>
                      <td><a href="frm_techniqce_9b64.aspx">งานผิวทางแอสฟัสต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b65.aspx">บ 6.5 </a></td>
                      <td><a href="frm_techniqce_9b65.aspx">งานผิวทางคอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b66.aspx">บ 6.6</a></td>
                      <td><a href="frm_techniqce_9b66.aspx">งานเสริมผิวทางลาดยางแอสฟัสต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b7.aspx">บ 7 </a></td>
                      <td><a href="frm_techniqce_9b7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b81.aspx">บ 8.1</a></td>
                      <td><a href="frm_techniqce_9b81.aspx"> งานสำรวจสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b82.aspx">บ 8.2 </a></td>
                      <td><a href="frm_techniqce_9b82.aspx">งานฐานรากสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b83.aspx">บ 8.3</a></td>
                      <td><a href="frm_techniqce_9b83.aspx">งานตอม่อสะพาน</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b84.aspx">บ 8.4 </a></td>
                      <td><a href="frm_techniqce_9b84.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                       <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b85.aspx">บ 8.5</a></td>
                      <td><a href="frm_techniqce_9b85.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p16.aspx">บ 8.6</a></td>
                      <td><a href="frm_techniqce_9b86.aspx">งานทางเท้าและราวสะพาน</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9b87.aspx">บ 8.7</a></td>
                      <td><a href="frm_techniqce_9b87.aspx">งานพื้นคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p16.aspx">บ 8.8</a></td>
                      <td><a href="frm_techniqce_9b88.aspx">งานส่วนประกอบอื่นๆ ของสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->--%>

<!------------------------------------------ประเภท ป------------------------------->
                    <%-- <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p1.aspx">ป 1 </a></td>
                      <td><a href="frm_techniqce_9p1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p2.aspx">ป 2 </a></td>
                      <td><a href="frm_techniqce_9p2.aspx">งานดิน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                     <td><a href="frm_techniqce_9t1.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p31.aspx">ป 3 </a></td>
                      <td><a href="frm_techniqce_9p3.aspx">งานท่อกลม กสล.</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p32.aspx">ป 4 </a></td>
                      <td><a href="frm_techniqce_9p4.aspx">งานลูกรัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                  <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p51.aspx">ป 5.1</a></td>
                      <td><a href="frm_techniqce_9p51.aspx">งานหินคลุก</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p52.aspx">ป 5.2</a></td>
                      <td><a href="frm_techniqce_9p52.aspx">การนำวัสดุชั้นทางบนถนนลูกลังเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p61.aspx">ป 5.3 </a></td>
                      <td><a href="frm_techniqce_9p61.aspx">การนำวัสดุชั้นทางบนถนนลาดยางเดิมกลับมาใช้ใหม่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p61.aspx">ป 6.1 </a></td>
                      <td><a href="frm_techniqce_9p61.aspx">งานผิวทางแอสฬลต์คอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p62.aspx">ป 6.2</a></td>
                      <td><a href="frm_techniqce_9p62.aspx">งานผิวทางคอนกรีต</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p63.aspx">ป 6.3 </a></td>
                      <td><a href="frm_techniqce_9p63.aspx">งานผิวทางลาดยางเคพซีล</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p7.aspx">ป 7</a></td>
                      <td><a href="frm_techniqce_9p7.aspx">งานเครื่องหมายจราจรและสิ่งอำนวยความสะดวกปลอดภัย</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p8.aspx">ป 8 </a></td>
                      <td><a href="frm_techniqce_9p8.aspx">งงานสำรวจเพื่อการก่อสร้างและการวางผัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                      <tr>
                       <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p9.aspx">ป 9</a></td>
                      <td><a href="frm_techniqce_9p9.aspx">งานรากฐานสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p10.aspx">ป 10 </a></td>
                      <td><a href="frm_techniqce_9p10.aspx">งานตอม่อสะพาน</a></td>
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p11.aspx">ป 11</a></td>
                      <td><a href="frm_techniqce_9p11.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p12.aspx">ป 12 </a></td>
                      <td><a href="frm_techniqce_9p12.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td>  
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">18/08/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                      <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p13.aspx">ป 13</a></td>
                      <td><a href="frm_techniqce_9p13.aspx">งานทางเท้าและราวสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p14.aspx">ป 14 </a></td>
                      <td><a href="frm_techniqce_9p14.aspx">งานพื้นถนนคอนกรีตเสริมเหล็กเพื่อพาดคอสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                    <tr>
                   <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9p15.aspx">ป 15</a></td>
                      <td><a href="frm_techniqce_9p15.aspx">งานส่วนประกอบอื่นๆ</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">25/07/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>

                  </table>
           </div><!-- /.box-body -->

                </div>--%>
<!------------------------------------------ประเภท ซ------------------------------->
                     <%-- <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_sh1.aspx">ซ 1 </a></td>
                      <td><a href="frm_techniqce_9sh1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                  </table>
           </div><!-- /.box-body -->

                </div>--%>

              <div class="col-md-1"></div>

            </div><!-- /.box-body --><br />
          
        </div></div>
                  </div>     
              
         
            </div> 
            </div>
                </ContentTemplate>
        </asp:UpdatePanel>
        </div>
        </div>
    </section>
   </div>

      
</asp:Content>
