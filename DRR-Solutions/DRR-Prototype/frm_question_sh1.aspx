﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_sh1.aspx.cs" Inherits="DRR_Citizen.frm_question_sh1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

     <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
       
        <section class="content">

            <div class="row">
            <div class="col-md-12">
             
              <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">กรอกข้อมูล</a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">แบบสอบถาม</a></li>
                </ul>
                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1"><br />
   <!--------------------------------------ข้อมูลโครงการ------------------------------------------------>  
                     <b class="text-primary">ข้อมูลโครงการ </b>       
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">

                         <table class="table">
    	                       
                         <tr>
                        <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">โครงการก่อสร้างถนนสาย :</label>
                              <div class="col-sm-10">
                            <asp:TextBox ID="input1" runat="server" class="form-control" placeholder="ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) " ReadOnly ="true"></asp:TextBox>
                         </div> 
                             </div><!-- /.form-group --></td>
    		             </tr>
                       </table>

                          <table class="table">
    	                       
                         <tr>
                         <td class="actions" style=" text-align :left ;">  <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">อำเภอ :</label>
                           <div class="col-sm-8">
                           <select class="form-control select2" style="width: 100%;" disabled>
                            <option selected="selected" >-</option>
                            <option>ชลบุรี</option>
                            <option>นครราชสีมา</option>
                         </select></div>
                         </div><!-- /.form-group --> </td>

                          <td class="actions" style=" text-align :left ;">   <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                             <div class="col-sm-8">
                             <select class="form-control select2" style="width: 100%;" disabled>
                             <option selected="selected" >สตูล</option>
                             <option>ชลบุรี</option>
                             <option>นครราชสีมา</option>
                            </select></div>
                           </div><!-- /.form-group --></td>
                          </tr>
                        </table>
                       
                           </div></div>
                        </div></div>
                   
  <!--------------------------------------ข้อมูลผู้ควบคุมงาน------------------------------------------------>
                     <b class="text-primary">ข้อมูลผู้ควบคุมงาน</b>                           
                

                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้ควบคุมงาน :</label>
                              <div class="col-sm-8">
                          
                               <asp:TextBox ID="TextBox29" runat="server" class="form-control" placeholder="นายเดชา พรหมมี" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                                <div class="col-sm-8">
                               <asp:TextBox ID="TextBox37" runat="server" class="form-control" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                              <asp:TextBox ID="TextBox31" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                      </div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                             <div class="col-sm-8">
                           <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="แขวงทางหลวงชนบทสตูล" ReadOnly ="true"></asp:TextBox>
                      </div>
                            </div><!-- /.form-group --></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้วิศวกรโยธา :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                  </table>
                      
                           </div></div>
                        </div></div>
                 

                     <b class="text-primary">ข้อมูลผู้มีส่วนร่วม</b>
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้มีส่วนร่วม :</label>
                               <div class="col-sm-7">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">วันที่ร่วมตรวจสอบ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">บ้านเลขที่/หมู่ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                         </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;">
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">ตำบล :</label>
                          <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                        </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                          <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">โทรศัพท์ :</label>
                        <div class="col-sm-8">
                    <input type="text"  class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> 
                          <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">ที่ทำงาน/หน่วยงาน :</label>
                        <div class="col-sm-7">
                    <input type="text" class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">ถนน/ซอย :</label>
                           <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">อำเภอ :</label>
                           <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">จังหวัด :</label>
                           <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">รหัสไปรษณีย์ :</label>
                           <div class="col-sm-7">
                           <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --> </td>
                    <td class="actions" style=" text-align :left ;"></td>
                    </tr>
                   
                  </table>
                      
                           </div></div>
                        </div></div>

                <a href="#tab_2" data-toggle="tab" aria-expanded="true"> <button class="btn btn-success pull-right">บันทึกถัดไป</button></a>
                  
                <br /><br /></div><!-- /.box-body -->
                 
                  <div class="tab-pane" id="tab_2">
                   
                    <div class="row">

                    <div class="col-xs-1"></div>

                   <div class="col-xs-10">

                      <div class="box box-solid box-primary">
                        <div class="box-body box-profile">
                  
                            <h3 class="profile-username">แบบสอบถาม ซ1 : ประชาชนมีส่วนร่วม</h3>
                           

                            <ul class="list-group list-group-unbordered">
                                    <li class="list-group-item">

                           <h5 class="text-blue"><b>1.  มีการจัดประชุมชี้แจงเป้าหมายการซ่อมบำรุง วิธีการซ่อมบำรุง/การอำนวยความปลอดภัยหรือไม่
                               <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มี </label></td>
                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-2 control-label text-right"><small>เวลา:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                                <div class="input-group-addon"> <i class="fa fa-clock-o"></i></div>
                                            </div><!-- /.input group --> </td>

                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-5 control-label text-right"><small>จำนวนผู้รับฟังการชี้แจง :</small></label>
                                     <div class="input-group">
                                                <input type="text" class="form-control text-center">
                                                <div class="input-group-addon"> <i class="fa fa-user"></i></div>
                                            </div><!-- /.input group --></td>
    		                    </tr>

                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                 <td></td>
                                <td></td>
    		                    </tr>
                                
                            </table>
                         </div></li>
                                    
                                    <li class="list-group-item">
                                    <div class="form-group">
                                        
                                        <h5 class="text-blue"><b>2.เครื่องจักร อุปกรณ์ เครื่องมือ ใช้ในการซ่อมบำรุงและอำนวยความปลดภัย ประกอบด้วยอะไรบ้าง
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                        <div class="box-body no-padding">
                           
                            <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>รถยนต์บรรทุก หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                                <div class="input-group-addon"> <i class="fa fa-car"></i></div>
                                            </div><!-- /.input group --> </td>
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>รถยนต์บรรทุกเล็ก หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                                <div class="input-group-addon"> <i class="fa fa-car"></i></div>
                                            </div><!-- /.input group --> </td>
                                
    		                    </tr>
                                
                            </table></div>
                            
                            <h5 class="text-blue"><b>2.1 วัสดุทีนำมาใช้ในการซ่อม
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                        <div class="box-body no-padding">
                           
                            <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions bg-info" style=" text-align :center ;"><label><input type="checkbox"><small> หินคลุก </small></label></td>
                                <td class="actions bg-info" style=" text-align :center ;"><label><input type="checkbox"><small> หินผสมยาง </small></label></td>
                                <td class="actions bg-info" style=" text-align :center ;"><label><input type="checkbox" checked="checked"><small> หินฝุ่น </small></label></td>
                                <td class="actions bg-info" style=" text-align :center ;"><label><input type="checkbox"><small> Cold mix ยางมะตอยผสมสำเร็จ </small></label></td>
                                </tr>
                                 <tr>
                                <td class="actions bg-info" style=" text-align :center ;"><label><input type="checkbox" checked="checked"><small> ยางมะตอย </small></label></td>
                                <td class="actions bg-info" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-5 control-label text-right"><small>จำนวนกี่ถัง:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                           </div><!-- /.input group --> </td>
                                <td class="actions bg-info" style=" text-align :center ;"></td>
                                <td class="actions bg-info" style=" text-align :center ;"></td>
                                </tr>
                                 <tr>
                                <td class="actions bg-warning" style=" text-align :center ;"><label><input type="checkbox" checked="checked"><small> มีรถขุดตัก </small></label></td>
                                <td class="actions bg-warning" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                           <div class="input-group-addon"> <i class="fa fa-car"></i></div>
                                       </div><!-- /.input group --> </td>
                                <td class="actions bg-warning" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                <td class="actions bg-warning" style=" text-align :center ;"></td>
                                </tr>

                                 <tr>
                                 <td class="actions bg-warning" style=" text-align :center ;"><label><input type="checkbox"><small> มีรถบดล้อเหล็ก </small></label></td>
                                <td class="actions bg-warning" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                           <div class="input-group-addon"> <i class="fa fa-car"></i></div>
                                       </div><!-- /.input group --> </td>
                                <td class="actions bg-warning" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                <td class="actions bg-warning" style=" text-align :center ;"></td>
                                </tr>

                                <tr>
                                <td class="actions bg-success" style=" text-align :center ;"><label><input type="checkbox" checked="checked"><small> มีเครื่องขุด </small></label></td>
                                <td class="actions bg-success" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                <td class="actions bg-success" style=" text-align :center ;"></td>
                                <td class="actions bg-success" style=" text-align :center ;"></td>
                                </tr>

                                <tr>
                                 <td class="actions bg-success" style=" text-align :center ;"><label><input type="checkbox"><small> มีเครื่องบดอัดเฉพาะจุด </small></label></td>
                                 <td class="actions bg-success" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                 <td class="actions bg-success" style=" text-align :center ;"></td>
                                 <td class="actions bg-success" style=" text-align :center ;"></td>
                                </tr>
                            </table>
                                 </div>

                                </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>3.การป้องกันอันตรายขณะดำเนินการซ่อมก่อนการลงมือซ่อมบำรุง/อำนวยความปลอดภัยหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>

                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มี </label></td>
                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>จำนวนที่ตั้งกรวย(อัน):</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                </tr>

                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มี </label></td>
                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>จำนวนป้ายขณะซ่อม(ป้าย):</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                </tr>

                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มีเจ้าหน้าที่จัดจราจร </label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                <td></td>
    		                    </tr>
                                
                            </table>
                         </div>
                                    </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>4.วิธีการซ่อมบำรุงและอำนวยความปลอดภัย </b></h5>
                                        <h5 class="text-blue">4.1 มีการวัดพื้นที่ชำรุดเสียหายก่อนและหลังดำเนินการหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></h5>

                                       <div class="box-body no-padding">
                           <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มี </label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                
    		                    </tr>
                            </table>
                           </div>
                                
                                <h5 class="text-blue">4.2 มีการบดอัดความแน่นหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></h5>

                                       <div class="box-body no-padding">
                            <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มี </label></td>
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                
    		                    </tr>
                                
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> มี </label></td>
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่มี </label></td>
                                
    		                    </tr>
                            </table>
                            
                         </div>
                                        <h5 class="text-blue">4.3 การลาดยางแต่ละชั้นได้ให้เวลายางในการซึมลงในพื้นทาง/ยางแตกตัว และแห้งหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></h5>

                                       <div class="box-body no-padding">
                           <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> ให้เวลา </label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่ให้เวลา </label></td>
                                
    		                    </tr>
                            </table>
                           </div>
                                    </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>5.สภาพซ่อมบำรุง มีความเรียบร้อย สวยงาม แข็งแรง ปลอดภัยหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                            <table class="table table-condensed table-bordered">
                                
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> เรียบร้อย </label></td>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> ไม่เรียบร้อย </label></td>
                                <td></td>
                                </tr>

                            </table>
                         </div>
                                           
                                    </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>6.กรณีงานซ่อมมีการขุดเจาะวัสดุที่ไม่เหมาะสมออก อย่างเพียงพอหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                           <table class="table table-condensed table-bordered">
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                               <tr>
                               <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                                
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
    		                    </tr>

                            </table>
                         </div>
                                      
                                    </div></li>

                                    <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>7. ท่านได้เห็นความเสียหายของผิวทางที่ทำการซ่อมนี้มาเป็นเวลานานแค่ไหน
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>
                                        <div class="box-body no-padding">
                           
                            <table class="table table-condensed table-bordered">
                                 <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                                <tr>
                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>เป็นเวลา (วัน/เดือน):</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                               <td></td>

    		                    </tr>

                               
                            </table>
                         </div>
                                    </div></li>

                                <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>8.ท่านได้พบเห็นรถบรรทุกหนักใดบ้าง ใช้บนถนนเส้นนี้
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                           <table class="table table-condensed table-bordered">
                                <tr> <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                               <tr>
                               <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> รถบรรทุกอ้อย </label></td>
                               <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ช่วงเดือน</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                              <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ถึงเดือน</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                </tr>
                               <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox""> รถบรรทุกไม้ </label></td>
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ช่วงเดือน</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                              <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ถึงเดือน</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                </tr>

                               <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox"> รถบรรทุกหิน/ทราย </label></td>
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ช่วงเดือน</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                              <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ถึงเดือน</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
                                </tr>

                            </table>
                         </div></div></li>

                                <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>9.เส้นทางนี้เคยเกิดอุบัติเหตุที่บริเวณ
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                           <table class="table table-condensed table-bordered">
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                               <tr>
                               <td class="actions" style=" text-align :center ;">
                                    <div class="col-sm-12">
                      <textarea class="form-control" rows="3" placeholder="ข้อความ"></textarea>
                     </div></td>

    		                    </tr>

                            </table>
                         </div></div></li>

                                <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>10.ในช่วงฤดูฝนถนนเส้นนี้เคยถูกน้ำท่วมหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                           <table class="table table-condensed table-bordered">
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                               <tr>
                                <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> เคย </label></td>
                                <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ช่วง กิโลเมตร</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                                
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
    		                    </tr>

                            </table>
                         </div> </div></li>

                                <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>6.กรณีงานซ่อมมีการขุดเจาะวัสดุที่ไม่เหมาะสมออก อย่างเพียงพอหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                           <table class="table table-condensed table-bordered">
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                               <tr>
                               <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                                
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
    		                    </tr>

                            </table>
                         </div></div></li>

                                <li class="list-group-item">
                                    <div class="form-group">
                                        <h5 class="text-blue"><b>6.กรณีงานซ่อมมีการขุดเจาะวัสดุที่ไม่เหมาะสมออก อย่างเพียงพอหรือไม่
                                            <a class="pull-right"><input type="checkbox">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                           <table class="table table-condensed table-bordered">
                                <tr>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                               <tr>
                               <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>

                                
                                 <td class="actions" style=" text-align :center ;">
                                    <label for="inputName" class="col-sm-6 control-label text-right"><small>ใช้รถบดอัดเฉพาะจุด หมายเลขทะเบียน:</small></label>
                                       <div class="input-group">
                                                <input type="text" class="form-control text-center" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                       </div><!-- /.input group --> </td>
    		                    </tr>

                            </table>
                         </div></div></li>

                                </ul>

                               
                                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกแล้วทำต่อ</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div> 
                 
                  
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>

                   <div class="col-xs-1"></div>     
                 </div>
                </div><!-- /.tab-pane -->

                   </div>
                </div>
              
              </div><!-- nav-tabs-custom -->
            
          </div> <!-- /.row -->
         
        </section><!-- /.content -->
      </div>


</asp:Content>

