﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce2.aspx.cs" Inherits="DRR_Citizen.frm_techniqce2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
 
<div class="content-wrapper">
       
       <section class="content" runat ="server">
         
           <div class="row">
             <div class="col-md-12">


                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
                      <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูล Website (CPM_UT0406)</b></h4></div>
            

                      <div class="box-body">
                       
                      <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                                
    	                        <tr class="bg-blue-gradient">
    		                    <th class="actions" style=" text-align :center ;"><b>หัวข้อ</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>กรอกข้อมูล</b></th>
                                </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ชื่อเว็บไซต์/URL :</b></td>
    		                    <td width="80%"><div class="col-sm-12">
                                    <div class="input-group" style="width: 100%;">
                                    
                                    <input type="text1" class="form-control" id="inputnNameProject" placeholder="ข้อความ">
                                </div></div></td>
    		                    </tr>

                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		                    <td width="80%"><div class="col-sm-5">
                                    <div class="input-group" style="width: 100%;">
                                         <input type="text2" class="form-control" id="inputnPlan" placeholder="">
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div></div></td>
    		                    </tr>
                                <tr>

    		                    <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการจริง :</b></td>
    		                    <td width="80%"><div class="col-sm-5">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text3" class="form-control" id="inputnDate" placeholder="">
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>สถานที่ดำเนินการ :</b></td>
    		                    <td width="80%"><div class="col-sm-12">
                                    <div class="input-group" style="width: 100%;">
                                    <input type="text4" class="form-control" id="inputnPlace" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
    		                    <td width="80%"> <div class="col-sm-12">
                                <textarea class="form-control" rows="7" placeholder="ข้อความ"></textarea>
                                <span class="input-group-btn">
                                </span></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
    		                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text5" class="form-control" id="inputnSuccess" placeholder="ข้อความ"></div></div></td>
    		                    </tr>

                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
    		                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                   <input type="text6" class="form-control" id="inputnOther" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                                   <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;"
                                 <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                                 <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                                 <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                                 <div class="col-sm-12">
                                     <div class="input-group" style="width: 100%;"></div>
                                 </div></div></td> 
    		                    </tr>
                            </table> <br />
                      </div><!-- /.box-body -->

                     <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   </div>
                        </div>
                       </div><!-- /.box-body -->
                     </div>
                       
                      
            </div><!-- /.box-body -->
        
        </section><!-- /.content -->

    </div>

</asp:Content>

