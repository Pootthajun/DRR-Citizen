﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_typeproject2.aspx.cs" Inherits="DRR_Citizen.frm_config_typeproject2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
             <div class="box box-solid box-primary">

       <div class="box-body">
             <div class="box-body">
                 <div class="row">
                    <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลโครงการ (CPM_UT0206)</b></h4></div>
                    <form class="form-horizontal">
                        <div class="col-md-6">
           
                    
                        <div class="box-body">

                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">รหัสโครงการ :</label>
                      <div class="col-sm-8">
                        <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="01" ReadOnly ="true"></asp:TextBox>
                      </div><br />
                    </div>
                     
                      </div>

                       </div>

                        <div class="col-md-6">
           
                        <div class="box-body">

                   
                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">ชื่อโครงการ :</label>
                      <div class="col-sm-8"> 
                        <asp:TextBox ID="input1" runat="server" class="form-control text-left" placeholder="ก่อสร้างทาง " ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div>
                      
                        </div> <br /> 
                   
                      </div></form></div>
                     <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  </div><!-- /.box -->
                    
                
                  
                </div>
            </div>
            </div><!-- /.box-body -->
            
               <!-- /.box -->
            </div>
          
        </section><!-- /.content -->
      </div>
</asp:Content>
