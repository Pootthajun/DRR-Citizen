﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce16MOU.aspx.cs" Inherits="DRR_Citizen.frm_techniqce16MOU" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
   <div class="content-wrapper">
       
       <section class="content" runat ="server">
         
           <div class="row">
             <div class="col-md-12">
                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
             <div class="box-header with-border"><h4 class="pull-left text-blue"><b> จัดการการประชุม MOU <small>โครงการ ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) </small>(CPM_UT0433)</b></h4></div>
              <div class="box-body">

                      
                    <div class="box-body">
                   <div class="box-body no-padding"style="overflow-x:auto;">
                   <table class="table table-bordered">
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                     </tr>
                    <tr>
    		         <br/><td class="actions" style=" text-align :right ;"><b>ชื่อการประชุม :</b></td>
                     <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="บันทึกความร่วมมือ 3 ฝ่าย การบริหารโครงการเพื่อความโปร่งใส " ReadOnly ="true"></asp:TextBox></div></div></td>
                     </tr>
                 
                      <tr>
    		          <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		          <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;">
                    <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
    		         </tr>
                    
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการ :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="20/12/2557" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
                     </tr>
                   
                       <tr>
    		         <td class="actions" style=" text-align :right ;"><b>เวลา :</b></td>
                     <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="09.00-11.00" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>

                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>พิกัดที่อยู่ ละติจูด - ลองติจูด:</b></td>
                     <td width="80%"><div class="col-sm-5"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox7" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox></div></div>

                    <div class="col-sm-5"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox9" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox></div></div>
                     </td>
                    </tr>

                    <tr>
    		        <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
                      <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                      <textarea class="form-control" rows="7" placeholder="เมื่อวันที่ 20 ธันวาคม 2557 สำนักงานทางหลวงชนบทที่ 12 (สงขลา) ได้จัดประชุม MOU มีผู้เข้าร่วม 3 ฝ่าย ได้แก่ ผู้รับจ้าง  ประชาชน  กรมทางหลวงชนบท " disabled></textarea>
                     </div></div></td></tr>
                    
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
                      <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="การบันทึกเรียบร้อย" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b> </td>
                      <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>

<!-----------------------------------MOU------------------------------------------>
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>แนบเอกสาร MOU :</b> <br />
                         <div class="radio">
                        <label><input type="radio" name="optionsRadios" id="options1" value="option3" checked>มี</label>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
                         <label><input type="radio" name="optionsRadios" id="options2" value="option3">ไม่มี</label></div> 
                       <br />
                           <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>

                     <td width="80%">
                      <br />

                       <i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                          <div class="attachments">
							<ul><br />
								
								<li>
									<span class="label label-info">pdf</span> <b>รายชื่อผู้ร่วมทำสัญญา.pdf</b> <i>(7KB)</i>
                                     &nbsp;&nbsp;&nbsp;<span class="label label-danger"> <i class="fa fa-close" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span>
									<span class="quickMenu">
										<a href="#" class="glyphicons search"><i></i></a>
										<a href="#" class="glyphicons share"><i></i></a>
										<a href="#" class="glyphicons cloud-download"><i></i></a>
									</span>
								</li><br />
							</ul>		
						</div>
                       </td>
                    </tr>

                   <tr>
    		        <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                            <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                      <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,png)</i>
                      <div class="input-group" style="width: 100%;">
                      <div class="timeline-body"><br />

                            <img src="dist/img/news/t1-2.png" width="150" height="150" /> 
                          
                             <img src="dist/img/news/t1-1.png" width="150" height="150">
                      
                    </div></div>
                       </div></td> 
                    </tr>

                  </table>
                      </div>

                    </div>
                

            
           <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
                        </div></div><!-- /.box-body --> 

           </div></div></div>
        </section>

    </div>
</asp:Content>
