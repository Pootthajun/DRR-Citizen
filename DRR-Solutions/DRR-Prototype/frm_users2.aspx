﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_users2.aspx.cs" Inherits="DRR_Citizen.frm_users2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
<div class="content-wrapper">
      
<!-- Main content ----------------------------------------------------------->
        <section class="content">
                <div class="row">
     
               <div class="col-md-12">  

                 <div class="box box-widget widget-user-2">
                 <div class="widget-user-header bg-teal-gradient" >
                  <div class="widget-user-image">
                 <img class="img-circle" src="../dist/img/user1-128x128.jpg" alt="User Avatar">
                 <h5 class="widget-user-username">นายเอกชัย  ลิ้มเจริญอนันต์ </h5>
                 <h5 class="widget-user-desc">เพิ่ม/แก้ไข</h5></div><!-- /.widget-user-image -->
              </div>
              
                    <div class="box box-solid box-primary"> 
                     <div class="box-body"><b class="text-info">ข้อมูลส่วนตัว </b> 
                      <div class="box-body">
                       <div class="row">
                       <table class="table">
    	                     
                    <tr>
                        <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">เลขที่บัตรประชาชน :</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox14" runat="server" class="form-control" placeholder="3-1304-00102-91-2" ReadOnly ="true"></asp:TextBox>
                          </div>
                        </div><!-- /.form-group --> </td>
 
                        
                       <td class="actions" style=" text-align :left ;"><div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">คำนำหน้า :</label>
                       <div class="col-sm-8">
                       <asp:TextBox ID="TextBox1" runat="server" class="form-control" placeholder="นาย" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td> 
    		          </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-4 control-label ">ชื่อ :</label>
                     <div class="col-sm-8">
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control" placeholder="เอกชัย" ReadOnly ="true"></asp:TextBox></div>
                     </div><!-- /.form-group --></td>
  
                    <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                    <div class="col-sm-8">
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control" placeholder="ลิ้มเจริญอนันต์" ReadOnly ="true"></asp:TextBox></div>
                  </div><!-- /.form-group --> </td>
  
                  </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">บ้านเลขที่</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox4" runat="server" class="form-control" placeholder="4/4" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">หมู่ที่</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox5" runat="server" class="form-control" placeholder="5" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">ตำบล </label>
                        <div class="col-sm-8">
                        <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="บึงชำอ้อ" ReadOnly ="true"></asp:TextBox></div>
                        </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">จังหวัด </label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox7" runat="server" class="form-control" placeholder="ปทุมธานี" ReadOnly ="true"></asp:TextBox>
                         </div>
                       </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-4 control-label">อาชีพ </label>
                        <div class="col-sm-8">
                        <asp:TextBox ID="TextBox8" runat="server" class="form-control" placeholder="เกษตรกร" ReadOnly ="true"></asp:TextBox>
                    </div>
                  </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">การศึกษา </label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="TextBox9" runat="server" class="form-control" placeholder="มัธยมศึกษา" ReadOnly ="true"></asp:TextBox>
                            </div>
                          </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="TextBox10" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                       </div>
                     </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                        <div class="col-sm-8">
                        <asp:TextBox ID="TextBox12" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 1 (ปทุมธานี)" ReadOnly ="true"></asp:TextBox>
                        </div>
                        </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label"> เบอร์โทร </label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox11" runat="server" class="form-control" placeholder="0-2905-9522" ReadOnly ="true"></asp:TextBox>
                    </div>
                    </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :center ;"></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"></td>
                     <td class="actions" style=" text-align :left ;"></td> </tr>
                  </table><br />
                   </div></div>
                    </div>
                     
            <div class="row style="width: 100%;"">          
              <div class="col-md-1"> </div>

                <div class="col-md-10">
                        
                        
                 <div id="tabs  bg-lime-active"">
                 <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1-1" data-toggle="tab"><span><i class="fa fa-user text text-green"></i> ประวัติการเข้าร่วมการเป็นสมาชิก</span></a></li>
                  <li><a href="#tab_2-2" data-toggle="tab"><span><i class="fa fa-road text text-green"></i> ข้อมูลสายทางที่รับผิดชอบ</span></a></li>
                  <li><a href="#tab_3-2" data-toggle="tab"><span><i class="fa fa-trophy text text-green"></i> ประวัติการรับรางวัล</span></a></li>
                 
                 </ul>
                 <div class="tab-content">
                  <div class="tab-pane active" id="tab_1-1">
                  <div class="box box-solid box-primary"> 
                   <div class="box-body">
                     <div class="box-header with-border">
                    <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่ม</i></a></div>
                           
                       <div class="box-body no-padding">
                        <table class="table table-bordered tab-content table-hover">
    	               <tr class="bg-blue-gradient">
                                
                      <th class="actions" style=" text-align :center ;">ครั้งที่</th>
                      <th class="actions" style=" text-align :center ;">วันเข้าเป็นสมาชิก</th>
                      <th class="actions" style=" text-align :center ;">ถึงวันที่</th>
                      <th class="actions" style=" text-align :center ;">สถานะ</th>

    	                        </tr>
                                <tr>
                      <td class="actions" style=" text-align :center ;"><small>1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>5/1/2553</small></td>
                      <td class="actions" style=" text-align :center ;"><small>5/1/2555</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
    		                    </tr>
                                <tr>
                      <td class="actions" style=" text-align :center ;"><small>2</small> </td>
                      <td class="actions" style=" text-align :center ;"><small>15/1/ 2555</small></td>
                      <td class="actions" style=" text-align :center ;"><small>15/1/2559</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
    		                    </tr>
                                
                            </table>
                           </div></div>
                         </div>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_2-2">
                <div class="box box-solid box-success"> 
                    <div class="box-body">
                         <div class="box-header with-border">
                          <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่ม</i></a></div>
                           
                       <div class="box-body no-padding">
                        <table class="table table-bordered tab-content table-hover">
    	                <tr class="bg-blue-gradient">
                                
                      <th class="actions" style=" text-align :center ;">รหัสสายทาง</th>
                      <th class="actions" style=" text-align :center ;">ชื่อสายทาง</th>
                      <th class="actions" style=" text-align :center ;">ระยะทาง กม.</th></tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><small>ปท.4001</small> </td>
                      <td class="actions" style=" text-align :center ;"><small>แยกทางหลวงหมายเลข3261-บ้านคลองห้า</small> </td>
                      <td class="actions" style=" text-align :center ;"><small>16.365</small> </td>
    		        </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><small>ปท.4002</small> </td>
                      <td class="actions" style=" text-align :center ;"><small>แยกทางหลวงหมายเลข3261-บ้านคลองสิบ</small> </td>
                      <td class="actions" style=" text-align :center ;"><small>20.654</small></td>
    		         </tr>   
                            </table>
                           </div></div>
                       </div><!-- /.form-group -->
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_3-2">

                    <div class="box box-solid box-success"> 
                    <div class="box-body">
                        <div class="box-header with-border">
                        <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่ม</i></a></div>
                           
                       <div class="box-body no-padding">
                        <table class="table table-bordered tab-content table-hover">
    	                <tr class="bg-blue-gradient">
                                
                      <th class="actions" style=" text-align :center ;">ครั้งที่</th>
                      <th class="actions" style=" text-align :center ;">ชื่อรางวัล</th>
                      <th class="actions" style=" text-align :center ;">วันที่ได้รับ</th>
                      <th class="actions" style=" text-align :center ;">หมายเหตุ</th>

    	                        </tr>
                                <tr>
                      <td class="actions" style=" text-align :center ;"><small>1</small></td>
                      <td class="actions" style=" text-align :center ;"><small>อสทช. ดีเด่น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>5/1/2554</small></td>
                      <td class="actions" style=" text-align :center ;"></td>
    		                    </tr>
                             
                            </table>
                         </div>

                     </div>
                       
                      </div>

                  
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
               <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
              </div>
                      </div> 

              <div class="col-md-1"> </div>  
                  
                    </div>
              </div></div>
               </div>     
               </div><!-- /.box-body -->
      </section>
   </div>

</asp:Content>
