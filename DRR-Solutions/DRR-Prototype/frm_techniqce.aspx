﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce.aspx.cs" Inherits="DRR_Citizen.frm_techniqce" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
  <title>ตั้งค่ากระบวนงาน | กรมทางหลวงชนบท</title>

     <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        
          <ol class="breadcrumb">
            <li><a href="Dashboard.aspx">หน้าหลัก</a></li>
            <li><a href="config_department.aspx">ตั้งค่ากระบวนงาน</a></li>
          </ol>
        </section><br />

        <!-- Main content -->
      <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">

                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-maroon" style=" text-align :center ;">รหัส</th>
                      <th class="actions bg-maroon" style=" text-align :center ;">กระบวนงาน</th>
                      <th class="actions bg-maroon" style=" text-align :center ;">ดำเนินการ</th>
                     </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">01</td> 
                      <td class="actions" style=" text-align :center ;">วางแผน </td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">02</td> 
                      <td class="actions " style=" text-align :center ;">สำรวจและออกแบบ </td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                   
                    <tr>
                      <td class="actions" style=" text-align :center ;">03</td> 
                      <td class="actionsg" style=" text-align :center ;">เสนอแผนงปม.ให้กรมพิจารณา</td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                  <tr>
                      <td class="actions " style=" text-align :center ;">04</td> 
                      <td class="actions" style=" text-align :center ;">เวนคืนอสังหาริมทรัพย์ </td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                   <tr>
                      <td class="actionsg" style=" text-align :center ;">05</td> 
                      <td class="actions " style=" text-align :center ;">จัดจ้างก่อสร้าง</td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">06</td> 
                      <td class="actions " style=" text-align :center ;">การก่อสร้าง </td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                   <tr>
                      <td class="actions" style=" text-align :center ;">07</td> 
                      <td class="actions " style=" text-align :center ;">คืนค้ำประกันสัญญา </td> 
                     <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-edit">แก้ไข</i></span>
                        <span class="label label-danger"><i class="fa fa-edit">ลบ</i></span></td>
                    </tr>
                   
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
        
              </div>
           
            

          </div></div>
        </section>
      </div>

</asp:Content>

