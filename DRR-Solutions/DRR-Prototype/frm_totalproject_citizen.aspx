﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_totalproject_citizen.aspx.cs" Inherits="DRR_Citizen.frm_totalproject_citizen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
     

<!-- Main content ----------------------------------------------------------->
       <section class="content">
 <div class="row">
          <div class="col-md-12">  

             <div class="box box-solid box-primary">
     
                      <div class="box-body">
              <div class="box-header with-border"><h4 class="pull-left "><b>ข้อมูลโครงการ (CPM_UT0301)</b></h4>
                   <a href="frm_totalproject_citizen.aspx" class="pull-right">ประชาชน</a>
                   <a href="frm_totalproject.aspx" class="pull-right">ผู้คุมงาน/</a>
              </div>
                 <div class="box-body">
                   <div class="row">
                     <div class="col-md-6">
                          <form class="form-horizontal">
                              <div class="box-body">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label text-right">ปีงบประมาณ :</label>
                                <div class="col-sm-8">
                                    <select class="form-control select2" style="width: 100%;">
                                    <option selected="selected">ทั้งหมด</option>
                                    <option>2559 (2213 โครงการ)</option>
                                    <option>2558 (4122 โครงการ)</option>
                                    <option>2557 (1659 โครงการ)</option>
                                    <option>2556 (11 โครงการ)</option>
                                    <option>2555 (1 โครงการ)</option>
                               
                                </select></div>
                             </div><br />
                                 
                              </div><!-- /.box-body -->
                              </form> </div>

                  <div class="col-md-6">
                   <form class="form-horizontal">
                   <div class="box-body">
                   <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label ">ประเภทโครงการ :</label>
                    <div class="col-sm-8">
                  <select class="form-control select2" style="width: 100%;">
                   <option selected="selected">ทั้งหมด</option>
                   <option>ก่อสร้างทาง</option>
                   <option>ก่อสร้างสะพาน</option>
                   <option>บำรุงรักษา</option>
                   <option>อำนวยความปลอดภัย</option>
                   <option>ซ่อมบำรุง</option>
                   </select></div>
                  </div><!-- /.form-group -->
                                  
                         </div>
                         </form> </div>

                     <div class="col-md-12">
                      <form class="form-horizontal">
                        <div class="box-body">
                       
                        <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label text-left">ค้นหา :</label>
                        <div class="col-sm-10">
                        <div class="input-group" style="width: 100%;">
                        <input type="text" name="table_search" class="form-control input-sm" placeholder="ค้นหาตามเลขที่สัญญา,ชื่อโครงการ,ชื่อผู้ควบคุมงาน">
                     
                        </div>
                       </div></div><!-- /.form-group -->  
                      </div>
                     </form> </div>

                      </div>
                 <div class="col-md-12">
                   
                   <div class="box-body pad table-responsive">
                      <table class="table table-bordered text-center">
                    <tr>
                      <th><button class="btn btn-block btn bg-defult btn-lg">   
                        <span class="info-box-text">ก่อสร้างทาง</span>
                        <span class="badge bg-red">1425 (โครงการ)</span>
                         </button>
                       </th>

                      <th><button class="btn btn-block btn bg-defult btn-lg">
                        <span class="info-box-text">ก่อสร้างสะพาน</span>
                        <span class="badge bg-blue">1188 (โครงการ)</span>
                          </button></th>
                      <th><button class="btn btn-block btn bg-defult btn-lg">
                          <span class="info-box-text">บำรุงทาง</span>
                          <span class="badge bg-green">2825 (โครงการ)</span>
                          </button></th>
                      <th><button class="btn btn-block btn-defult btn-lg">
                         <span class="info-box-text">อำนวยความปลอดภัย</span>
                         <span class="badge bg-orange">1321 (โครงการ)</span>
                         </button></th>
                    <th><button class="btn btn-block btn-defult btn-lg">
                         <span class="info-box-text">ซ่อมบำรุงปกติ</span>
                         <span class="badge bg-yellow">1145 (โครงการ)</span>
                         </button></th>
                    </tr>
                 
                  </table>
                  </div><!-- /.box -->

                     
                   <div class="box-body pad table-responsive">
                      <table class="table table-bordered text-center">
                    <tr>
                      <th><button class="btn btn-block btn bg-defult btn-lg">   
                        <span class="info-box-text">สำรวจออกแบบ</span>
                        <span class="badge bg-red">0 (โครงการ)</span>
                         </button>
                       </th>

                      <th><button class="btn btn-block btn bg-defult btn-lg">
                        <span class="info-box-text">คืนค้ำประกันสัญญา</span>
                        <span class="badge bg-blue">0 (โครงการ)</span>
                      </button></th>
                     
                  </table>
                  </div><!-- /.box -->
               </div><!-- /.col -->  
            </div> 
             
               <div class="row no-print">
               <a href="New_project.aspx" class="btn bg-aqua pull-right" style="margin-right: 50px;"><i class="fa fa-plus"> เพิ่มโครงการ</i></a>
               <button class="btn bg-yellow pull-right" style="margin-right: 20px;"><span><b><i class="fa fa-search"></i></b>  ค้นหา </span> </button></div>
               </div><!-- /.box-body -->
<!-----------------------------------------------ตาราง ข้อมูลโครงการ------------------------------------------------------------>

         <ul class="list-group list-group-unbordered"> <li class="list-group-item">
           <div class="box-body">  
               <div class="box-body"> 
                <div class="box-body">     
                 <div class="box-body no-padding"style="overflow-x:auto;">
                    <table class="table table-bordered table-striped table-hover">
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;"><small>ปีงบประมาณ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ประเภทโครงการ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ชื่อโครงการ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>เลขที่สัญญา</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ความก้าวหน้าโครงการ(%)</small></th>
                      <th class="actions" style=" text-align :center ;"><small>สถานะแผ่นพับ</small></th>
                      <th class="actions " style=" text-align :center;"><small>หน่วยดำเนินงาน</small></th>
                      <th class="actions" style=" text-align :center ;"><small>กิจกรรมการมีส่วนร่วม</small></th>
                      <th class="actions" style=" text-align :center ;"><small>สถานะ ทชจ.</small></th>
                      <th class="actions" style=" text-align :center ;"><small>สถานะสำนัก</small></th>
                      <th class="actions" style=" text-align :center ;"><small>สถานะ สอร.</small></th>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>01/2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสายแยก ทช.สต.5041 - บ.ศาลเจ้า</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>033/2559</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสายแยก ทช.สต.5041 - บ.ศาลเจ้า</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>033/2559</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-yellow">70%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสายแยก ทล.4051 - บ.หาดทรายยาว</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>02/2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>03/2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>สายแยก ทล.22 - บ.โพนแพง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>004/2559</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสาย แยก ทล.1269 - ศูนย์พัฒนาโครงการหลวงห้วยเสี้ยว</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>005/2559</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_year2557.aspx"><small>2557</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_pj_t"><small>โครงการก่อสร้างทาง</small></a></td>
                      <td><a href="frm_timeline1.aspx"><small>ถนนสาย แยก ทล.2026 - น้ำตกถ้ำพระ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline1.aspx"><small>006/2559</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ท 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แขวงทางหลวงชนบทสตูล</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                      <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
                    </tr>
                  </table>
                </div><!-- /.box-body -->
                
                </div>
                  
                 <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">สุดท้าย</a></li>
                  </ul>
                </div>
               </div></div></li></ul>
         </div> 
     </div></div>
    </section>
   </div>
</asp:Content>
