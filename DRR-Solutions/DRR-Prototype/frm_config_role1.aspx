﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_role1.aspx.cs"  Inherits="DRR_Citizen.frm_config_role1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
             <div class="box box-solid box-primary">
<div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลสิทธ์ผู้ใช้งาน (CPM_UT0211)</b></h4></div>
                 
                       
                             <div class="box-body">
                              <div class="col-md-6">
           
                        <div class="box-body">

                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right">กลุ่มผู้ใช้งาน  :</label>
                    
                       <div class="col-sm-8">
                    <select class="form-control select2" style="width: 120%;">
                      <option>ผู้บริหาร</option>
                      <option>Admin</option>
                      <option>ผส. สำนักงานภูมิภาคส่วนกลาง</option>
                      <option>ผอ. แขวง</option>
                      <option>ผู้ตรวจรับแทน</option>
                      <option>ช่างผู้ควบคุมงาน</option>
                      <option>กลุ่มเครือข่ายที่เกี่ยวข้อง</option>
                      <option>อส.ทช</option>
                      <option>ประชาชน</option>
                    </select></div>

                    <br />
                    </div>
                      </div>

                       </div>

                               <div class="col-md-6">
           
                        <div class="box-body">

                   <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-right"><small>ค้นหา :</small></label>
                      <div class="col-sm-8">
                        <input type="text" name="table_search" class="form-control input-sm" placeholder="ตามชื่อ นามสกุล,ตำแหน่ง,ชื่อผู้ใช้ในระบบ.....">
                      </div><br />
                    </div>
                     
                      </div>

                       </div>
                         </div> 
                        </div>

        
                 <div class="box-body">
                 <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มสิทธิ์ผู้ใช้งาน</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ลำดับ</th>
                      <th class="actions" style=" text-align :center ;">ชื่อ</th>
                      <th class="actions" style=" text-align :center ;">นามสกุล</th>
                      <th class="actions" style=" text-align :center ;">ตำแหน่ง</th>
                      <th class="actions" style=" text-align :center ;">ชื่อผู้ใช้ในระบบ</th>
                      <th class="actions" style=" text-align :center ;">ชื่อกลุ่มผู้ใช้งาน</th>
                      <th class="actions" style=" text-align :center ;">แก้ไข</th> 
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>1 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_role2.aspx"><small>อานนท์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_role2.aspx"><small>แฮวอู</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_role2.aspx"><small>เจ้าหน้าที่ระบบสารสนเทศ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_role2.aspx"><small>abc</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_role2.aspx"><small>Admin</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>กลุ่มเครือข่าย</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>dds</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>กลุ่มเครือข่ายที่เกี่ยวข้อง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>3 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>สมยศ  </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แสงมณี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>อสทช.</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ggd</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>อสทช.</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>วีระ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ชื่นทรัพย์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผู้บริหาร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>wwa</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผู้บริหาร</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>5</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>เจ้าหน้าที่ระบบสารสนเทศ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>eew</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>Admin</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>6 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผส.สำนักภูมิภาคส่วนกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>htr</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผส.สำนักภูมิภาคส่วนกลาง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>7 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผอ.แขวง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>zxs</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผอ.แขวง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>8 </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>สนธยา </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>แก่นด้วง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผอ.ผู้ตรวจสอบแทน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>trt</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="Users-2.aspx"><small>ผอ.ผู้ตรวจสอบแทน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                  </table>
                </div><!-- /.box-body -->
                     <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนหลัง</a></li>
                  </ul>
                </div>
               </div><!-- /.box -->
            </div>
              


          </div></div>
        </section><!-- /.content -->
      </div>
</asp:Content>
