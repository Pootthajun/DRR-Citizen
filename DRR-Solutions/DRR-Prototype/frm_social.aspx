﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_social.aspx.cs" Inherits="DRR_Citizen.frm_social" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style>
      .color-palette {
        height: 35px;
        line-height: 35px;
        text-align: center;
      }
      .color-palette-set {
        margin-bottom: 15px;
      }
      .color-palette span {
        display: none;
        font-size: 12px;
      }
      .color-palette:hover span {
        display: block;
      }
      .color-palette-box h4 {
        position: absolute;
        top: 100%;
        left: 25px;
        margin-top: -40px;
        color: rgba(255, 255, 255, 0.8);
        font-size: 12px;
        display: block;
        z-index: 7;
      }
    </style>
    
  
<div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
     
    
          <div class="col-md-12">  

      <div class="box box-widget widget-user-2">
            <div class="box box-solid box-primary">
              <div class="widget-user-header bg-black" style="background: url('../dist/img/social.jpg') center center;">
                 <h3 class="widget-user-username text-black"><b>Socail Listening</b></h3>
                </div>
        
     
                      <div class="box-body">
       
             <div class="box-body">
                 <div class="row">

                
              <!-- Custom Tabs -->
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab">Social</a></li>
                  <li><a href="#tab_2" data-toggle="tab">ตั้งค่าระบบ</a></li>
                 
                 
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                   
                  <div class="col-sm-12">
                      <img id="zoom_05" src="../../dist/img/social1.png" style="width: 100%;" data-zoom-image="../../dist/img/drr-2/map.png"> 
                    </div>
               
                  </div><!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_2">
                    <div class="bg-orange disabled color-palette"></div>
                    <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                      <div class="bg-orange disabled color-palette"></div>
                  </div><!-- /.tab-pane -->
                 
                </div><!-- /.tab-content -->
              </div><!-- nav-tabs-custom -->
           
               
                </div>  <div class="footer">

                    <div class="text-center">
                    <a class="btn btn-social-icon btn-bitbucket"><i class="fa fa-bitbucket"></i></a>
                    <a class="btn btn-social-icon btn-dropbox"><i class="fa fa-dropbox"></i></a>
                    <a class="btn btn-social-icon btn-facebook"><i class="fa fa-facebook"></i></a>
                    <a class="btn btn-social-icon btn-flickr"><i class="fa fa-flickr"></i></a>
                    <a class="btn btn-social-icon btn-foursquare"><i class="fa fa-foursquare"></i></a>
                    <a class="btn btn-social-icon btn-github"><i class="fa fa-github"></i></a>
                    <a class="btn btn-social-icon btn-google"><i class="fa fa-google-plus"></i></a>
                    <a class="btn btn-social-icon btn-instagram"><i class="fa fa-instagram"></i></a>
                    <a class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
                    <a class="btn btn-social-icon btn-tumblr"><i class="fa fa-tumblr"></i></a>
                    <a class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
                    <a class="btn btn-social-icon btn-vk"><i class="fa fa-vk"></i></a>
                  </div>
                 
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            
               
           
             

            </div><!-- /.box-body -->
        
              </div>
                </div><!-- /.widget-user -->
    
      <div class="box box-solid box-primary">
               
                      <div class="box-body">
                          <div class="box-header with-border"><h4 class="pull-left"><b>Socail Listening </b></h4></div>
                            <br /> 
                         


                          <div class="col-md-12">
                           <div class="box box-solid box-primary">
                              <div class="row">
                         <div class="col-md-6">
                          <form class="form-horizontal">
                              <div class="box-body">
                              <div class="form-group">
                                <label for="inputEmail3" class="col-sm-4 control-label">ปีงบประมาณ :</label>
                                <div class="col-sm-8">
                                    <select class="form-control select2" style="width: 100%;">
                                    <option>2559 (2213 โครงการ)</option>
                                    <option>2558 (4122 โครงการ)</option>
                                    <option selected="selected">2557 (1659 โครงการ)</option>
                                    <option>2556 (11 โครงการ)</option>
                                    <option>2555 (1 โครงการ)</option>
                                <option>ดูทั้งหมด</option>
                                </select></div>
                             </div><br />
                                  <div class="form-group"><br />
                 <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                   <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">สำนักงานทางหลวงชนบทที่ 1 (ปทุมธานี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 2 (สระบุรี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 3 (ชลบุรี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 4 (เพชรบุรี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 5 (นครราชสีมา)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 6 (ขอนแก่น)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 7 (อุบลราชธานี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 8 (นครสวรรค์)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 9 (อุตรดิตถ์)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 10 (เชียงใหม่)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 11 (สุราษฏร์ธานี)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 12 (สงขลา)</option>
                      <option>สำนักงานทางหลวงชนบทที่ 13 (ฉะเชิงเทรา)</option>
                      <option>ประเภท อื่นๆ</option>
                    </select></div><br />
                  </div><!-- /.form-group -->
                                 
                              </div><!-- /.box-body -->
                    </form> </div>

                     <div class="col-md-6">
                      <form class="form-horizontal">
                        <div class="box-body">
                       <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">กระบวนงาน :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">วางแผน</option>
                      <option>สำรวจออกแบบ</option>
                      <option>เสนอแผนงปม.ให้กรมพิจารณา</option>
                      <option>เวนคืนอสังหาริมทรัพย์</option>
                      <option>จัดจ้างก่อสร้าง</option>
                      <option>การก่อสร้าง</option>
                      <option>คืนค้ำประกันสัญญา</option>
                    </select></div>
                  </div><!-- /.form-group -->
                                  
                                  <div class="form-group">
                   <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">แขวงทางหลวงชนบทนนทบุรี</option>
                      <option>แขวงทางหลวงชนบทปทุมธานี</option>
                      <option>แขวงทางหลวงชนบทพระนครศรีอยุธยา  </option>
                      <option>แขวงทางหลวงชนบทสมุทรปราการ</option>
                      <option>แขวงทางหลวงชนบทอ่างทอง</option>
                      <option>แขวงทางหลวงชนบทชัยนาท</option>
                      <option>แขวงทางหลวงชนบทลพบุรี</option>
                      <option>แขวงทางหลวงชนบทสระบุรี</option>
                      <option>แขวงทางหลวงชนบทสิงห์บุรี</option>
                      <option>แขวงทางหลวงชนบทจันทบุรี</option>
                      <option>แขวงทางหลวงชนบทชลบุรี</option>
                      <option>แขวงทางหลวงชนบทตราด</option>
                      <option>แขวงทางหลวงชนบทระยอง</option>
                      <option>ประเภท อื่นๆ</option>
                    </select></div>
                  </div><!-- /.form-group --> 
                                 
                               
                                 </div>
                         </form> </div>

                      </div></div>
                              <div class="box-body">



                               <div class="box-body no-padding"style="overflow-x:auto;">
                                <table class="table table-bordered">
                               
    	                        <tr class="bg-blue-gradient">
    		                    <th class="actions" style=" text-align :center ;"><b>หัวข้อ</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>กรอกข้อมูล</b></th>
                                </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ชื่อการดำเนินงาน :</b></td>
    		                    <td width="80%"><div class="col-sm-12">
                                    <div class="input-group" style="width: 100%;">
                                    <%--<input type="text" name="table_search" class="form-control input-sm" placeholder="">--%>
                                    <input type="text1" class="form-control" id="inputnNameProject" placeholder="ข้อความ">
                                </div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการจริง :</b></td>
    		                    <td width="80%"><div class="col-sm-5">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text3" class="form-control" id="inputnDate" placeholder="">
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
    		                    <td width="80%"> <div class="col-sm-12">
                                <textarea class="form-control" rows="7" placeholder="ข้อความ"></textarea>
                                <span class="input-group-btn">
                                </span></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
    		                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text5" class="form-control" id="inputnSuccess" placeholder="ข้อความ"></div></div></td>
    		                    </tr>

                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
    		                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                   <input type="text6" class="form-control" id="inputnOther" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                                   <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;"
                                 <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                                 <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                                 <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 5 MB (jpg,pdf,png)</i>
                                 <div class="col-sm-12">
                                     <div class="input-group" style="width: 100%;"></div>
                                 </div></td> 
    		                    </tr>
                            </table> <br />
                           <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
                        </div> </div>

                
         </div></div>
            </div>
              </div></div>
    </section>
   </div>

</asp:Content>
