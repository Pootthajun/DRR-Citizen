﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_t1_detail.aspx.cs" Inherits="DRR_Citizen.frm_question_t1_detail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
      
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">
          <div class="col-md-12">  

             <div class="box box-solid box-primary">
                    <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>สถานะแบบสอบถาม  ท1 (รวม 25 )   (CPM_UT0204)</b></h4></div>
 
                 <div class="box-body no-padding">
                   <table class="table table-bordered tab-content table-hover">
                      
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">สีแบบสอบถาม</th>
                      <th class="actions" style=" text-align :center ;">ชื่อ</th>
                      <th class="actions" style=" text-align :center ;">นามสกุล</th>
                      <th class="actions" style=" text-align :center ;">เบอร์โทร</th>
                      <th class="actions" style=" text-align :center ;">วันที่ดำเนินการ</th>
                      <th class="actions" style=" text-align :center ;">วันที่บันทึกข้อมูล</th>
                      <th class="actions" style=" text-align :center ;">สถานะแบบสอบถาม</th> 
                      <th class="actions" style=" text-align :center ;">สถานะดำเนินการ</th> 
                      <th class="actions" style=" text-align :center ;">วันที่แก้ไข</th>
                      <th class="actions" style=" text-align :center ;">ข้อที่แก้ไข</th>
                      <th class="actions" style=" text-align :center ;">วิธีการแก้ไข</th>
                      
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#shortModal1" data-toggle="modal"><small>ดาโหด </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users3.aspx"><small>เส็นทุ่ง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>เอกชัย </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ลิ้มเจริญอนันต์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>065-4353874</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>วิเชียร </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>เจ๊ะแอ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>054-4353094</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>แก้ไขแล้ว</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>11/12/2014</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 4</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>จงรักษ์ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กำสัมฤทธิ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>080-4353200</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ยังไม่แก้ไข</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 6</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ออลาระ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>พัฒชา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>084-4357634</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ยังไม่แก้ไข</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ข้อ 7</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>มนตรีย </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>หมานละงู</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>สาธิต </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ลารีน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>089-4353654</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>
                    
                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ยูฮัน </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>บูเทศ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-5463788</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>อามีน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กอลาบันหลง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>086-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>จงรักษ์ </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กำสัมฤทธิ์</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>075-4365234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ออลาระ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>พัฒชา</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353434</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>มนตรีย </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>หมานละงู</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>085-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>สาธิต </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ลารีน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>089-4353654</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>
                    
                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>ยูฮัน </small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>บูเทศ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>099-5487788</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t1.aspx"><span class="label label-danger">สีแดง</span></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>อามีน</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>กอลาบันหลง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_users2.aspx"><small>12/20/2556</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>086-4353234</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>10/21/2014</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ไม่มีข้อแก้ไข</small></a> </td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                     </tr>
                  </table>
                </div><!-- /.box-body -->
               
                <div class="box-footer clearfix">
                  <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนกลับ</a></li>
                  </ul>
                </div>
             
           
                           <%-- <div class="panel panel-default">
                              
                                <div class="bootstrap-admin-panel-content">
                               <div id="shortModal1" class="modal modal-wide fade">
                                 <div class="modal-dialog">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                         <h4 id="myModalLabel" class="modal-title">ข้อมูลผู้ทำแผ่นพับ</h4>
                                        </div>
                                     <div class="box box-solid box-primary">
                           
                       <table class="table">
    	                       
                    <tr>
                        <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-5 control-label">เลขที่บัตรประชาชน :</label>
                         <div class="col-sm-7">
                         <asp:TextBox ID="TextBox14" runat="server" class="form-control" placeholder="3-1304-00102-91-2" ReadOnly ="true"></asp:TextBox>
                          </div>
                        </div><!-- /.form-group --> </td>
 
                        
                       <td class="actions" style=" text-align :left ;"><div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">คำนำหน้า: </label>
                       <div class="col-sm-8">
                       <asp:TextBox ID="TextBox1" runat="server" class="form-control" placeholder="นาย" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td> 
    		          </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-5 control-label ">ชื่อ :</label>
                     <div class="col-sm-7">
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control" placeholder="ธีรเจต" ReadOnly ="true"></asp:TextBox></div>
                     </div><!-- /.form-group --></td>
  
                    <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                    <div class="col-sm-8">
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control" placeholder="ภู่ระหงษ์" ReadOnly ="true"></asp:TextBox></div>
                  </div><!-- /.form-group --> </td>
  
                  </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-5 control-label">บ้านเลขที่ :</label>
                         <div class="col-sm-7">
                         <asp:TextBox ID="TextBox4" runat="server" class="form-control" placeholder="122" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">หมู่ที่ :</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox5" runat="server" class="form-control" placeholder="3" ReadOnly ="true"></asp:TextBox></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-5 control-label ">ตำบล :</label>
                        <div class="col-sm-7">
                        <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="บึงชำอ้อ" ReadOnly ="true"></asp:TextBox></div>
                        </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">จังหวัด :</label>
                         <div class="col-sm-8">
                         <asp:TextBox ID="TextBox7" runat="server" class="form-control" placeholder="ปทุมธานี" ReadOnly ="true"></asp:TextBox>
                         </div>
                       </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-5 control-label">อาชีพ :</label>
                        <div class="col-sm-7">
                        <asp:TextBox ID="TextBox8" runat="server" class="form-control" placeholder="เกษตรกร" ReadOnly ="true"></asp:TextBox>
                    </div>
                  </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">การศึกษา :</label>
                        <div class="col-sm-8">
                            <asp:TextBox ID="TextBox9" runat="server" class="form-control" placeholder="มัธยมศึกษา" ReadOnly ="true"></asp:TextBox>
                            </div>
                          </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                   
                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                    <label for="inputEmail3" class="col-sm-5 control-label"> เบอร์โทร :</label>
                      <div class="col-sm-7">
                          <asp:TextBox ID="TextBox11" runat="server" class="form-control" placeholder="0-2905-9522" ReadOnly ="true"></asp:TextBox>
                    </div>
                    </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :center ;"></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"></td>
                     <td class="actions" style=" text-align :left ;"></td> </tr>
                  </table>
                   
                        </div><!-- /.box-body -->
                    
               
                 
                 <b class="text-info">ประวัติการเข้าร่วมกิจกรรม</b><br />
                  <div class="box box-solid box-primary">
                   <div class="row">
                         <form class="form-horizontal">
                    <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label text-right">โครงการ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ค้นหาโครงการ</option>
                      <option>ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)</option>
                      <option>ถนนสายแยก ทช.สต.5041 - บ.ศาลเจ้า</option>
                      <option>ถนนสายแยก ทล.4051 - บ.หาดทรายยาว</option>
                      <option>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</option>
                      <option>อื่น</option>
                    </select></div>
                    </div>
                
                  </div><!-- /.box-body -->
                  
                </form><br />

                </div>
                    <div class="box-body no-padding">
                      <table class="table table-striped table-bordered">
                      <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ลำดับ</th>
                      <th class="actions" style=" text-align :center ;">การเข้าร่วมกิจกรรม</th>
                      <th class="actions" style=" text-align :center ;">ประเภทกิจกรรม</th>
                      <th class="actions" style=" text-align :center ;">วันที่-เวลา </th>
                      <th class="actions" style=" text-align :center ;">ชื่อกิจกรรม</th>
                      <th class="aactions" style=" text-align :center ;">ชื่อโครงการ</th>
                      <th class="actions" style=" text-align :center ;">ชื่อสายทาง</th>
                      <th class="actions" style=" text-align :center ;">ผลความคิดเห็น</th>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;">1</td>
                      <td class="actions" style=" text-align :center ;"><small>วางแผน</small></td>
                      <td class="actions" style=" text-align :center ;"><small>จัดประชุม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>10/01/2557 13.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมวางแผนโครงการ</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>
                     
                     <tr>
                      <td class="actions" style=" text-align :center ;">2</td>
                      <td class="actions" style=" text-align :center ;"><small>เสนอแผนงปม.ให้กรมพิจารณา</small></td>
                      <td class="actions" style=" text-align :center ;"><small>จัดประชุม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>12/07/2557 08.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมจัดเสนอแผนโครงการ</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทช.สต.5041 - บ.ศาลเจ้า</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">ลบ</span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;">3</td>
                      <td class="actions" style=" text-align :center ;"><small>เสนอแผนงปม.ให้กรมพิจารณา</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมระดมสมอง</small></td>
                      <td class="actions" style=" text-align :center ;"><small>12/08/2557 09.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมโครงการเพื่อแก้ปัญหา</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.4051 - บ.หาดทรายยาว</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;">4</td>
                      <td class="actions" style=" text-align :center ;"><small>จัดจ้างก่อสร้าง</small></td>
                      <td class="actions" style=" text-align :center ;"><small>จัดประชุม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>22/05/2556 10.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชุมวางแผน</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>

                    <tr>
                      <td class="actions" style=" text-align :center ;">5</td>
                      <td class="actions" style=" text-align :center ;"><small>การก่อสร้าง</small></td>
                      <td class="actions" style=" text-align :center ;"><small>แบบสอบถาม</small></td>
                      <td class="actions" style=" text-align :center ;"><small>2/09/2556 11.00 น</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ท 2 งานดิน</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ถนนสายแยก ทล.416 - บ.ปาล์ม 4 (ตอนที่ 3)</small></td>
                      <td class="actions" style=" text-align :center ;"><small>-</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">บวก</span></td>
                    </tr>
                  </table>
                 </div>
                    </div>
              </div>
                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
 --%>
                                </div>
                            </div>
                       

            </div>
           </div>
     
    </section>
   </div>
</asp:Content>
