﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_add_techniqce.aspx.cs"  Inherits="DRR_Citizen.frm_add_techniqce" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div class="content-wrapper" style="min-height: 1096px;">
      <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
            <div class="box box-solid box-primay">
     
            <div class="box-body">
             <div class="box-header with-border"> <h4 class="pull-left"><b>จัดการข้อมูลแผ่นพับ (CPM_UT0208)</b></h4></div>
                          
             <div class="box-body">
                 <div class="row">

                        <div class="col-md-7">
                    
                        <div class="box-body">

                            <div class="form-group">
                    <label for="inputEmail3" class="col-sm-3 control-label">แผ่นพับ :</label>
                        <div class="col-sm-9">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ท 1 ประชาชนมีส่วนร่วม</option>
                    </select></div>
                  </div><!-- /.form-group -->
                  
                    
                      </div></div>

                        <div class="col-md-5">
                    
                        <div class="box-body">

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-3 control-label text-left">version :</label>
                      <div class="col-sm-9">
                        <input type="password" class="form-control text-left" id="inputPassword" placeholder="v1.1">
                      </div>
                    </div>
                    
                      </div></div>
                
                </div>
            </div>
                <div class="box-body">
                <div class="box-header with-border">
                        <a data-toggle="modal" href="#shortModal3" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มคำถาม</i></a>
                        <a href="#" class="btn bg-olive pull-right" style="margin-right: 20px;"><i class="fa fa-load"> Update เวอร์ชันแผ่นพับ</i></a></div>
                 <div class="box-body no-padding">
                        <table class="table table-bordered tab-content table-hover">
    	                <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ข้อที่</th>
                      <th class="actions" style=" text-align :center ;">คำถาม</th>
                      <th class="actions" style=" text-align :center ;">จำนวนคำตอบ</th>
                      <TH COLSPAN="2" class="actions" style=" text-align :center ;">ดำเนินการ</th> 
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a  data-toggle="modal" href="#shortModal4"><small>1</small></a></td>
                      <td><a  data-toggle="modal" href="#shortModal4"><small>มีการจัดประชุมชี้แจงรายละเอียดโครงการต่อประชาชนหรือไม่</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>4</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a  data-toggle="modal" href="#shortModal4"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                       </td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>2</small></a></td>
                      <td><a href="#"><small>การชี้แจงมีการใช้แผ่นโปรเตอร์ ขั้นตอนการก่อสร้างอธิบายทำความเข้าใจต่อประชาชนหรือไม่</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>3</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                       </td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>3</small></a></td>
                      <td><a href="#"><small>มีการแนะนำวิธีการตรวจสอบและขั้นตอนการใช้แผ่นพับประชาชนร่วมตรวจสอบขั้นตอนการก่อสร้างหรือไม่</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                       </td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>4</small></a></td>
                      <td><a href="#"><small>มีการนำเครื่องมือที่ใช้ในการตรวจสอบและทดสอบงานทางมาแสดงหรือไม่</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>3</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                      </td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>5</small></a></td>
                      <td><a href="#"><small>มีการอธิบายข้อตกลง 3 ฝ่ายหรือไม่</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                       </td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>6</small></a></td>
                      <td><a href="#"><small>มีการลงนามในการทำข้อตกลง 3 ฝ่ายหรือไม่มี</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                       </td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>7</small></a></td>
                      <td><a href="#"><small>มีความพึงพอใจและเข้าใจในคำอธิบายและยินดีเข้าร่วมตรวจสอบขั้นตอนการก่อสร้างทางเพื่อชุมชนหรือไม่</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_timeline_1.aspx"><small>2</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_techniqce.aspx"><span class="label label-danger"><i class="fa fa-trash"></i></span></a>
                      </td>
                    </tr>
                        

                  </table>
                </div><!-- /.box-body -->
              </div>

<!--------------------------------เพิ่มคำถาม-------------------------------------------------->

                    <div id="shortModal3" class="modal modal-wide fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">เพิ่มคำถาม</h4> </div>
                    
                            <div class="modal-body">
                            <div class="box-body">

                        <div class="box box-solid box-info">
                              <div class="box-body">
                                 
                 <div class="box-body no-padding">
                      <table class="table table-bordered table-condensed">
                   
     <!-----------------------------------------ข้อ---------------------------------------->                 

    	              <tr>
                       <td class="actions" style=" text-align :center ;">ข้อที่</td>
                       <td class="actions" style=" text-align :center ;">
                           <div class="col-sm-12">
                               <div class="input-group" style="width: 100%;"><input type="password" class="form-control pull-right " id="inputchoice" placeholder="8">
                          </div></div></td>
                      <td COLSPAN="4" class="actions" style=" text-align :center ;"></td>
                    </tr>
                 
         <!-----------------------------------------คำถาม---------------------------------------->             
                      <tr>
                       <td class="actions" style=" text-align :center ;">คำถาม</td>
                       <td class="actions" style=" text-align :center ;"> 
                           <div class="col-sm-12">
                            <textarea class="form-control" rows="2" placeholder="รายละเอียดคำถาม"></textarea>
                           </div></td>
                      <td width="15%" class="actions" style=" text-align :center ;">
                          <div class="form-group"><input type="text" class="form-control" placeholder="ข้อความ"></div></td>

                      <td width="15%" class="actions" style=" text-align :center ;"><div class="form-group">
                       <div class="form-group">
                        <select class="form-control" >
                        <option>ข้อความ</option>
                        <option selected="selected">ตัวเลข</option>
                        <option>วันที่ </option>
                        </select>
                       </div></div></td>
                        <td class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-plus"></i></span></td>
                        <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                     </tr>
       <!-----------------------------------------รูปภาพ----------------------------------------> 

                        <tr>
                      <td class="actions" style=" text-align :center ;">รูปภาพ</td>
                      <td COLSPAN="3">
                          <div class="form-group"> <label for="inputExperience" class="col-sm-2 control-label"></label>
                         <div class="col-sm-12">
                        <div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment">
                         </div>
                     <div class="timeline-body"><br />
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                    </div></div></div></td>
                    <td COLSPAN="4" class="actions" style=" text-align :center ;"></td>
                    </tr>
                     </table><br />
                

                  <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มคำตอบ</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;"><small>คำตอบที่</small></th>
                      <th class="actions" style=" text-align :center ;"><small>รายละเอียด</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ถูกต้อง</small></th>
                     <th class="actions" style=" text-align :center ;"><small>ต้องแก้ไข</small></th>
                       <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เพิ่ม</small></th>
                       <TH COLSPAN="2" class="actions" style=" text-align :center ;"><small>เพิ่มเติม</small></th>
                       <th COLSPAN="1" class="actions" style=" text-align :center ;"><small>ลบ</small></th>
                    </tr>
                   <tr>
                     <td class="actions" style=" text-align :center ;"><b>1</b></td>
                     <td class="actions" style=" text-align :center ;"><div class="form-group">
                           <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="มี"></asp:TextBox></div></td>

    		         <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked" > </label></td>
                     <td class="actions" style=" text-align :center ;"><label><input type="checkbox" > </label></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-plus"></i></span></td>
    		         <td class="actions" style=" text-align :center ;"><div class="form-group">
                           <input type="text" class="form-control" placeholder="กิโลเมตรที่"></div></td>

                  <td class="actions" style=" text-align :center ;"><div class="form-group">
                    <div class="form-group">
                      <select class="form-control" >
                        <option>ข้อความ</option>
                        <option selected="selected">ตัวเลข</option>
                        <option>วันที่ </option>
                      </select>
                    </div></div></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"></td>
                      <td class="actions" style=" text-align :center ;"></td>
                      <td class="actions" style=" text-align :center ;"></td>  
                      <td class="actions" style=" text-align :center ;"></td> 
                      <td class="actions" style=" text-align :center ;"></td>
    		        
    		         <td class="actions" style=" text-align :center ;"><div class="form-group">
                            <input type="text" class="form-control" placeholder="จำนวนคนที่เข้าร่วม"></div></td>
                      <td class="actions" style=" text-align :center ;"><div class="form-group">
                           <div class="form-group">
                      <select class="form-control">
                        <option>ข้อความ</option>
                        <option selected="selected">ตัวเลข</option>
                        <option>วันที่ </option>
                      </select>
                    </div></div></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

 <!---------------------------------------------------2--------------------------------------------------->                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><b>2</b> </td>
                     <td class="actions" style=" text-align :center ;"><div class="form-group">
                           <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="ไม่มี"></asp:TextBox></div></td>
                        
                       <td class="actions" style=" text-align :center ;"><label><input type="checkbox" > </label></td>
                     <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> </label></td>

                      <td class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-plus"></i></span></td>
    		         <td class="actions" style=" text-align :center ;"><div class="form-group">
                           <input type="text" class="form-control" placeholder="วันที่ตรวจ"></div></td>

                      <td class="actions" style=" text-align :center ;"><div class="form-group">
                             <div class="form-group">
                      <select class="form-control" >
                        <option >ข้อความ</option>
                        <option>ตัวเลข</option>
                        <option selected="selected">วันที่ </option>
                      </select>
                    </div></div></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                 
                  </table>
               <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  
                </div><!-- /.box-body -->
                           

                     </div><!-- /.box-body --></div></div>
                  
               
               </div></div>
                    
                </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->

<!--------------------------------แก้ไขคำถาม-------------------------------------------------->

                    <div id="shortModal4" class="modal modal-wide fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">แก้ไขคำถาม</h4> </div>
                    
                            <div class="modal-body">
                            <div class="box-body">

                        <div class="box box-solid box-info">
                              <div class="box-body">
                                 
                 <div class="box-body no-padding">
                      <table class="table table-bordered table-condensed">
                   
     <!-----------------------------------------ข้อ---------------------------------------->                 

    	              <tr>
                       <td class="actions" style=" text-align :center ;">ข้อที่</td>
                       <td class="actions" style=" text-align :center ;">
                           <div class="col-sm-12">
                               <div class="input-group" style="width: 100%;">
                                    <asp:TextBox ID="TextBox14" runat="server" class="form-control" placeholder="1" ReadOnly ="true"></asp:TextBox>
                          </div></div></td>
                      <td COLSPAN="4" class="actions" style=" text-align :center ;"></td>
                    </tr>
                 
         <!-----------------------------------------คำถาม---------------------------------------->             
                      <tr>
                       <td class="actions" style=" text-align :center ;">คำถาม</td>
                       <td class="actions" style=" text-align :center ;"> 
                           <div class="col-sm-12">
                            <textarea class="form-control" rows="2" placeholder="มีการจัดประชุมชี้แจงรายละเอียดโครงการต่อประชาชนหรือไม่" disabled></textarea>
                           </div></td>
                      <td width="15%" class="actions" style=" text-align :center ;">
                          <div class="form-group"><input type="text" class="form-control" placeholder="-"></div></td>

                      <td width="15%" class="actions" style=" text-align :center ;"><div class="form-group">
                       <div class="form-group">
                        <select class="form-control" >
                        <option selected="selected" disabled>ไม่มี</option>
                        <option>ข้อความ</option>
                        <option>ตัวเลข</option>
                        <option>วันที่ </option>
                        </select>
                       </div></div></td>
                        <td class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-plus"></i></span></td>
                        <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                     </tr>
       <!-----------------------------------------รูปภาพ----------------------------------------> 

                        <tr>
                      <td class="actions" style=" text-align :center ;">รูปภาพ</td>
                      <td COLSPAN="3">
                          <div class="form-group"> <label for="inputExperience" class="col-sm-2 control-label"></label>
                         <div class="col-sm-12">
                        <div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment">
                         </div>
                     <div class="timeline-body"><br />
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                     <img src="http://placehold.it/150x100" alt="..." class="margin"><span class="label label-danger"><i class="fa fa-trash"></i></span>
                    </div></div></div></td>
                    <td COLSPAN="4" class="actions" style=" text-align :center ;"></td>
                    </tr>
                     </table><br />
                

                  <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มคำตอบ</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">คำตอบที่</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                      <th class="actions" style=" text-align :center ;">ถูกต้อง</th>
                     <th class="actions" style=" text-align :center ;">ต้องแก้ไข</th>
                       <TH COLSPAN="1" class="actions" style=" text-align :center ;"><small>เพิ่ม</small></th>
                       <TH COLSPAN="2" class="actions" style=" text-align :center ;"><small>เพิ่มเติม</small></th>
                       <th COLSPAN="1" class="actions" style=" text-align :center ;"><small>ลบ</small></th>
                    </tr>
                   <tr>
                     <td class="actions" style=" text-align :center ;"><b>1</b></td>
                     <td class="actions" style=" text-align :center ;"><div class="form-group">

                         <asp:TextBox ID="TextBox5" runat="server" class="form-control" placeholder="มี" ReadOnly ="true"></asp:TextBox>

    		         <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked" > </label></td>
                     <td class="actions" style=" text-align :center ;"><label><input type="checkbox" > </label></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-plus"></i></span></td>
    		         <td class="actions" style=" text-align :center ;"><div class="form-group">
                          
                       <asp:TextBox ID="TextBox3" runat="server" class="form-control" placeholder="วันที่" ReadOnly ="true"></asp:TextBox>
                  <td class="actions" style=" text-align :center ;"><div class="form-group">
                    <div class="form-group">
                      <select class="form-control" disabled >
                        <option>ข้อความ</option>
                        <option>ตัวเลข</option>
                        <option selected="selected">วันที่ </option>
                      </select>
                    </div></div></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"></td>
                      <td class="actions" style=" text-align :center ;"></td>
                      <td class="actions" style=" text-align :center ;"></td>  
                      <td class="actions" style=" text-align :center ;"></td> 
                      <td class="actions" style=" text-align :center ;"></td>
    		        
    		         <td class="actions" style=" text-align :center ;"><div class="form-group">
                            <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="จำนวนคนเข้าร่วม" ReadOnly ="true"></asp:TextBox>
                      <td class="actions" style=" text-align :center ;"><div class="form-group">
                           <div class="form-group">
                      <select class="form-control" disabled>
                        <option>ข้อความ</option>
                        <option selected="selected">ตัวเลข</option>
                        <option>วันที่ </option>
                      </select>
                    </div></div></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

 <!---------------------------------------------------2--------------------------------------------------->                  
                    <tr>
                    <td class="actions" style=" text-align :center ;"><b>2</b> </td>
                     <td class="actions" style=" text-align :center ;"><div class="form-group">
                        <asp:TextBox ID="TextBox7" runat="server" class="form-control" placeholder="ไม่มี" ReadOnly ="true"></asp:TextBox></div></td>
                       <td class="actions" style=" text-align :center ;"><label><input type="checkbox" > </label></td>
                     <td class="actions" style=" text-align :center ;"><label><input type="checkbox" checked="checked"> </label></td>

                      <td class="actions" style=" text-align :center ;"><span class="label label-success"><i class="fa fa-plus"></i></span></td>
    		         <td class="actions" style=" text-align :center ;"><div class="form-group">
                        <asp:TextBox ID="TextBox4" runat="server" class="form-control" placeholder="ความคิดเห็น" ReadOnly ="true"></asp:TextBox></div></td>
                      <td class="actions" style=" text-align :center ;"><div class="form-group">
                             <div class="form-group">
                      <select class="form-control" >
                        <option selected="selected" disabled >ข้อความ</option>
                        <option>ตัวเลข</option>
                        <option>วันที่ </option>
                      </select>
                    </div></div></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                 
                  </table>
                     
               <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  
                </div><!-- /.box-body -->
                           

                     </div><!-- /.box-body --></div></div>
                  
               
               </div></div>
                    
                </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->


                   
                </div><!-- /.modal -->

               <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   

              </div><!-- /.box -->
            </div>
        </div>  

        </section><!-- /.content -->
      </div>

</asp:Content>
