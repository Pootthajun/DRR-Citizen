﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_add_techniqce2.aspx.cs"  Inherits="DRR_Citizen.frm_add_techniqce2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
 <div class="row">

     <div class="col-md-12">
          <div class="box box-solid box-primary">
              <div class="box-body">
                <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลเทคนิคการมีส่วนร่วม (CPM_UT0210)</b></h4></div>
                  
                   <div class="box-body">
                  
                 
                   <div class="box-body"> 
                       <div class="col-md-6">
           
                        <div class="box-body">
  
                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">รหัส :</label>
                      <div class="col-sm-8">
                        <input type="text"  class="form-control " placeholder="ตัวเลข">
                      </div><br />
                    </div>
                     
                      </div>

                       </div>
                              <div class="col-md-6">
           
                        <div class="box-body">
                           <div class="form-group">
                            <label for="inputPassword3" class="col-sm-4 control-label"> ชื่อกิจกรรมการมีส่วนร่วม :</label>
                            <div class="col-sm-8">
                            <input type="text"  class="form-control" placeholder="ข้อความ">
                          </div>
                        </div>
                     
                      </div>

                       </div>
                                <div class="col-md-6">
           
                   <div class="box-body">

                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label">ระดับการมีส่วนร่วม :</label>
                    
                       <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option>ระดับ 1</option>
                      <option>ระดับ 2</option>
                      <option>ระดับ 3</option>
                      <option>ระดับ 4</option>
                      <option>ระดับ 5</option>
                    </select></div>
                    </div>
                           
                     
                      </div>

                       </div>
                     
                               
                         </div>  
                       
                   
                 <div class="box-body">
             <div class="box-body">
                 <div class="row">
                    <form class="form-horizontal">
                      
                             <div class="box-body">
                       
                        <table class="table table-bordered table-condensed">
    	                  <tr class="bg-blue-gradient">
                                <th class="actions" style=" text-align :center ;"><b>เลือก</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>รายการ</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>ชื่อที่ใช้แสดง</b></th>
                                </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ชื่อการดำเนินงาน</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text" class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;วันที่วางแผนไว้</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                   <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;วันที่ดำเนินการ</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                   <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;เวลา</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text" class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;จากวันที่-ถึงวันที่</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สถานที่ดำเนินการ</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text" class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;จำนวนผู้มีส่วนร่วม/เกี่ยวข้อง</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;รายละเอียด</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ผลการดำเนินการ</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ข้อเสนอแนะ</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text" class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;แนบเอกสาร</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="แนบรูป"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;แนบรูป</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;รายชื่อผู้เข้าร่วม/ผู้มีส่วนได้ส่วนเสีย</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :center ;"><label><input type="checkbox"></label></td>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;หมายเหตุ</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 120%;">
                                    <input type="text"  class="form-control input-sm" placeholder="ข้อความ"></div></div></td>
    		                   </tr>
                                
                            </table>
                             </div>
                       
                      </div> 
            
                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button></div> </div></div>

                  </div>
              
                
       

              </div></div>
       </div></div>
      </section>
   </div>
</asp:Content>
