﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_groupuser1.aspx.cs" Inherits="DRR_Citizen.frm_config_groupuser1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
        
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">


                 <div class="box box-solid box-primary">
                    <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลกลุ่มผู้ใช้งาน (CPM_UT0213)</b></h4></div>               
                      
                      <div class="box-body">
                <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มสิทธิ์ผู้ใช้งาน</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                     <th class="actions" style=" text-align :center ;">รหัสกลุ่ม</th>
                      <th class="actions" style=" text-align :center ;">กลลุ่มผู้ใช้งาน</th>
                      <th class="actions" style=" text-align :center ;">แก้ไข</th>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;">01<small></small></td>
                      <td class="actions" style=" text-align :center ;"><small>ผู้บริหาร</small></td>
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_groupuser2.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><small>02</small></td>
                      <td class="actions" style=" text-align :center ;"><small>Admin</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><small>03</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ผส.สำนักงานภูมิภาคส่วนกลาง</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><small>04</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ผอ.แขวง</small></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><small>05</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ผอ.ผู้ตรวจสอบแทน</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><small>06</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ช่างควบคุมงาน</small></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><small>07</small></td>
                      <td class="actions" style=" text-align :center ;"><small>กลุ่มเครือข่ายที่เกี่ยวข้อง</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;"><small>08</small></td>
                      <td class="actions" style=" text-align :center ;"><small>อสทช.</small></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;"><small>09</small></td>
                      <td class="actions" style=" text-align :center ;"><small>ประชาชน</small></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                    </tr>

                  </table>
                </div><!-- /.box-body -->
               </div><!-- /.box -->
        
              </div>
           
            

          </div></div>
        </section><!-- /.content -->
      </div>
</asp:Content>
