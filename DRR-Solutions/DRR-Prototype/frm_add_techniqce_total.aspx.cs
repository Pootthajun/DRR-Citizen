﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace DRR_Citizen
{
    public partial class frm_add_techniqce_total : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Type_Peoject();
            }
        }


        public void Type_Peoject()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ValueF");
            dt.Columns.Add("Display");
            DataRow row;


            row = dt.NewRow();
            row["ValueF"] = "1";
            row["Display"] = "แผ่นป้ายประชาสัมพันธ์";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "2";
            row["Display"] = "E-mail";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "3";
            row["Display"] = "จดหมายข่าว";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "4";
            row["Display"] = "แถลงข่าว";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "5";
            row["Display"] = "วิทยุ";
            dt.Rows.Add(row);


            row = dt.NewRow();
            row["ValueF"] = "6";
            row["Display"] = "โทรทัศน์";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "7";
            row["Display"] = "หนังสือพิมพ์";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "8";
            row["Display"] = "ประกาศตามระเบียบราชการ";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "9";
            row["Display"] = "สัมภาษณ์รายบุคคล";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "10";
            row["Display"] = "สำรวจความคิดเห็น";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "11";
            row["Display"] = "สนทนากลุ่มย่อย";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "12";
            row["Display"] = "สายด่วน/สายตรง";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "13";
            row["Display"] = "พบปะอย่างไม่เป็นทางการ";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "14";
            row["Display"] = "จัดประชุม";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "15";
            row["Display"] = "สัมมนาเชิงปฏิบัติการ";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "16";
            row["Display"] = "เวทีสาธารณะ";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "17";
            row["Display"] = "คณะทำงานเพื่อแลกเปลี่ยนข้อมูล";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "18";
            row["Display"] = "ประชุมระดมสมอง";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "19";
            row["Display"] = "ประชุมกลุ่มย่อยในการวางแผน";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "20";
            row["Display"] = "ประชุมระดมความคิดเห็น";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "21";
            row["Display"] = "คณะที่ปรึกษาร่วมภาคประชาชน";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "22";
            row["Display"] = "คณะกรรมการร่วมภาคประชาชน";
            dt.Rows.Add(row);

            row = dt.NewRow();
            row["ValueF"] = "23";
            row["Display"] = "การทำประชามติ";
            dt.Rows.Add(row);

        

            ddl_type.DataTextField = "Display";
            ddl_type.DataValueField = "ValueF";
            ddl_type.DataSource = dt;
            ddl_type.DataBind();

        }

        protected void ddl_type_SelectedIndexChanged(object sender, EventArgs e)
        {            
            //table_search.Text = ddl_type.SelectedItem.Text;
            lblType.Text = ddl_type.SelectedItem.Text;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Record Inserted Successfully')", true);
        }
    }
}