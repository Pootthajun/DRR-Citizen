﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_s.aspx.cs" Inherits="DRR_Citizen.frm_question_s" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>

<div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">


         <div class="row">
          <div class="col-md-12">
            
                <div class="box box-solid box-info ">
                    
                      <div class="box-body">
             <div class="box-body">
                 <div class="row">
                         <form class="form-horizontal">
                                        <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label">ค้นหาแผ่นพับ :</label>
                      <div class="col-sm-7">
                    <select class="form-control select2" style="width: 100%;">
                      <option>ประเภท ท</option>
                      <option>ประเภท บ</option>
                       <option selected="selected">ประเภท ส</option>
                      <option>ประเภท ป</option>
                      <option>ประเภท ซ</option>
                      <option>อื่น</option>
                    </select></div>
                    </div>
                
                  
                  </div><!-- /.box-body -->
                  
                </form>

                </div></div>
                  </div></div>      
              <div class="box box-solid box-info ">  
         <div class="box-body">
               <br /><div class="row">
              <div class="col-md-1"></div>

                   <div class="col-md-10">

                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                    <tr>
                      <th class="actions bg-primary" style=" text-align :center ;">ประเภท</th>
                      <th class="actions bg-primary" style=" text-align :center ;">ชื่อแผ่นพับ</th>
                      <th class="actions bg-primary" style=" text-align :center ;">เวอร์ชั่น</th>
                       <th class="actions bg-primary" style=" text-align :center ;">วันที่ใช้</th>
                      <th class="actions bg-primary" style=" text-align :center ;">สถานะ</th>
                     </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s1.aspx">ส 1 </a></td>
                      <td><a href="frm_techniqce_9s1.aspx">ประชนมีส่วนร่วม</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">27/02/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s2.aspx">ส 2 </a></td>
                      <td><a href="frm_techniqce_9s2.aspx">งานสำรวจเพื่อการก่อสร้างและการวางผัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.1</td>
                     <td><a href="frm_techniqce_9t1.aspx">30/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger">inactive</span></td>
                    </tr>
                   
                    <tr>
                    <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s3.aspx">ส 3 </a></td>
                      <td><a href="frm_techniqce_9s3.aspx">งานฐานรากสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">06/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s4.aspx">ส 4</a></td>
                      <td><a href="frm_techniqce_9s4.aspx">งานตอม่อสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">29/09/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9t4.aspx">ท 4</a></td>
                      <td><a href="frm_techniqce_9t4.aspx">งานลูกรัง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">21/05/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s5.aspx">ส 5</a></td>
                      <td><a href="frm_techniqce_9s5.aspx">งานพื้นสะพานแบบหล่อในพื้นที่</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                      <td><a href="frm_techniqce_9t1.aspx">07/07/2015</a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s6.aspx">ส 6 </a></td>
                      <td><a href="frm_techniqce_9s6.aspx">งานสะพานแบบคอนกรีตอัดแรง</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">09/10/2014</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s7.aspx">ส 7 </a></td>
                      <td><a href="frm_techniqce_9s7.aspx">งานทางเท้าและราวสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">03/11/2015</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                   
                    <tr>
                     <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s8.aspx">ส 8</a></td>
                      <td><a href="frm_techniqce_9s8.aspx">งานพื้นถนนคอนกรีตเสริมเหล็กช่วงพาดคอสะพาน</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">15/01/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="frm_techniqce_9s9.aspx">ส 9</a></td>
                      <td><a href="frm_techniqce_9s9.aspx">งานส่วนประกอบอื่นๆ</a></td> 
                      <td class="actions" style=" text-align :center ;">V1.0</td>
                       <td><a href="frm_techniqce_9t1.aspx">31/03/2016</a></td> 
                      <td class="actions" style=" text-align :center ;"><span class="label label-success">active</span></td>
                    </tr>
                  
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->

              <div class="col-md-1"></div>

            </div><!-- /.box-body --><br />
          
        </div></div> 
            
            </div>
        </div>

    </section>
   </div>

<%--<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
        <h5>แผ่นพับประเภทการสร้างสะพาน</h5>
          <ol class="breadcrumb">
            <li><a href="Dashboard.aspx">หน้าหลัก</a></li>
             <li class="active">แผ่นพับประเภทการสร้างสะพาน</li>
          </ol>
        </section> <br />
		

<!-- Main content ----------------------------------------------------------->
        <section class="content">


         <div class="row">
          <div class="col-md-12">
            
         <div class="box-body">
               <br /><div class="row">
              <div class="col-md-2"></div>

                   <div class="col-md-8">

                       <div class="box-body">
        
          <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-striped">
                  
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->

              <div class="col-md-2"></div>

            </div><!-- /.box-body --><br />
          
        </div></div> 
            
            </div>
        </div>

  
    </section>
   </div>--%>

</asp:Content>


