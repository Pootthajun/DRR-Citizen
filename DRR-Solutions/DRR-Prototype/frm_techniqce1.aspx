﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce1.aspx.cs" Inherits="DRR_Citizen.frm_techniqce1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
       
       <section class="content" runat ="server">
         
           <div class="row">
               <div class="col-md-12">
                <div class="box box-solid box-primary">
                <div class="box-body">
                <div class="box-header with-border"><h4 class="pull-left text-blue"><b> จัดการข้อมูลแผ่นป้ายประชาสัมพันธ์ <small>โครงการ ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) </small>(CPM_UT0401)</b></h4></div>
             
               
                 <div class="box-body">
                   <div class="box-body no-padding"style="overflow-x:auto;">
                    <table class="table table-bordered">
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                     </tr>
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ชื่อการดำเนินงาน :</b></td>
                    <td width="80%"><div class="col-sm-12">
                          <div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="ห้ามใช้ยานพาหนะที่มีน้ำหนัก น้ำหนักบรรทุก หรือน้ำหนักลงเพลาเกินกว่าที่ได้กำหนด" ReadOnly ="true"></asp:TextBox>
                          </div></div></td>
                    </tr> 
                     <tr>
    		          <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		          <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="04/7/2016" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
    		         </tr>
                    
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการ :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="05/16/2016" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
                     </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ลักษณะแผ่นป้าย :</b></td>
                     <td width="80%"><div class="col-sm-12">
                          <div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="ป้ายชี้ทาง  ลักษณะขนาดและรูปร่างของป้ายห้ามรถบรรทุกดังรูป ระยะทางจากทางแยกถึงสถานที่ดังกล่าว เป็นกิโลเมตร หรือเมตร" ReadOnly ="true"></asp:TextBox>
                          </div></div></td>
                    </tr>

                    <tr>
    		        <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                      <textarea class="form-control" rows="7" placeholder="แขวงทางหลวงชนบทสตูล ได้ติดตั้งป้ายประชาสัมพันธ์ประกาศผู้อำนวยการทางหลวงชนบท เรื่อง ห้ามใช้ยานพาหนะที่มีน้ำหนัก น้ำหนักบรรทุก หรือน้ำหนักลงเพลาเกินกว่าที่ได้กำหนด หรือโดยที่ยานพาหนะนั้นอาจทำให้ทางหลวงเสียหาย เดินบนทางหลวงชนบทในเขตความรับผิดชอบของกรมทางหลวงชนบท" disabled></textarea>
                     </div></div></td></tr>
                    
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
                     <td width="70%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="ติดตั้งจำนวน 20 ป้ายเสร็จสิ้น" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
                     <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                      <!-----------------------------------------แนบเอกสาร------------------------------------>               
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>แนบเอกสาร :</b>
                         <br /> <br />
                           <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td> 
                         
                         <%--<div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment"> </div>--%>
    		        
                      <td>
                          <div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                      <div class="timeline-body"><br />
                                <div class="attachments">
							<ul><li>
									<span class="label label-info">pdf</span> <b>รายงาน.pdf</b> <i>(2.5MB)</i>
                                     &nbsp;&nbsp;&nbsp;<span class="label label-danger"> <i class="fa fa-close" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span>
									<span class="quickMenu">
										<a href="#" class="glyphicons search"><i></i></a>
										<a href="#" class="glyphicons share"><i></i></a>
										<a href="#" class="glyphicons cloud-download"><i></i></a>
									</span>
								</li>
								<li>
									<span class="label label-info">pdf</span> <b>รายชื่อ.pdf</b> <i>(5KB)</i>
                                     &nbsp;&nbsp;&nbsp;<span class="label label-danger"> <i class="fa fa-close" data-widget="delete" data-toggle="tooltip" title="ลบ"></i></span>
									<span class="quickMenu">
										<a href="#" class="glyphicons search"><i></i></a>
										<a href="#" class="glyphicons share"><i></i></a>
										<a href="#" class="glyphicons cloud-download"><i></i></a>
									</span>
								</li>
								
							</ul>		
						</div>
                    </div></div> </td> 
                    </tr>

<!-----------------------------------------แนบรูปภาพ------------------------------------> 
                     <tr>
    		        <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                            <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                      <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                      <div class="input-group" style="width: 100%;">
                      <div class="timeline-body"><br />
                            <img src="dist/img/news/t4.jpg" width="150" height="150" /> 
                          
                             <img src="dist/img/news/t2.jpg" width="150" height="150">
                      
                    </div></div>
                       </div></td> 
                    </tr>
                  </table>
                </div>
                </div><!-- /.box-body -->
        
                </div>

                    <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
              </div>

               </div>    
            </div>
          
        </section><!-- /.content -->

    </div>
      
      
</asp:Content>
