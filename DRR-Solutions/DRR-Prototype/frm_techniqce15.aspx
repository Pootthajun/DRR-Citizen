﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce15.aspx.cs" Inherits="DRR_Citizen.frm_techniqce15" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

    <div class="content-wrapper">
       
       <section class="content" runat ="server">
         
           <div class="row">
             <div class="col-md-12">
                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลพบปะอย่างไม่เป็นทางการ (CPM_UT0432)</b></h4></div>
              <div class="box-body">


              <div id="tabs bg-info">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab"><span><i class="fa fa-file-text text-green"></i>  รายละเอียดการจัดประชุม</span></a></li>
                  <li><a href="#tab_2" data-toggle="tab"><span><i class="fa fa-users text-green"></i>  ตารางวิเคราะห์ผู้มีส่วนได้ส่วนเสีย</span></a></li>
                 
                </ul>
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                      
                    <div class="box-body">
                   <div class="box-body no-padding"style="overflow-x:auto;">
                   <table class="table table-bordered">
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                     </tr>
                    <tr>
    		         <br/><td class="actions" style=" text-align :right ;"><b>ชื่อการประชุม :</b></td>
                     <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                    <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                     </tr>
                 
                      <tr>
    		          <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		          <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;">
                    <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
    		         </tr>
                    
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการ :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
                     </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>เวลา :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                     </div></div></div> </td>
                     </tr>

                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>สถานที่ดำเนินการ :</b></td>
                     <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox8" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>

                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>พิกัด ละติจูด - ลองติจูด:</b></td>
                     <td><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox7" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                     <div class="input-group-addon"> <i class="fa fa- fa-map-marker"></i> </div></div></div> 

                         <div class="col-sm-5"> <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox9" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                     <div class="input-group-addon"><i class="fa fa- fa-map-marker"></i>
                     </div></div></div> 
                     </td>
                    </tr>

                   
                    <tr>
    		        <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
                      <td><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                      <textarea class="form-control" rows="7" placeholder="" disabled></textarea>
                     </div></div></td></tr>
                    
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
                      <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b> </td>
                      <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>

<!-----------------------------------------แนบเอกสาร------------------------------------>               
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>แนบเอกสาร :</b>
                         <br /><br />
                           <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                           <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td> 
                         
                         <%--<div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment"> </div>--%>
    		        
                      <td>
                          <div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,pdf)</i>
                      <div class="timeline-body"><br />
                        
                    </div></div> </td> 
                    </tr>

<!-----------------------------------------แนบรูปภาพ------------------------------------> 
                      <tr>
    		        <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                      <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;" <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                      <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                      <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 10 MB (jpeg,png)</i>
                      <div class="input-group" style="width: 100%;">
                      <div class="timeline-body"><br />

                      
                    </div></div>
                       </div></td> 
                    </tr>
                  </table>
                      </div>

                    </div>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane" id="tab_2">
                    
                     <div class="box-body">  
                       <a data-toggle="modal" href="#shortModal" class="btn bg-aqua pull-right" "pull-right"><i class="fa fa-plus">เพิ่มรายชื่อ</i></a></div>  
               
                   <div class="box-body no-padding">
                      <table class="table table-striped table-bordered">
                      <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;"><small>ลำดับ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ชื่อ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>นามสกุล</small></th>
                      <th class="actions" style=" text-align :center ;"><small>เบอร์โทร</small></th>
                      <th class="actions" style=" text-align :center ;"><small>หน่วยงาน/ที่ทำงาน</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ตำแหน่ง/สายงาน/อาชีพ</small></th>
                      <th class="actions" style=" text-align :center ;"><small>อยู่ในพื้นที่</small></th>
                      <th class="aactions" style=" text-align :center ;"><small>ผลกระทบ</small></th> 
                      <th class="actions" style=" text-align :center ;"><small>อิทธิพล/บทบาท</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ประเภท <br />key/primary/<br />secondary</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ความคิดเห็น <br />ไม่มี/บวก/ลบ</small></th> 
                      <th class="actions" style=" text-align :center ;"><small>เชิญ</small></th>
                      <th class="aactions" style=" text-align :center ;"><small>เข้าร่วม</small></th>
                      <th class="actions" style=" text-align :center ;"><small>แก้ไข</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ลบ</small></th>
                    <tr/>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><a href="Stakedetail.aspx"><small>1</small></a></td>
                      <td><a href="Stakedetail.aspx"><small>นายจรินทร์</small></a></td>
                      <td><a href="#"><small>จักกะพาก</small></a></td>
                      <td><a href="Timeline_1.aspx"><small>081-1634805</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small></small></a></td>
                      <td><a href="#"><small>หน่วยงานราชการ</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ก่อส้ราง</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>โดยอ้อม</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>ปานกลาง</small></a></td>
                      <td><a href="#"><small>Secondary</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small>บวก</small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small> <label> <input type="checkbox"></label></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small> <label> <input type="checkbox"></label></small></a></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-warning"><i class="fa fa-pencil"></i></span></td>
                      <td class="actions" style=" text-align :center ;"><span class="label label-danger"><i class="fa fa-trash"></i></span></td>
                    </tr>

                  </table>
                </div><!-- /.box-body --><br />
                   
                    <ul class="pagination pagination-sm no-margin pull-right">
                    <li><a href="#">ก่อนหน้า</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">ย้อนหลัง</a></li>
                  </ul> <br /><br />
                         
                             <div id="shortModal" class="modal modal-wide fade">
                 <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title text-info">เพิ่มข้อมูลผู้มีส่วนได้ส่วนเสีย</h4>
                        </div>
                        <div class="modal-body">
            
                        <div class="box-body">
                       
                       <div class="box box-solid box-primary">
     
                      <div class="box-body">
       
            
                 <b class="text-info">ข้อมูลส่วนตัว </b>
                   
                       
                       <div class="row">
                       <table class="table"><br />
    	                       
                    <tr>
                        <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">เลขที่บัตรประชาชน :</label>
                         <div class="col-sm-7">
                         <input type="text" class="form-control input-sm " placeholder=""></div>
                        </div><!-- /.form-group --> </td>

                        
                       <td class="actions" style=" text-align :left ;"><div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">คำนำหน้า :</label>
                       <div class="col-sm-8">
                       <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --></td> 
    		          </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-4 control-label ">ชื่อ :</label>
                     <div class="col-sm-7">
                     <input type="text" class="form-control input-sm " placeholder=""></div>
                     </div><!-- /.form-group --></td>
  
                    <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control input-sm " placeholder="" /></div>
                  </div><!-- /.form-group --> </td>
  
                  </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">บ้านเลขที่ :</label>
                         <div class="col-sm-7">
                         <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">หมู่ที่ :</label>
                         <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder="">
                       </div></div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">ตำบล :</label>
                        <div class="col-sm-7">
                         <input type="text" class="form-control input-sm " placeholder=""></div>
                        </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"> 
                          <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label ">อำเภอ :</label>
                         <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder="" />
                         </div>
                       </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                        <div class="col-sm-7">
                       <input type="text" class="form-control input-sm " placeholder="" />
                    </div>
                  </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"></td>
                    </tr>

                      <tr>
                     <td class="actions" style=" text-align :left ;"><div class="form-group">
                     <label for="inputEmail3" class="col-sm-4 control-label">ทีอยู่พิกัด ละติจูด :</label>
                        <div class="col-sm-7">
                       <input type="text" class="form-control input-sm " placeholder="" />
                    </div>
                  </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">ลองติจูด :</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder="" />
                         </div> </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label"> อาชีพ :</label>
                      <div class="col-sm-7">
                         <input type="text" class="form-control input-sm " placeholder="" />
                    </div>
                    </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">การศึกษา :</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder="" />
                         </div> </div><!-- /.form-group --></td>
                    </tr>
                    
                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label"> เบอร์โทรศัพท์ :</label>
                      <div class="col-sm-7">
                         <input type="text" class="form-control input-sm " placeholder="" />
                    </div>
                    </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"></td>
                     <td class="actions" style=" text-align :left ;"></td> </tr>
                  </table>
                  </div>
                    


                <b class="text-info">ข้อมูลวิเคราะห์ผู้มีส่วนได้ส่วนเสีย สำหรับโครงการ ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)<br /> </b>
                 
                   <div class="box-body no-padding"style="overflow-x:auto;">
                      <table class="table table-striped table-bordered">
                      <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ตำแหน่ง/สายงาน/อาชีพ</th>
                      <th class="actions" style=" text-align :center ;">อยู่ในพื้นที่</th>
                      <th class="actions" style=" text-align :center ;">ผลกระทบ</th>
                      <th class="actions" style=" text-align :center ;">อิทธิพล/บทบาท</th>
                      <th class="actions" style=" text-align :center ;">Stackhoder</th>
                      <th class="actions" style=" text-align :center ;">ความคิดเห็น</th>
                      <th class="actions" style=" text-align :center ;">เชิญ</th>
                      <th class="actions" style=" text-align :center ;">เข้าร่วม</th>
                    </tr>
                
                      <tr>
                      <td class="actions" style=" text-align :center ;">
                   <div class="form-group">
                        <div class="col-sm-12">
                        <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">หน่วยงานราชการ</option>
                      <option>หน่วยงานรัฐวิสาหกิจ</option>
                      <option>หน่วยงานท้องถิ่น</option>
                      <option>หน่วยงานเอกชน/ภาคเอกชน</option>
                      <option>กลุ่ม/ชุมชน</option>
                      <option>ผู้อาศัยสองข้างทาง/ใกล้เคียง</option>
                      <option>ผู้ใช้เส้นทางทั่วไป</option>
                      <option>ผู้ประกอบการ /ร้านค้า</option>
                      <option>องค์กรอิสระ /มูลนิธิ</option>
                      <option>สื่อมวลชน</option>
                      <option>ผู้นาชุมชน</option>
                      <option>สถาบันการศึกษา/นักวิชาการอิสระ</option>
                    </select></div>
                       </div><!-- /.form-group --> </td>
                      <td class="actions" style=" text-align :center ;"> <div class="form-group">
                        <div class="col-sm-12">
                            <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ก่อสร้าง</option>
                      <option>ใกล้เคียง</option>
                      <option>อื่นๆ</option>
                    </select></div>
                       </div><!-- /.form-group --></td>
                      <td class="actions" style=" text-align :center ;"> <div class="form-group">
                        <div class="col-sm-12">
                            <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">โดยตรง</option>
                      <option>ทางอ้อม</option>
                      <option>ไม่กระทบแต่สนใจ</option>
                    </select></div>
                       </div><!-- /.form-group --></td>
                      <td class="actions" style=" text-align :center ;"> <div class="form-group">
                        <div class="col-sm-12">
                            <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">มีมาก</option>
                      <option>ปานกลาง</option>
                      <option>มีน้อย</option>
                      <option>ไม่มีผล</option>
                    </select></div>
                       </div><!-- /.form-group --></td>


                      <td class="actions" style=" text-align :center ;">
                      <asp:TextBox ID="input1" runat="server" class="form-control text-left" placeholder="Key Stakeholder " ReadOnly ="true"></asp:TextBox></td>
                      <td class="actions" style=" text-align :center ;"> <div class="form-group">
                        <div class="col-sm-12">
                            <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">บวก</option>
                      <option>ลบ</option>
                    </select></div>
                       </div><!-- /.form-group --></td>
   
                      <td class="actions" style=" text-align :center ;"><a href="#"><small> <label> <input type="checkbox"></label></small></a></td>
                      <td class="actions" style=" text-align :center ;"><a href="#"><small> <label> <input type="checkbox"></label></small></a></td>
                    </tr>

                  </table><br />
                       </div>
  


               </div>
               
            </div>
                    
     
            <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
            </div></div>

                      <div class="box box-solid box-primary">
                      <div class="box-body">      
                  <h4><b>ตารางผลสรุป</b></h4>
                   <div class="box-body no-padding">
                     <table class="table table-condensed table-bordered">
                     <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">ประเภท</th>
                      <th class="actions" style=" text-align :center ;">เชิญ</th>
                      <th class="actions" style=" text-align :center ;">เข้าร่วม</th>
                      <th class="actions" style=" text-align :center ;">ไม่มี</th> 
                      <th class="actions" style=" text-align :center ;">บวก</th>
                      <th class="actions" style=" text-align :center ;">ลบ</th> 
                    </tr>
                
                    <tr>
                      
                      <td class="actions" style=" text-align :center ;">key</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;">seconary</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;">primary</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      <td class="actions" style=" text-align :center ;">0</td>
                      
                    </tr>

                    
                  </table>
                </div><!-- /.box-body -->

                  </div><!-- /.tab-pane -->
                     </div>
                  </div><!-- /.tab-pane -->
                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div></div>
              </div>
             
                        </div></div><!-- /.box-body --> 

           </div></div></div>
        </section>

    </div>
  
</asp:Content>