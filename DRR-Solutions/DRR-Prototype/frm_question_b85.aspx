﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_b85.aspx.cs" Inherits="DRR_Citizen.frm_question_b85" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper" style="min-height: 1096px;">
       
        <!-- Main content -->
        <section class="content">

            <div class="row">
            <div class="col-md-12">  

                 <div class="box box-solid box-primay">
     
                    <div class="box-body">
              <div class="nav-tabs-custom"> <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลแบบสอบถาม บ8.5 (CPM_UT0604)</b></h4></div>
                  <div id="tabs bg-info"> 
                   <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><span><i class="fa fa-sticky-note text text-green"></i> กรอกข้อมูล</span></a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><span><i class="fa fa-clone text text-green"></i> แบบสอบถาม</span></a></li>
                </ul>
                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1"><br />
   <!--------------------------------------ข้อมูลโครงการ------------------------------------------------>  
                     <b class="text-primary">ข้อมูลโครงการ </b>       
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">

                         <table class="table">
    	                       
                         <tr>
                        <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">โครงการก่อสร้างถนนสาย :</label>
                              <div class="col-sm-10">
                            <asp:TextBox ID="input1" runat="server" class="form-control" placeholder="ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) " ReadOnly ="true"></asp:TextBox>
                         </div> 
                             </div><!-- /.form-group --></td>
    		             </tr>
                       </table>

                          <table class="table">
    	                       
                         <tr>
                         <td class="actions" style=" text-align :left ;">  <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">อำเภอ :</label>
                           <div class="col-sm-8">
                           <select class="form-control select2" style="width: 100%;" disabled>
                            <option selected="selected" >-</option>
                            <option>ชลบุรี</option>
                            <option>นครราชสีมา</option>
                         </select></div>
                         </div><!-- /.form-group --> </td>

                          <td class="actions" style=" text-align :left ;">   <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                             <div class="col-sm-8">
                             <select class="form-control select2" style="width: 100%;" disabled>
                             <option selected="selected" >สตูล</option>
                             <option>ชลบุรี</option>
                             <option>นครราชสีมา</option>
                            </select></div>
                           </div><!-- /.form-group --></td>
                          </tr>
                        </table>
                       
                           </div></div>
                        </div></div>
                   
  <!--------------------------------------ข้อมูลผู้ควบคุมงาน------------------------------------------------>
                     <b class="text-primary">ข้อมูลผู้ควบคุมงาน</b>                           
                

                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้ควบคุมงาน :</label>
                              <div class="col-sm-8">
                          
                               <asp:TextBox ID="TextBox29" runat="server" class="form-control" placeholder="นายเดชา พรหมมี" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                                <div class="col-sm-8">
                               <asp:TextBox ID="TextBox37" runat="server" class="form-control" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                              <asp:TextBox ID="TextBox31" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                      </div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                             <div class="col-sm-8">
                           <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="แขวงทางหลวงชนบทสตูล" ReadOnly ="true"></asp:TextBox>
                      </div>
                            </div><!-- /.form-group --></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้วิศวกรโยธา :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                  </table>
                      
                           </div></div>
                        </div></div>
                 

                     <b class="text-primary">ข้อมูลผู้มีส่วนร่วม</b>
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้มีส่วนร่วม :</label>
                               <div class="col-sm-7">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">วันที่ร่วมตรวจสอบ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">บ้านเลขที่/หมู่ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                         </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;">
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">ตำบล :</label>
                          <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                        </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                          <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">โทรศัพท์ :</label>
                        <div class="col-sm-8">
                    <input type="text"  class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> 
                          <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">ที่ทำงาน/หน่วยงาน :</label>
                        <div class="col-sm-7">
                    <input type="text" class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">ถนน/ซอย :</label>
                           <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">อำเภอ :</label>
                           <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">จังหวัด :</label>
                           <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">รหัสไปรษณีย์ :</label>
                           <div class="col-sm-7">
                           <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --> </td>
                    <td class="actions" style=" text-align :left ;"></td>
                    </tr>
                   
                  </table>
                      
                           </div></div>
                        </div></div>

                <a href="#tab_2" data-toggle="tab" aria-expanded="true"> <button class="btn btn-success pull-right">บันทึกถัดไป</button></a>
                  
                <br /><br /></div><!-- /.box-body -->
                 
                  <div class="tab-pane" id="tab_2">
                   
                    <div class="row">
       

                   <div class="col-xs-12">

                      <div class="box box-solid box-primary">
                        <div class="box-body box-profile">
                  
                            <h3 class="profile-username">แบบสอบถาม บ8.5 : งานสะพานแบบคอนกรีตอัดแรง</h3>
                           

                            <ul class="list-group list-group-unbordered">

<!-------------------------------------------------------Choice-1--------------------------------->
                                    <li class="list-group-item">

                           <h5 class="text-blue"><b>1.  พื้นสะพานแบบคานคอนกรีตอัดแรงแบบแผ่นพื้นและแบบคานคอนกรีตอัดแรงรูปกล่อง<a class="pull-right">ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                      
                           <div class="col-xs-1"></div>

                           <div class="col-xs-10">
                               <h5 class="text-blue"><b>1.1  การกองและเก็บแผ่นพื้นคานคอนกรีตอัดแรงแบบแผ่นพื้นและแบบคานคอนกรีตอัดแรงรูปกล่องที่โครงการมีการวางโดยมีหมอนรองที่ปลายคานทั้ง 2 ด้านหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>
                               
                                </table>
                                <h5 class="text-blue"><b>1.2  คานคอนกรีตอัดแรงแบบแผ่นพื้นและแบบคานคอนกรีตอัดแรงรูปกล่อง ที่ติดตั้งที่โครงการมีรอยแตกร้าวหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>
                               
                                </table>
                                <h5 class="text-blue"><b>1.3  มีการติดตั้งยางรองคานสะพานก่อนการยกติดตั้งหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td><input type="checkbox"> มี </td>
    		                      </tr>
                                 <tr class="bg-info">
                                 <td class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ขนาดกว้าง (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                   <tr class="bg-info">
                                 <td class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ขนาดยว (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                   <tr class="bg-info">
                                 <td class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ขนาดหนา (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                <tr class="bg-info">
                                 <td><input type="checkbox"> มี </td>
    		                      </tr>
                                </table>

                                <h5 class="text-blue"><b>1.4  ในการติดตั้งคานมีเหล็กเดือยร้อยที่ปลายคานฝังลงในคานตอม่ะสะพานและอุดด้วยปูนทรายในด้านหนึ่งและอุดด้วยแอสฟัลต์ที่อีกด้านหนึ่งหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มีอุดปูนหนึ่งด้าน/อุดแอสฟัลต์หนึ่งด้าน </td>
                                 <td><input type="checkbox"> ไม่มีอุด </td>
    		                      </tr>
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> อุดผิดเป็นปูนสองด้าน </td>
                                 <td><input type="checkbox"> อุดผิดเป็นแอสฟัสต์สองด้าน </td>
    		                      </tr>
                               
                                </table>
                                <h5 class="text-blue"><b>1.5  หลังจากติดตั้งคานคอนกรีตอัดแรงแบบแผ่นพื้นที่หรือแบบกล่องแล้ว มีการเชื่อมเหล็กที่บริเวณข้างคานให้ยึดติดกันอย่างแน่นหนาหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มีเชื่อมทุกจุดทุกแผ่น </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>
                               
                                </table>
                                  </div>   
                                           
                             <div class="col-xs-1"></div>
                          </div></li>
  
 <!-------------------------------------------------------Choice-2--------------------------------->                                                                 
                                    <li class="list-group-item">
                                   
                                        <h5 class="text-blue"><b>2. พื้นสะพานแบบคานคอนกรีตอัดแรงรูปตัวไอ  <a class="pull-right">ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                               <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                                <div class="col-xs-1"></div>
                             <div class="col-xs-10">
                                  <h5 class="text-blue"><b>2.1  คานคอนกรีตอัดแรงรูปตัวไอที่ติดตั้งที่โครงการฯ มีรอยแตกร้าวหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>

                                 <h5 class="text-blue"><b>2.2  มีการปรับระดับปูนรองใต้หัวคานและติดตั้งยางรองคานสะพานก่อนติดตั้งคานหรือไม่</b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>

                                  
                                  </div> 
                           <div class="col-xs-1"></div>  
                         </div></li>

<!-------------------------------------------------------Choice-3--------------------------------->
                                    <li class="list-group-item">
                                   
                     <h5 class="text-blue"><b>3. เหล็กเสริมที่พื้นสะพานมีความสะอาดไม่มีคราบดินหรือสนิมติดตั้งเหล็กกำหนดระดับพื้นสะพาน มีเหล็กค้ำเหล็กเสริมบนให้อยู่ในตำแหน่งที่ถูกต้องและมีผู้ควบคุมงานตรวจสอบระดับพื้นสะพานหรือไม่ <a class="pull-right">ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                                <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> สะอาด </td>
                                 <td><input type="checkbox"> ติดตั้งเหล็กค้ำเสริม </td>
    		                      </tr>
                                 <tr class="bg-info">
                                 <td colspan="2" class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">เหล็กเสริมบน ขนาด (มิลลิเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                  <tr class="bg-info">
                                 <td colspan="2" class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ระยะห่าง (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                  <tr class="bg-info">
                                 <td colspan="2" class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">เหล็กเสริมล่าง (มิลลิเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                   <tr class="bg-info">
                                 <td colspan="2" class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ระยะห่าง (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                </table>
                              </div>
                           <div class="col-xs-1"></div>
                         </div></li>

<!-------------------------------------------------------Choice-4--------------------------------->
                                    <li class="list-group-item">
                                    
                                        <h5 class="text-blue"><b>4. มีการติดตั้งเหล็กขอบรอบต่อพื้นสะพานและกั้นรอยต่อ ดังกล่าวด้วยกระดาษชานอ้อยหรือวัสดุอื่นที่เหมาะสมตลอดความลึกของพื้นสะพานหรือไม่ <a class="pull-right">ข้าม</a></b></h5>

                                      <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed"> 
                                <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                               <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                   <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มีติดตั้ง </td>
                                 <td colspan="2"><input type="checkbox"> ไม่มีการติดตั้ง </td>
    		                      </tr>
                                  </table>
                                 <table class="table table-bordered table-condensed">
                                <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> เหล็กขอบ </td>
                                 <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ขนาดยาว (เมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                    <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ลึก (เมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>

                                <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> กระดาษชานอ้อย </td>
                                 <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ขนาดยาว (เมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                    <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ลึก (เมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>

                                 <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> วัสดุอื่นๆ </td>
                                 <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ขนาดยาว (เมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                    <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">ลึก (เมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
    		                      </tr>
                                </table>
                              </div>
                           <div class="col-xs-1"></div>
                         </div></li>

<!-------------------------------------------------------Choice-5--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>5. มีการตรวจสอบค่าการยุบตัวและเก็บตัวอย่างคอนกรีต และมีผู้ควบคุมงานควบคุมการเทคอนกรีตหรือไม่ <a class="pull-right">ข้าม</a></b></h5>
                            <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div> 
                             <div class="col-xs-10">
                                  <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีการตรวจสอบ </td>
                                 <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">วัดได้ (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                    <td width="20%"><input type="checkbox"> มีการตรวจสอบ </td>
    		                      </tr>
                                  <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีการเก็บตัวอย่าง </td>
                                 <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">จำนวน (ลูก) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                    <td width="20%"><input type="checkbox"> ไม่มีการเก็บตัวอย่าง </td>
    		                      </tr>
                                 <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีผู้ควบคุมงานควบคุม </td>
                                 
                                    <td colspan="2" width="20%"><input type="checkbox"> มีการตรวจสอบ </td>
    		                      </tr>
                                </table>
                              
                              </div>
                           <div class="col-xs-1"></div>
                           
                         </div></li>

<!-------------------------------------------------------Choice-6--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>6. มีการบ่มคอนกรีตต่อเนื่องอย่างน้อย 7 วันหรือไม่<a class="pull-right">ข้าม</a></b></h5>
                            <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                      <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  มี </td> 
                                 <td width="50%"><input type="checkbox"> ใช้น้ำฉีด</td>
    		                      </tr>
                                <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  ใช้กระสอบบ่ม </td> 
                                 <td width="50%"><input type="checkbox"> ใช้พาสติกบ่ม</td>
    		                      </tr>
                                <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  อื่นๆ </td> 
                                 <td width="50%"><input type="checkbox"> ไม่มี</td>
    		                      </tr>
                                </table>
                                      
                                 <div class="col-xs-1"></div>
 
                                </div>
                           
                         </div></li>
<!-------------------------------------------------------Choice-7--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>7. มีการติดตั้งป้ายเตือนความปลอดภัยในสถานที่ก่อสร้างหรือไม่<a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox">  มี </td> 
                                 <td width="50%"><input type="checkbox"> ไม่มี</td>
    		                      </tr>
 
                                </table>
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li> 
                        
                                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกแล้วทำต่อ</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div> 
                      </ul></div> 

                           
                 
                  
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>

                      
                 </div>
                </div><!-- /.tab-pane -->

               </div></div>
                </div>
              
              </div><!-- nav-tabs-custom -->
            </div>
         </div>
        </section><!-- /.content -->
      </div>


</asp:Content>

