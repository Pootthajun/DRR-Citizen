﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_question_b82.aspx.cs" Inherits="DRR_Citizen.frm_question_b82" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper" style="min-height: 1096px;">
       
        <!-- Main content -->
        <section class="content">

            <div class="row">
            <div class="col-md-12">  

                 <div class="box box-solid box-primay">
     
                    <div class="box-body">
              <div class="nav-tabs-custom"> <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลแบบสอบถาม บ8.2 (CPM_UT0604)</b></h4></div>
                  <div id="tabs bg-info"> 
                   <ul class="nav nav-tabs">
                  <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><span><i class="fa fa-sticky-note text text-green"></i> กรอกข้อมูล</span></a></li>
                  <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><span><i class="fa fa-clone text text-green"></i> แบบสอบถาม</span></a></li>
                </ul>
                <div class="tab-content">

                  <div class="tab-pane active" id="tab_1"><br />
   <!--------------------------------------ข้อมูลโครงการ------------------------------------------------>  
                     <b class="text-primary">ข้อมูลโครงการ </b>       
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">

                         <table class="table">
    	                       
                         <tr>
                        <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-2 control-label">โครงการก่อสร้างถนนสาย :</label>
                              <div class="col-sm-10">
                            <asp:TextBox ID="input1" runat="server" class="form-control" placeholder="ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2) " ReadOnly ="true"></asp:TextBox>
                         </div> 
                             </div><!-- /.form-group --></td>
    		             </tr>
                       </table>

                          <table class="table">
    	                       
                         <tr>
                         <td class="actions" style=" text-align :left ;">  <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">อำเภอ :</label>
                           <div class="col-sm-8">
                           <select class="form-control select2" style="width: 100%;" disabled>
                            <option selected="selected" >-</option>
                            <option>ชลบุรี</option>
                            <option>นครราชสีมา</option>
                         </select></div>
                         </div><!-- /.form-group --> </td>

                          <td class="actions" style=" text-align :left ;">   <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                             <div class="col-sm-8">
                             <select class="form-control select2" style="width: 100%;" disabled>
                             <option selected="selected" >สตูล</option>
                             <option>ชลบุรี</option>
                             <option>นครราชสีมา</option>
                            </select></div>
                           </div><!-- /.form-group --></td>
                          </tr>
                        </table>
                       
                           </div></div>
                        </div></div>
                   
  <!--------------------------------------ข้อมูลผู้ควบคุมงาน------------------------------------------------>
                     <b class="text-primary">ข้อมูลผู้ควบคุมงาน</b>                           
                

                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้ควบคุมงาน :</label>
                              <div class="col-sm-8">
                          
                               <asp:TextBox ID="TextBox29" runat="server" class="form-control" placeholder="นายเดชา พรหมมี" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                                <div class="col-sm-8">
                               <asp:TextBox ID="TextBox37" runat="server" class="form-control" placeholder="วิศวกรโยธา" ReadOnly ="true"></asp:TextBox>
                      </div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                              <asp:TextBox ID="TextBox31" runat="server" class="form-control" placeholder="สำนักงานทางหลวงชนบทที่ 12 (สงขลา)" ReadOnly ="true"></asp:TextBox>
                      </div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                             <div class="col-sm-8">
                           <asp:TextBox ID="TextBox6" runat="server" class="form-control" placeholder="แขวงทางหลวงชนบทสตูล" ReadOnly ="true"></asp:TextBox>
                      </div>
                            </div><!-- /.form-group --></td>
                    </tr>
                    
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้วิศวกรโยธา :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">สังกัดหน่วย :</label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">หน่วยดำเนินงาน :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                  </table>
                      
                           </div></div>
                        </div></div>
                 

                     <b class="text-primary">ข้อมูลผู้มีส่วนร่วม</b>
                      <div class="box box-solid box-primary">
                        <div class="box-body">
                         <div class="box-body">
                          <div class="row">
                          <table class="table">
    	                       
                    <tr>
                      <td class="actions" style=" text-align :left ;">  <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">ชื่อผู้มีส่วนร่วม :</label>
                               <div class="col-sm-7">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td>

                        
                        <td class="actions" style=" text-align :left ;"> <div class="form-group">
                              <label for="inputEmail3" class="col-sm-4 control-label">นามสกุล :</label>
                               <div class="col-sm-8">
                               <input type="text" class="form-control input-sm " placeholder=""></div>
                             </div><!-- /.form-group --></td> 
    		        </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                            <label for="inputEmail3" class="col-sm-4 control-label">วันที่ร่วมตรวจสอบ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                             <label for="inputEmail3" class="col-sm-4 control-label">ตำแหน่ง :</label>
                              <div class="col-sm-8">
                             <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div><!-- /.form-group --></td>
                    </tr>

                    <tr>
                     <td class="actions" style=" text-align :left ;">
                          <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">บ้านเลขที่/หมู่ :</label>
                            <div class="col-sm-7">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                         </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;">
                        <div class="form-group">
                         <label for="inputEmail3" class="col-sm-4 control-label">ตำบล :</label>
                          <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                        </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4 control-label">จังหวัด :</label>
                          <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">โทรศัพท์ :</label>
                        <div class="col-sm-8">
                    <input type="text"  class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> 
                          <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">ที่ทำงาน/หน่วยงาน :</label>
                        <div class="col-sm-7">
                    <input type="text" class="form-control input-sm " placeholder=""></div>
                  </div><!-- /.form-group --></td>

                      <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">ถนน/ซอย :</label>
                           <div class="col-sm-8">
                            <input type="text" class="form-control input-sm " placeholder=""></div>
                            </div></td>
                    </tr>

                     <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">อำเภอ :</label>
                           <div class="col-sm-7">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --> </td>

                      <td class="actions" style=" text-align :left ;"><div class="form-group">
                          <label for="inputEmail3" class="col-sm-4  control-label">จังหวัด :</label>
                           <div class="col-sm-8">
                          <input type="text" class="form-control input-sm " placeholder=""></div>
                       </div><!-- /.form-group --></td>
                    </tr>

                   <tr>
                     <td class="actions" style=" text-align :left ;"> <div class="form-group">
                           <label for="inputEmail3" class="col-sm-4 control-label">รหัสไปรษณีย์ :</label>
                           <div class="col-sm-7">
                           <input type="text" class="form-control input-sm " placeholder=""></div>
                          </div><!-- /.form-group --> </td>
                    <td class="actions" style=" text-align :left ;"></td>
                    </tr>
                   
                  </table>
                      
                           </div></div>
                        </div></div>

                <a href="#tab_2" data-toggle="tab" aria-expanded="true"> <button class="btn btn-success pull-right">บันทึกถัดไป</button></a>
                  
                <br /><br /></div><!-- /.box-body -->
                 
                  <div class="tab-pane" id="tab_2">
                   
                    <div class="row">
       

                   <div class="col-xs-12">

                      <div class="box box-solid box-primary">
                        <div class="box-body box-profile">
                  
                            <h3 class="profile-username">แบบสอบถาม บ8.2 : งานฐานรากสะพาน</h3>
                           

                            <ul class="list-group list-group-unbordered">

<!-------------------------------------------------------Choice-1--------------------------------->
                                    <li class="list-group-item">

                           <h5 class="text-blue"><b>1.  ฐานรากเสาเข็มตอก<a class="pull-right">ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                      
                           <div class="col-xs-1"></div>

                           <div class="col-xs-10">
                               <h5 class="text-blue"><b>1.1  ผู้รับจ้างกองเก็บเสาเข็มโดยใช้หมอนรองใต้เสาเข็มตรงตำแหน่งหูยกเสาเข็ม และตัวเสาเข็มไม่มีรอยแตกร้าวใช่หรือไม่ </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มีการรองหมอน </td>
                                 <td><input type="checkbox"> ไม่มีการรองหมอน </td>
    		                      </tr>
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> ไม่มีรอยแตกร้าว </td>
                                 <td><input type="checkbox"> เสาเข็มมีรอยแตกร้าว </td>
    		                      </tr>
                              
                                </table>
                               <h5 class="text-blue"><b>1.2  มีผู้ควบคุมงานตรวจสอบการตอกเสาเข็มและจดบันทึกการตอกเสาเข็มหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>
                               
                                </table>
                               <h5 class="text-blue"><b>1.3  ผู้รับจ้างใช้เครื่องมือในการตัดหัวเสาเข็มโดยรอบให้ได้แนวตรงก่อนสกัดหัวเสาเข็มหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> ใช้ </td>
                                 <td><input type="checkbox"> ไม่ใช้ </td>
    		                      </tr>
                               
                                </table>

                                  </div>   
                                           
                             <div class="col-xs-1"></div>
                          </div></li>
  
 <!-------------------------------------------------------Choice-2--------------------------------->                                                                 
                                    <li class="list-group-item">
                                   
                                        <h5 class="text-blue"><b>2. ฐานรากเสาเข็มเจาะ <a class="pull-right">ข้าม</a></b></h5>

                                       <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                               <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                                <div class="col-xs-1"></div>
                             <div class="col-xs-10">
                                <h5 class="text-blue"><b>2.1  มีผู้ควบคุมงานตรวจสอบควบคุมแนวดิ่งและความลึกในการเจาะเสาเข็มหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr> 
                                </table>

                                 <h5 class="text-blue"><b>2.2  ก่อนการเทคอนกรีตของเสาเข็มเจาะมีการตรวจสอบค่าการยุบตัวและเก็บตัวอย่างของคอนกรีตหรือไม่ </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีการทดสอบ </td>
                                    <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">วัดได้ (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                 <td><input type="checkbox"> ไม่มีการทดสอบ </td>
    		                      </tr>
                                 <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีการเก็บตัวอย่าง </td>
                                    <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">จำนวน (ตัวอย่างลูก) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                 <td><input type="checkbox"> ไม่มีการเก็บตัวอย่าง </td>
    		                      </tr>
                              
                                </table>

                                 <h5 class="text-blue"><b>2.3  มีผู้ควบคุมงานตรวจสอบควบคุมการเทคอนกรีตเสาเข็มเจาะหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr> 
                                </table>

                                 <h5 class="text-blue"><b>2.4  ในการเทคอนกรีตผู้รับจ้างใช้ท่อช่วยส่งคอนกรีตลงสู่หลุมเจาะหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> ใช้ </td>
                                 <td><input type="checkbox"> ไม่ใช้ </td>
    		                      </tr> 
                                </table>

                                 <h5 class="text-blue"><b>2.5  ผู้รับจ้างใช้เครื่องมือในการตัดหัวเสาเข็มโดยรอบให้ได้แนวตรงก่อนสกัดหัวเสาเข็มหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> ใช้ </td>
                                 <td><input type="checkbox"> ไม่ใช้ </td>
    		                      </tr> 
                                </table>
                                  </div> 
                           <div class="col-xs-1"></div>  
                         </div></li>

<!-------------------------------------------------------Choice-3--------------------------------->
                                    <li class="list-group-item">
                                   
                                        <h5 class="text-blue"><b>3. ฐานรากแผ่  <a class="pull-right">ข้าม</a></b></h5>
                                        
                                        <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                                <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                <h5 class="text-blue"><b>3.1  ผู้รับจ้างขุดเปิดพื้นที่ก่อสร้างฐานรากแผ่จนถึงชั้นหินหรือชั้นดินแข็ง และมีการทดสอบการรับน้ำหนักของชั้นดินดังกล่าวหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> ถึงชั้น </td>
                                 <td><input type="checkbox"> ไม่ถึงชั้น </td>
    		                      </tr> 
                                  <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มีการทดสอบ </td>
                                 <td><input type="checkbox"> ไม่มีการทดสอบ </td>
    		                      </tr>
                                   <tr class="bg-info">
                                   <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">วันที่ :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                    <td class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">เครื่องมือที่ใช้ :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                               </tr> 
                                      <tr class="bg-info">
                                   <td colspan="2" class="actions" style=" text-align :center ;">
                                     <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">โดยหน่วยงาน :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                               </tr> 
                                </table>

                                <h5 class="text-blue"><b>3.2  ผู้รับจ้างมีการฝังเหล็กเดือยบนชั้นหินหรือชั้นดินแข็งก่อนก่อสร้างฐานรากแผ่หรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มี </td>
                                 <td class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">วันที่ :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr> 
                                </table>

                                <h5 class="text-blue"><b>3.3  มีผู้ควบคุมการเทคอนกรีต ทดสอบค่าการยุบตัวและเก็บตัวอย่างคอนกรีตในระหว่างการเทคอนกรีตหรือไม่  </b></h5>
                                 <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="30%"><input type="checkbox"> มีผู้ควบคุม </td>
                                 
                                 <td><input type="checkbox"> ไม่มีผู้ควบคุม </td>
    		                      </tr>
                                     </table>

                                      <table class="table table-bordered table-condensed">
                                   <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีการทดสอบ </td>
                                 <td class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">วัดได้ (เซนติเมตร) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                 <td><input type="checkbox"> ไม่มีการทดสอบ </td>
    		                      </tr>  

                                     <tr class="bg-info">
                                 <td width="20%"><input type="checkbox"> มีการเก็บ </td>
                                 <td class="actions" style=" text-align :center ;">
                                 <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-4 control-label text-right">จำนวนตัวอย่าง (ลูก) :</label>
                                            <div class="col-sm-6">
                                            <input type="text6" class="form-control" id="date" placeholder="">
                                        </div></div></td>
                                 <td><input type="checkbox"> ไม่มีการทดสอบ </td>
    		                      </tr>  
                                </table>
                              </div>
                           <div class="col-xs-1"></div>
                         </div></li>

<!-------------------------------------------------------Choice-4--------------------------------->
                                    <li class="list-group-item">
                                    
                                        <h5 class="text-blue"><b>4. มีการติดตั้งป้ายเตือนความปลอดภัยในสถานที่ก่อสร้างหรือไม่ <a class="pull-right">ข้าม</a></b></h5>

                                      <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed"> 
                                <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                            </table><br />
                               <div class="col-xs-1"></div>
                            <div class="col-xs-10">
                                  <table class="table table-bordered table-condensed">
                                 <tr class="bg-info">
                                 <td width="50%"><input type="checkbox"> มี </td>
                                 <td><input type="checkbox"> ไม่มี </td>
    		                      </tr>
                                </table>
                              </div>
                           <div class="col-xs-1"></div>
                         </div></li>

<!-------------------------------------------------------Choice-5--------------------------------->
                                    <li class="list-group-item">
                                        <h5 class="text-blue"><b>5. ข้อเสนอแนะ <a class="pull-right">ข้าม</a></b></h5>
                                     <div class="box-body no-padding">
                           
                            <table class="table table-bordered table-condensed">
                                
                              <tr>
                                <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    <th class="actions" style=" text-align :center ;"><img src="http://placehold.it/150x100" alt="..." class="margin"></th>
    		                    </tr>
                           </table><br />
                                <div class="col-xs-1"></div>
                                   <div class="col-xs-10">
                                 <table class="table table-bordered table-condensed">
                               
                                 <tr class="bg-info">
                                 <td COLSPAN="2"><div class="col-sm-12"><div class="input-group" style="width: 100%;"> 
                                 <textarea class="form-control" rows="4" placeholder="ข้อความ" ></textarea>
                                </div></div></td></tr>
                                </table>
                                
                              </div>
                            <div class="col-xs-1"></div>
                         </div></li>  

                                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกแล้วทำต่อ</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div> 
                      </ul></div> 

                           
                 
                  
                        </div><!-- /.box-body -->
                      </div><!-- /.box -->
                    </div>

                      
                 </div>
                </div><!-- /.tab-pane -->

               </div></div>
                </div>
              
              </div><!-- nav-tabs-custom -->
            </div>
         </div>
        </section><!-- /.content -->
      </div>


</asp:Content>

