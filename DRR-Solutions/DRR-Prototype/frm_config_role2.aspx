﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_role2.aspx.cs" Inherits="DRR_Citizen.frm_config_role2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <div class="content-wrapper" style="min-height: 1096px;">
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
             <div class="box box-solid box-primary">
                <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลสิทธ์ผู้ใช้งาน (CPM_UT0212)</b></h4></div>
             <div class="box-body">
                 <div class="row">

                    <form class="form-horizontal">
                        <div class="col-md-7">
                    
                        <div class="box-body">

                            <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">คำนำหน้า :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">นาย</option>
                      <option>นางสาว</option>
                      <option>นาง</option>
                    </select></div>
                  </div><!-- /.form-group --><br />

                  <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ชื่อ :</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox3" runat="server" class="form-control" placeholder="อานนท์" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div><br />
                    <div class="form-group">
                        
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">นามสกุล :</label>
                      <div class="col-sm-8">
                         <asp:TextBox ID="TextBox4" runat="server" class="form-control" placeholder="แฮวอู" ReadOnly ="true"></asp:TextBox>
                        
                      </div>
                    </div><br />
                        <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">ตำแหน่ง :</label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox5" runat="server" class="form-control" placeholder="เจ้าหน้าที่ระบบสารสนเทศ" ReadOnly ="true"></asp:TextBox>
                      </div>
                    </div><br />
                            <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">รหัสกลุ่ม :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">01</option>
                      <option>02</option>
                      <option>03</option>
                      <option>04</option>
                      <option>05</option>
                      <option>06</option>
                      <option>07</option>
                      <option>08</option>
                      <option>09</option>
                    </select></div>
                  </div><!-- /.form-group --><br />

                       <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">กลุ่มผู้ใช้งาน :</label>
                        <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">ผู้บริหาร</option>
                      <option>Admin</option>
                      <option>ผส.สำนักงานภูมิภาคส่วนกลาง</option>
                      <option>ผอ.แขวง</option>
                      <option>ผอ.ผู้ตรวจสอบแทน</option>
                      <option>ช่างควบคุมงาน</option>
                      <option>กลุ่มเครือข่ายที่เกี่ยวข้อง</option>
                      <option>อสทช.</option>
                      <option>ประชาชน</option>
                    </select></div>
                  </div><!-- /.form-group --><br />
                      </div></div>
                        <div class="col-md-5">
                    
                        <div class="box-body">

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left"><small>ชื่อผู้ใช้ในระบบ :</small></label>
                      <div class="col-sm-8">
                       
                        <asp:TextBox ID="TextBox14" runat="server" class="form-control" placeholder="Anon" ReadOnly ="true"></asp:TextBox>
                      </div> </div><br />

                      <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">รหัสผ่าน :</label>
                      <div class="col-sm-8">
                          <asp:TextBox ID="TextBox1" runat="server" class="form-control" placeholder="abc" ReadOnly ="true"></asp:TextBox>
                        
                      </div><br />
                    </div>
                        <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left"><small>ยืนยันรหัสผ่าน :</small></label>
                      <div class="col-sm-8">
                           <asp:TextBox ID="TextBox2" runat="server" class="form-control" placeholder="1234" ReadOnly ="true"></asp:TextBox>
                      </div><br />
                    </div>
                      </div></div>
                     
                    </form>
                 
                </div>
            </div>
            </div><!-- /.box-body -->
         <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  </div><!-- /.box -->
              </div>
          </div>
        </section><!-- /.content -->
     </div>
</asp:Content>
