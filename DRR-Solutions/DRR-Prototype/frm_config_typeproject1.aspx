﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_typeproject1.aspx.cs" Inherits="DRR_Citizen.frm_config_typeproject1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
     <div class="content-wrapper" style="min-height: 1096px;">
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
                 <div class="box box-solid box-info">
                <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>ข้อมูลโครงการ (CPM_UT0205)</b></h4></div><br />
               <div class="box-header with-border">
                      <a href="#" class="btn bg-aqua pull-right" style="margin-right: 0px;"><i class="fa fa-plus"> เพิ่มโครงการ</i></a></div>
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">รหัสโครงการ</th>
                      <th class="actions" style=" text-align :center ;">ประเภทโครงการ</th>
                      <th class="actions" style=" text-align :center ;">แก้ไข</th>
                     </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">01</td>
                      <td class="actions" style=" text-align :center ;">ก่อสร้างทาง</td> 
                      <td class="actions" style=" text-align :center ;"><a href="frm_config_typeproject2.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;">02</td> 
                      <td class="actions " style=" text-align :center ;">ก่อสร้างสะพาน</td> 
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_s.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>
                   
                    <tr>
                      <td class="actions" style=" text-align :center ;">03</td> 
                      <td class="actionsg" style=" text-align :center ;">บำรุงรักษา</td> 
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_b.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>
                  <tr>
                      <td class="actions " style=" text-align :center ;">04</td> 
                      <td class="actions" style=" text-align :center ;">อำนวยความปลอดภัย</td> 
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_p.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>
                   <tr>
                      <td class="actionsg" style=" text-align :center ;">05</td> 
                      <td class="actions " style=" text-align :center ;">ซ่อมบำรุง</td> 
                      <td class="actions" style=" text-align :center ;"><a href="frm_question_sh.aspx"><span class="label label-warning"><i class="fa fa-pencil"></i></span></a></td>
                    </tr>
                   
                   
                  </table>
           </div><!-- /.box-body -->

                </div><!-- /.box-body -->
        
                </div>
              </div>
        
        </section><!-- /.content -->
      </div>

</asp:Content>
