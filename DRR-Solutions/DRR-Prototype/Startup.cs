﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DRR_Citizen.Startup))]
namespace DRR_Citizen
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
