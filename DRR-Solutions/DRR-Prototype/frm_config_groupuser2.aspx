﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_config_groupuser2.aspx.cs" Inherits="DRR_Citizen.frm_config_groupuser2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="content-wrapper" style="min-height: 1096px;">
        <!-- Content Header (Page header) -->
       
        <!-- Main content -->
        <section class="content">

          <div class="row">
            <!-- /.col -->
             <div class="col-md-12">
           
             <div class="box box-solid box-primary">
                    <div class="box-body">
                  <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลกลุ่มผู้ใช้ : ผู้บริหาร (CPM_UT0214)</b></h4></div>
     
             <div class="box-body">
                 <div class="row">
                  <table class="table table-bordered table-condensed">
    	           <tr class="bg-blue-gradient">
                     <th class="actions" style=" text-align :center ;"><small>เมนู</small></th>
                      <th class="actions" style=" text-align :center ;"><small>แสดง</small></th>
                      <th class="actions" style=" text-align :center ;"><small>ไม่แสดง</small></th>
                      <th class="actions" style=" text-align :center ;"><small>View</small></th>
                      <th class="actions" style=" text-align :center ;"><small>Edit</small></th>
                      <th class="actions" style=" text-align :center ;"><small>Update</small></th>
                      <th class="actions" style=" text-align :center ;"><small>Delete</small></th>
                      <th class="actions" style=" text-align :center ;"><small>Approve</small></th>
                    </tr>
                
                    <tr>
                      <td class="actions" style=" text-align :center ;"><small>โครงการ/สายทาง</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><small>ผู้มีส่วนได้ส่วนเสีย</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>

                     <tr>
                      <td class="actions" style=" text-align :center ;"><small>ข้อมูล อสทช.</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><small>เทคนิคการมีส่วนร่วม</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>

                      <tr>
                      <td class="actions" style=" text-align :center ;"><small>แบบสอบถาม</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>
                    <tr>
                      <td class="actions" style=" text-align :center ;"><small>โซเชียล</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>

                       <tr>
                      <td class="actions" style=" text-align :center ;"><small>เว็บบอร์ด</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;"><small>รายงาน</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>
                       <tr>
                      <td class="actions" style=" text-align :center ;"><small>ตั้งค่าระบบ</small></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                       <td class="actions" style=" text-align :center ;"><i class="fa fa-check text-green"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                      <td class="actions" style=" text-align :center ;"><i class="fa fa-minus text-red"></i></td>
                    </tr>
                  </table>
                 <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>  </div><!-- /.box -->
                   
                </div><!-- /.box-body -->
                  
                </div>
            </div></div>
            </div><!-- /.box-body -->
           
        </section><!-- /.content -->
      </div>
</asp:Content>
