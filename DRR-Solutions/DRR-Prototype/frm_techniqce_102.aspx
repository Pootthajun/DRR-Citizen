﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce_102.aspx.cs" Inherits="DRR_Citizen.frm_techniqce_102" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         
          <ol class="breadcrumb">
            <li><a href="Dashboard.aspx">หน้าหลัก</a></li>
             <li><a href="#">เทคนิคการมีส่วนร่วม</a></li>
             <li class="active">ประกาศตามระเบียบราชการ</li>
         </ol><br /><br />
        </section>

 
       <section class="content" runat ="server">
         
           <div class="row">
             <div class="col-md-12">
                 <div class="box box-solid box-primary">
                      <div class="box-header with-border"><h5><b>ประกาศตามระเบียบราชการ</b></h5></div>   
               
                 <div class="box-body">
             <div class="box-body">
                 <div class="row">

                    <form class="form-horizontal">
                         <div class="col-md-12">
                      <div class="box-body">
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">ชื่อการดำเนินงาน :</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" id="name" placeholder="">
                         </div> 
                      </div>
                     </div></div>
                     
                      <div class="col-md-6">
                    
                        <div class="box-body">

                            <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">วันที่ดำเนินการ :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                                    <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                      </div>
                    </div>
                        <div class="form-group">
                      <label for="inputPassword3" class="col-sm-4 control-label text-left">จากวันที่ :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                                    <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                      </div>
                    </div>
                            <div class="form-group">
                        <label for="inputName" class="col-sm-5 control-label">จำนวนผู้มีส่วนร่วม/เกี่ยวข้อง :</label>
                        <div class="col-sm-7">
                          <input type="email" class="form-control" id="inputName" placeholder="">
                         </div> 
                      </div>     
                    
                      </div></div>
               
                      <div class="col-md-6">
                    
                         <div class="box-body">

                             <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">เวลา :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                                    <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                    <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div></div><!-- /.input group -->
                      </div>
                    </div>
                             <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">ถึงวันที่ :</label>
                      <div class="col-sm-8">
                        <div class="input-group">
                                    <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div><!-- /.input group -->
                      </div>
                    </div>
                         
                    </div></div>

                      <div class="col-md-12">
                    
                         <div class="box-body">
                             <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">รายละเอียด :</label>
                       <div class="col-sm-10">
                      <textarea class="form-control" rows="5" placeholder=""></textarea>
                      <span class="input-group-btn">
                      </span></div>
                      </div>

                             <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">ผลการดำเนินการ :</label>
                       <div class="col-sm-10">
                      <textarea class="form-control" rows="3" placeholder=""></textarea>
                      <span class="input-group-btn">
                      </span></div>
                      </div>
                            <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">ข้อเสนอแนะ :</label>
                      <div class="col-sm-10">
                          <input type="email" class="form-control" id="Name" placeholder="">
                         </div>
                      </div>
                             <div class="form-group">
                            <label for="inputExperience" class="col-sm-2 control-label">แนบเอกสาร :</label>
                         <div class="col-sm-10">
                        <div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment">
                         </div></div>
                       </div>
                            <div class="form-group">
                            <label for="inputExperience" class="col-sm-2 control-label">แนบรูปภาพ :</label>
                         <div class="col-sm-10">
                        <div class="btn btn-success btn-file">
                             <i class="fa fa-paperclip"></i> แนบไฟล์
                             <input type="file" name="attachment">
                         </div><div class="timeline-body">
                      <img src="http://placehold.it/150x100" alt="..." class="margin">
                      <img src="http://placehold.it/150x100" alt="..." class="margin">
                      <img src="http://placehold.it/150x100" alt="..." class="margin">
                      <img src="http://placehold.it/150x100" alt="..." class="margin">
                      <img src="http://placehold.it/150x100" alt="..." class="margin">
                    </div></div>
                       </div>
                        

                         </div></div>
                            
                    </form>
                
                  
                </div><div class="row no-print"><center>
                   
                        <button class="btn btn-warning pull-right" style="margin-right: 40px;"> ยกเลิก</button>
                        <button class="btn btn-info pull-right" style="margin-right: 20px;"> บันทึกข้อมูล</button></center>
                </div><br />
            </div>
            </div><!-- /.box-body -->
        
            </div></div>
                  </div><!-- /.box-body -->
        
        </section><!-- /.content -->

    </div>
      

</asp:Content>

