﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true"  EnableEventValidation="false"  CodeBehind="frm_add_techniqce_total.aspx.cs" Inherits="DRR_Citizen.frm_add_techniqce_total" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
     <style>
      .example-modal .modal {
        position: relative;
        top: auto;
        bottom: auto;
        right: auto;
        left: auto;
        display: block;
        z-index: 1;
      }
      .example-modal .modal {
        background: transparent !important;
      }
    </style>
<!-- Main content ----------------------------------------------------------->
      <div class="content-wrapper">
       
<!-- Main content ----------------------------------------------------------->
        <section class="content">
            <asp:ScriptManager id="ScriptManager1" runat="server">
        </asp:ScriptManager>
             <asp:UpdatePanel id="UpdatePanel1" runat="server">
            <ContentTemplate>
               <div class="box box-solid box-primary">
     
                      <div class="box-body">
                           <div class="box-header with-border"><h4 class="pull-left"><b> เพิ่มกิจกรรมวางแผน <small>โครงการ ถนนสายแยก ทล.416 - บ.สาคร (ตอนที่ 2)</small></b></h4></div>
             
                     <div class="box-body">
                         <div class="row">
                         <form class="form-horizontal">
                          <div class="box-body">
                       
                      <div class="form-group">
                      <label for="inputEmail3" class="col-sm-3 control-label text-right">ประเภทกิจกรรม :</label>
                      <div class="col-sm-7">
                   <%-- <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">แผ่นป้ายประชาสัมพันธ์</option>
                      <option>Website</option>
                      <option>E-mail</option>
                      <option>จดหมายข่าว</option>
                      <option>แถลงข่าว</option>
                      <option>วิทยุ</option>
                      <option>โทรทัศน์</option>
                      <option>หนังสือพิมพ์</option>
                      <option>ประกาศตามระเบียบราชการ</option>
                      <option>สัมภาษณ์รายบุคคล</option>
                      <option>สำรวจความคิดเห็น</option>
                      <option>สนทนากลุ่มย่อย</option>
                      <option>สายด่วน/สายตรง</option>
                      <option>พบปะอย่างไม่เป็นทางการ</option>
                      <option>จัดประชุม</option>
                      <option>สัมมนาเชิงปฏิบัติการ</option>
                      <option>เวทีสาธารณะ</option>
                      <option>คณะทำงานเพื่อแลกเปลี่ยนข้อมูล</option>
                      <option>ประชุมระดมสมอง</option>
                      <option>ประชุมกลุ่มย่อยในการวางแผน</option>
                      <option>ประชุมระดมความคิดเห็น</option>
                      <option>คณะที่ปรึกษาร่วมภาคประชาชน</option>
                      <option>คณะกรรมการร่วมภาคประชาชน</option>
                      <option>การทำประชามติ</option>
                      <option>Social Listening</option>
                    </select>--%>
                           <asp:DropDownList ID="ddl_type" runat="server" class="form-control select2" style="width: 100%;" AutoPostBack="True" OnSelectedIndexChanged="ddl_type_SelectedIndexChanged"></asp:DropDownList>
                      </div>
                    </div>
                
                  
                  </div><!-- /.box-body -->
                  
                          </form>
                             <div class="box-body">
                            <br /><div class="row">

                              <div class="col-md-12">

                                <div class="box-body">
                               <div class="box-body no-padding"style="overflow-x:auto;">
                                <table class="table table-bordered">
                                <tr class="bg-blue-gradient">
                                <TH COLSPAN="2" class="actions" style=" text-align :center ;">
                                   <asp:Label ID="lblType" runat="server" Text="ข้อมูลแผ่นป้ายประชาสัมพันธ์"></asp:Label></th>
                                </tr>
    	                        <tr class="bg-blue-gradient">
    		                    <th class="actions" style=" text-align :center ;"><b>หัวข้อ</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>กรอกข้อมูล</b></th>
                                </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ชื่อการดำเนินงาน :</b></td>
    		                    <td width="80%"><div class="col-sm-12">
                                    <div class="input-group" style="width: 100%;">
                                    <%--<input type="text" name="table_search" class="form-control input-sm" placeholder="">--%>
                                    <input type="text1" class="form-control" id="inputnNameProject" placeholder="ข้อความ">
                                </div></div></div></td>
    		                    </tr>

                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		                    <td width="80%"><div class="col-sm-5">
                                    <div class="input-group" style="width: 100%;">
                                         <input type="text2" class="form-control" id="inputnPlan" placeholder="">
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div></div></td>
    		                    </tr>
                                <tr>

    		                    <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการจริง :</b></td>
    		                    <td width="80%"><div class="col-sm-5">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text3" class="form-control" id="inputnDate" placeholder="">
                                   <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>สถานที่ดำเนินการ :</b></td>
    		                    <td width="80%"><div class="col-sm-12">
                                    <div class="input-group" style="width: 100%;">
                                    <input type="text4" class="form-control" id="inputnPlace" placeholder="ข้อความ"></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>รายละเอียด :</b></td>
    		                    <td width="80%"> <div class="col-sm-12">
                                <textarea class="form-control" rows="7" placeholder="ข้อความ"></textarea>
                                <span class="input-group-btn">
                                </span></div></td>
    		                    </tr>
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
    		                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text5" class="form-control" id="inputnSuccess" placeholder="ข้อความ"></div></div></td>
    		                    </tr>

                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
    		                    <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                   <input type="text6" class="form-control" id="inputnOther" placeholder="ข้อความ"></div></div></div></td>
    		                    </tr>
                               
                                <tr>
    		                    <td class="actions" style=" text-align :right ;"><b>แนบรูปภาพ :</b><br /> <br />
                                   <img src="dist/img/button_icon/button-file.png"  width="40" height="40" style="margin-right: 20px;"
                                 <%--data-widget="add_file" data-toggle="tooltip" title="แนบไฟล์"--%> />
                                 <br /><a href="#">แนบไฟล์ &nbsp;&nbsp;&nbsp;</a></td>
                         
                                 <td><div class="col-sm-12"><i class="text-red text-left"><b>*</b> ขนาดไฟล์แนบไม่เกิน 5 MB (jpg,pdf,png)</i>
                                 <div class="col-sm-12">
                                     <div class="input-group" style="width: 100%;"></div>
                                 </div></td> 
    		                    </tr>
                            </table> <br />
                           <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
                        </div> </div>

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->


            </div><!-- /.box-body -->
          
        </div> 

 <%--<div class="box-body">
               <br /><div class="row">
              <div class="col-md-1"></div>

                   <div class="col-md-10">

                       <div class="box-body">
       
            <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered table-condensed">
                                <tr><TH COLSPAN="2" class="actions bg-primary" style=" text-align :center ;"><br>จัดการประชุม</th></tr>
    	                        <tr class="bg-blue">
    		                    <th class="actions" style=" text-align :center ;"><b>หัวข้อ</b></th>
    		                    <th class="actions" style=" text-align :center ;"><b>กรอกข้อมูล</b></th>
                                </tr>
                               
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ชื่อการดำเนินงาน</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;วันที่วางแผนไว้</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;วันที่ดำเนินการ</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;เวลา</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;สถานที่ดำเนินการ</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;จำนวนผู้มีส่วนร่วม</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;รายละเอียด</td>
    		                    <td> <div class="col-sm-12">
                                <textarea class="form-control" rows="7" placeholder=""></textarea>
                               
                                <span class="input-group-btn">
                                </span></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ผลการดำเนินการ</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;ข้อเสนอแนะ</td>
    		                    <td><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;แนบเอกสาร</td>
    		                    <td class="actions" style=" text-align :center ;"><div class="btn btn-success btn-file">
                                     <i class="fa fa-paperclip"></i> แนบไฟล์
                                     <input type="file" name="attachment">
                                </div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;แนบรูป</td>
    		                    <td class="actions" style=" text-align :center ;"><div class="btn btn-success btn-file">
                                    <i class="fa fa-paperclip"></i> แนบไฟล์
                                    <input type="file" name="attachment">
                                   </div></td>
    		                    </tr>
                                <tr>
    		                    <td> &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;รายชื่อผู้เข้าร่วม/ผู้มีส่วนได้ส่วนเสีย</td>
    		                    <td><div class="col-sm-9"><div class="input-group" style="width: 100%;">
                                    <input type="text" name="table_search" class="form-control input-sm" placeholder=""></div></div></td>
    		                    </tr>
                                
                                
                            </table> <br />
                             <div class="pull-right"> 
                                 <button class="btn btn-info" style="margin-right: 40px;"> บันทึกข้อมูล</button>
                                 <button class="btn btn-warning" style="margin-right: 20px;"> ยกเลิก</button></div>
                        </div> <br /></div>

                </div><!-- /.box-body -->
        
            </div><!-- ./col -->

              <div class="col-md-1"></div>

            </div><!-- /.box-body --><br />
          
        </div>--%>

                         </div>
               </div>
                  </div>     
              
            </div> 
         </ContentTemplate>
        </asp:UpdatePanel>
    </section>
   </div>

      
</asp:Content>
