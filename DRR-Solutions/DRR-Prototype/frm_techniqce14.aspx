﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masterpage.Master" AutoEventWireup="true" CodeBehind="frm_techniqce14.aspx.cs" Inherits="DRR_Citizen.frm_techniqce14" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">

<div class="content-wrapper">
       
       <section class="content" runat ="server">
         
           <div class="row">
             <div class="col-md-12">
                 <div class="box box-solid box-primary">
     
                      <div class="box-body">
         <div class="box-header with-border"><h4 class="pull-left"><b>จัดการข้อมูลสายด่วน/สายตรง (CPM_UT0430)</b></h4></div>
              <div class="box-body">
                   <div class="box-body no-padding"style="overflow-x:auto;">
                    <table class="table table-bordered">
                    <tr class="bg-blue-gradient">
                      <th class="actions" style=" text-align :center ;">หัวข้อ</th>
                      <th class="actions" style=" text-align :center ;">รายละเอียด</th>
                     </tr>
                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>หัวข้อสนทนา :</b></td>
                    <td width="80%"><div class="col-sm-12">
                          <div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox3" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                          </div></div></td>
                    </tr> 
                     <tr>
    		          <td class="actions" style=" text-align :right ;"><b>วันที่วางแผนไว้ :</b></td>
    		          <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox1" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
    		         </tr>
                    
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>วันที่ดำเนินการ :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox2" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                     </div></div></div> </td>
                     </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>เวลา :</b></td>
                     <td width="80%"><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox7" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox>
                    
                    <div class="input-group-addon">
                          <i class="fa fa-clock-o"></i>
                     </div></div></div> </td>
                     </tr>

                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>จากวันที่-ถึงวันที่ :</b></td>
                     <td><div class="col-sm-5">
                         <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox4" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                     <div class="input-group-addon"> <i class="fa fa-calendar"></i> </div></div></div> 

                         <div class="col-sm-5"> <div class="input-group" style="width: 100%;"> 
                     <asp:TextBox ID="TextBox8" runat="server" class="form-control text-left" placeholder="-" ReadOnly ="true"></asp:TextBox>
                     <div class="input-group-addon"><i class="fa fa-calendar"></i>
                     </div></div></div> 
                     </td>
                    </tr>

                    <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ผลการดำเนินการ :</b></td>
                     <td width="70%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox5" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                   
                     <tr>
    		         <td class="actions" style=" text-align :right ;"><b>ข้อเสนอแนะ :</b></td>
                     <td width="80%"><div class="col-sm-12"><div class="input-group" style="width: 100%;">
                     <asp:TextBox ID="TextBox6" runat="server" class="form-control text-left" placeholder="" ReadOnly ="true"></asp:TextBox></div></div></td>
                    </tr>
                 
                  </table>
                </div>
                </div><!-- /.box-body -->
        
                </div>

                    <div class="modal-footer">
              <button type="button" class="btn btn-linkedin"><i class="fa fa-save"></i> บันทึกข้อมูล</button>
             <button type="button" class="btn btn-google" data-dismiss="modal"><i class="fa fa-close"></i> ยกเลิก</button>
            </div>   
              </div>

               </div>    
            </div>
          
        </section><!-- /.content -->

    </div>
      
      
</asp:Content>
