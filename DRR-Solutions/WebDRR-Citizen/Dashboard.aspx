﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master/Masterpage.Master" AutoEventWireup="true" CodeFile="Dashboard.aspx.cs" Inherits="Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">



 <div class="content-wrapper">
        <!-- Content Header (Page header) -->
       <section class="content-header">
        
          <div class="col-md-4">

                  <div class="form-group">
                    <label>สรุปภาพรวมการมีส่วนร่วมภาคประชาชนในปี :</label>
                    <select class="form-control select2" style="width: 100%;">
                      <option selected="selected">2559</option>
                      <option>2558</option>
                      <option disabled="disabled">2557</option>
                      <option>2556</option>
                      <option>2555</option>
                      <option>2554</option>
                      <option>ดูทั้งหมด</option>
                    </select>
                  </div><!-- /.form-group -->
                </div><!-- /.col -->
         
						 
          <ol class="breadcrumb">
            <li><a href="Dashboard.aspx">หน้าหลัก</a></li>
          </ol>
        </section>
		
		

<!-- Main content ----------------------------------------------------------->
        <section class="content">

         <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
               
                  <div class="box-header">
                  <h5 class="box-title">สถานะโครงการรายล่าสุด</h5>
				   <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->

                <div class="box-body pad table-responsive">
               
                  <table class="table table-bordered text-center">
                    <tr>
                      <th><button class="btn btn-block btn bg-maroon btn-lg">   
                        <span class="info-box-text">สำรวจออกแบบ</span>
                        <span class="info-box-number"> 45 โครงการ</span>
                         </button>
                       </th>

                      <th><button class="btn btn-block btn bg-navy btn-lg">
                        <span class="info-box-text">ก่อสร้างสะพาน</span>
                        <span class="info-box-number">14 โครงการ</span>
                          </button></th>
                      <th><button class="btn btn-block btn bg-purple btn-lg">
                          <span class="info-box-text">บำรุงทาง</span>
                          <span class="info-box-number">10 โครงการ</span>
                          </button></th>
                      <th><button class="btn btn-block btn-info btn-lg">
                         <span class="info-box-text">อำนวยความปลอดภัย</span>
                         <span class="info-box-number">46 โครงการ</span>
                         </button></th>
                      <th><button class="btn btn-block btn bg-olive btn-lg">
                          <span class="info-box-text">ซ่อมบำรุงปกติ</span>
                          <span class="info-box-number">10 โครงการ</span>
                          </button></th>
                        <th><button class="btn btn-block btn bg-orange btn-lg">
                         <span class="info-box-text">คืนค้ำประกันสัญญา</span>
                         <span class="info-box-number">6 โครงการ</span>
                         </button></th>
                    </tr>
                 
                  </table>
                </div><!-- /.box -->
              </div>
            </div><!-- /.col -->
         </div>

        
        <div class="row">
            <!-- Left col -->
           
         <div class="col-md-4">
		   
		     <div class="box box-info">
               <div class="box-header with-border">
                  <h4 class="box-title">ประชาชนมีส่วนร่วม</h4>
				   <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header --><br/>
				   
             
              <!-- small box -->
              <div class="small-box bg-white">
			  
		
			  
             <div class="box-body">
                 <a class="btn btn-app">
                    <span class="badge bg-yellow">อสทช. ทั่วประเทศ</span>
                    <i class="fa fa-user"></i> 1390 คน
                  </a>
                  <a class="btn btn-app">
                    <span class="badge bg-green">อสทช. (สทช.4)</span>
                    <i class="fa fa-user"></i> 45 คน
                  </a>
                  <a class="btn btn-app">
                    <span class="badge bg-purple"></span>
					 <span class="badge bg-purple">คิดเป็นเปอร์เช็น</span>
                    <i class="fa fa-signal"></i> 3.28%
                  </a>
				  </div>  
			  
			  <center><img src="dist/img/drr-1/tt.png"></center><br/><br/> 
		
			<div class="box">
              <!-- TABLE: LATEST ORDERS -->
              <div class="box box-default">
               
                  <div class="box-header with-border">
                  <h6>จำนวนคนเข้าร่วมประชุม</h6>
                  <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
				   </div>
               <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-8">
					
                    <img src="dist/img/drr-1/t2.png" WIDTH=150 HEIGHT=150>
					   
                    </div><!-- /.col -->
                    <div class="col-md-4">
                      <ul class="chart-legend clearfix">
                        <li><i class="fa fa-circle-o text-red"></i>    ท</li>
                        <li><i class="fa fa-circle-o text-blue"></i>    ส</li>
                        <li><i class="fa fa-circle-o text-green"></i>   บ </li>
                      </ul>
                    </div><!-- /.col -->
					
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
				
                <div class="box-footer no-padding">
                  <ul class="nav nav-pills nav-stacked">
                   
                    <li><a href="#">ก่อสร้างทาง <span class="pull-right text-green"><i class="fa fa-angle-up"></i> 40%</span></a></li>
                    <li><a href="#">ก่อสร้างสะพาน <span class="pull-right text-yellow"><i class="fa fa-angle-left"></i> 20%</span></a></li> 
                    <li><a href="#">บำรุงรักษา <span class="pull-right text-red"><i class="fa fa-angle-down"></i> 12%</span></a></li>
                  </ul>
				 </div><!-- /.box --> 
			</div>
		</div>
      </div>

		 </div><!-- /.row -->
                
         <div class="col-md-8">
              <!-- MAP & BOX PANE -->
             
              <div class="row">
                <div class="col-md-6">
                  <!-- DIRECT CHAT -->
                  <div class="box direct-chat direct-chat-warning">
                    <div class="box-header with-border bg-teal">
                      <h6 class="box-title">จำนวนผู้ทำแผ่นพับ</h6>
                      <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->
					
					
                    <div class="box-body">
                      <!-- Conversations are loaded here -->
                      <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                        <script type="text/javascript">
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {

                    var data = google.visualization.arrayToDataTable([
                     ['Task', 'Hours per Day'],
                     ['ประเภท ท', 30],
                     ['ประเภท ส', 37],
                     ['ประเภท บ', 20],
                     ['ประเภท ป', 10],
                     ['ประเภท ซ', 3]
                      ]);

                    var options = { };

                    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                    chart.draw(data, options); }
                    </script>
  
             <div id="piechart" style="width: 280px; height: 250px;"></div>
                    <div class="box-footer text-center">
                      <a href="javascript::" class="uppercase">ข้อมูลทั้งหมด</a>
                    </div><!-- /.box-footer -->
 
                    </div><!-- /.box-body -->
                   
                  </div><!--/.direct-chat -->
                </div><!-- /.col -->

                <div class="col-md-6">
                     <!-- USERS LIST -->
                  <div class="box">
                    <div class="box-header with-border bg-teal">
                      <h3 class="box-title">ประเภทโครงการ</h3>
                      <div class="box-tools pull-right"> 
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                    </div><!-- /.box-header -->

                       <div class="box-body">
                       
                         <center><br /> &nbsp; &nbsp; &nbsp;<img src="dist/img/drr-1/d-3.png" WIDTH=220 HEIGHT=180></center>
                    
                    <div class="box-footer text-center">
                      <a href="javascript::" class="uppercase">ข้อมูลทั้งหมด</a>
                    </div><!-- /.box-footer -->
                    </div>
                  </div><!--/.box -->
                </div><!-- /.col -->

              </div><!-- /.row -->


             <div class="box">
             
			  <div class="box-header bg-warning">
                  <h5 class="box-title">สถานะจัดซื้อจัดจ้าง</h5>
				   <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
			  
               
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                        <tr>
                         <th>หน่วยงาน</th>
                          <th>โครงการทั้งหมด</th>
                          <th>ยังไม่ลงนาม</th>
                          <th>ลงนามในสัญญา</th>
						  <th>MOU</th>
                           <th>ทำแผน</th>
                          <th>การมีส่วนร่วม</th>
                         
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                           <td><a href="Totalproject.aspx">สทช.ที่1</a></td>
                          <td class="actions" style=" text-align :center ;">30</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                       <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่2</a></td>
                          <td class="actions" style=" text-align :center ;">31</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                       <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่3</a></td>
                          <td class="actions" style=" text-align :center ;">29</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa fa-close text-red"></span></td>
                        </tr>

                        <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่4</a></td>
                          <td class="actions" style=" text-align :center ;">40</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-close text-red"></span></td>
                        </tr>

                       <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่5</a></td>
                          <td class="actions" style=" text-align :center ;">6</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                        <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่6</a></td>
                          <td class="actions" style=" text-align :center ;">26</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa fa-close text-red"></span></td>
                        </tr>

                          <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่7</a></td>
                          <td class="actions" style=" text-align :center ;">34</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                          <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่8</a></td>
                          <td class="actions" style=" text-align :center ;">12</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                         <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่9</a></td>
                          <td class="actions" style=" text-align :center ;">43</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                       <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่10</a></td>
                          <td class="actions" style=" text-align :center ;">98</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                        <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่12</a></td>
                          <td class="actions" style=" text-align :center ;">45</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa  fa-check text-blue"></span></td>
                        </tr>

                          <tr>
                           <td><a href="pages/project/p-1.html">สทช.ที่13</a></td>
                          <td class="actions" style=" text-align :center ;">32</td>
                          <td class="actions" style=" text-align :center ;">2</td>
						  <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;">5</td>
                          <td class="actions" style=" text-align :center ;">28</td>
						  <td class="actions" style=" text-align :center ;"><span class="fa fa-close text-red"></span></td>
                        </tr>
                       
                      </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div>

                <div class="box-footer clearfix"> 
                  <a href="#" class="btn btn-sm btn-default btn-flat pull-right">ดูทั้งหมด</a>
                </div><!-- /.box-footer -->
             </div><!-- /.box -->

            </div><!-- /.col -->

        </div>

         <div class="row">
          <div class="col-lg-12">

           <div class="box box-primary">
            <div class="box-header with-border">
              <h5>โครงการทั้งหมด</h5>
                 <div class="box-tools">
                    <div class="input-group" style="width: 250px;">
                      <input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                      </div>
                    </div>
                  </div>
              </div>
            </div>
              <div class="row">
            <div class="box-body">
              <div class="box-body">
                    <div class="box-body">
                   
                      <div class="direct-chat-messages"> 

                 <div class="box-body no-padding">
                  <table class="table table-striped">
                    <tr>
                      <th class="actions" style=" text-align :center ;">เลขที่สัญญา</th>
                      <th class="actions" style=" text-align :center ;">ชื่อโครงการ</th>
                      <th class="actions" style=" text-align :center ;">รหัสสายทาง</th>
                      <th class="actions" style=" text-align :center ;">หน่วยดำเนินการ</th>
                      <th class="actions" style=" text-align :center ;">ความก้าวหน้าโครงการ(ร้อยละ)</th>
                      <th class="actions" style=" text-align :center ;">ผู้ควบคุม</th>
                      <th class="actions" style=" text-align :center ;">ผู้รายงาน</th>
                      <th class="actions" style=" text-align :center ;">ตรวจสอบล่าสุด</th>
                      <th class="actions" style=" text-align :center ;">สถานะ ทชจ.</th> 
                    </tr>
                    <tr>
                      <td><a href="Totalproject.aspx">033/2559</a></td>
                      <td><a href="Totalproject.aspx">สำรวจอสังหาริมทรัพย์ สาย นย.3007 แยก ทล.305-บ.คลอง33</a></td>
                      <td></td>
                      <td><a href="Totalproject.aspx"> สำกงานทางหลวงชนบท นครราชสีมา</a> </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-yellow">55%</span></td>
                      <td>นางทัศนีย์ ทนงแผลง</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>

                     <tr>
                      <td><a href="#">034/2559</a></td>
                      <td>สำรวจอสังหาริมทรัพย์ สาย นย.3008 แยก ทล.306-บ.คลอง34</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบทที่5 นครราชสีมา</td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-red">0%</span></td>
                      <td>นางมาริษา วันดี</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>8/18/2557</td>
                      <td></td>
                    </tr>

                      <tr>
                      <td><a href="#">035/2559</a></td>
                      <td>สำรวจอสังหาริมทรัพย์ สาย นย.3009 แยก ทล.307-บ.คลอง35</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบทที่5 นครราชสีมา</td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td>นางรัตนะ คุ้มยา</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>8/18/2557</td>
                      <td></td>
                    </tr>

                       <tr>
                      <td><a href="#">036/2559</a></td>
                      <td>สะพานข้ามคลองชลประทาน3</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบท นครราชสีมา </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-yellow">25%</span></td>
                      <td>นางสาวทวินันท์ เขียวทะเล</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>

                      <tr>
                      <td><a href="#">037/2559</a></td>
                      <td>สะพานข้ามคลองชลประทาน2</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบท นครราชสีมา </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-yellow">40%</span></td>
                      <td>นางสาวทวินันท์ เขียวทะเล</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>

                      <tr>
                      <td><a href="#">038/2559</a></td>
                      <td>สะพานข้ามคลองชลประทาน</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบท นครราชสีมา </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td>นางสาวทวินันท์ เขียวทะเล</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>

                       <tr>
                      <td><a href="#">039/2559</a></td>
                      <td>สะพานข้ามคลองชลประทาน3</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบท นครราชสีมา </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-yellow">25%</span></td>
                      <td>นางสาวทวินันท์ เขียวทะเล</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>

                      <tr>
                      <td><a href="#">040/2559</a></td>
                      <td>สะพานข้ามคลองชลประทาน2</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบท นครราชสีมา </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-yellow">40%</span></td>
                      <td>นางสาวทวินันท์ เขียวทะเล</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>

                      <tr>
                      <td><a href="#">041/2559</a></td>
                      <td>สะพานข้ามคลองชลประทาน</td>
                      <td></td>
                      <td>สำนักงานทางหลวงชนบท นครราชสีมา </td>
                      <td class="actions" style=" text-align :center ;"><span class="badge bg-green">100%</span></td>
                      <td>นางสาวทวินันท์ เขียวทะเล</td>
                      <td>นายธนะชัย อินทรศร</td>
                      <td>4/18/2557</td>
                      <td></td>
                    </tr>
                    
                  </table>
                </div><!-- /.box-body -->

                     </div><!-- /.box-body -->
            <div class="box-footer clearfix"> 
                  <a href="Totalproject.aspx" class="btn btn-sm btn-default btn-flat pull-right ">ดูทั้งหมด</a>
            </div><!-- /.box-footer -->
          </div><!-- /.box -->         
         </div><!-- /.direct-chat-pane -->
</div></div>
           </div>
         </div>
        

      </section>
      </div><!-- /.content-wrapper -->

</asp:Content>